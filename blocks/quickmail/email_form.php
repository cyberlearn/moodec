<?php

// Written at Louisiana State University

require_once($CFG->libdir . '/formslib.php');
$PAGE->requires->js('/blocks/quickmail/validation.js');

class email_form extends moodleform {
    public static $datefieldoptions = array('optional' => true, 'step' => 1);
    private function reduce_users($in, $user) {
        return $in . '<option value="'.$user.'">'.
               $this->option_display($user).'</option>';
    }

    private function option_display($user) {
        return sprintf("%s ", fullname($user));
    }


    public function definition() {
        global $CFG, $USER, $COURSE, $OUTPUT;

        $mform =& $this->_form;

        $mform->addElement('hidden', 'mailto', '');
        $mform->setType('mailto', PARAM_TEXT);
        
        $mform->addElement('hidden', 'userid', $USER->id);
        $mform->setType('userid',PARAM_INT);
        
        $mform->addElement('hidden', 'courseid', $COURSE->id);
        $mform->setType('courseid', PARAM_INT);
        
        $mform->addElement('hidden', 'type', '');
        $mform->setType('type', PARAM_ALPHA);
        
        $mform->addElement('hidden', 'typeid', 0);
        $mform->setType('typeid', PARAM_INT);

        $user_options = array();

        // Get all users

        $user_options['_all_'] = "All Users";
        //Get all group in course

        foreach ($this->_customdata['groups'] as $group) {
            $user_options['_'.$group->id.'_'] = 'Group : '.$group->name;
        }
        // Get all specific users
        foreach ($this->_customdata['users'] as $user) {
            $user_options[$user->id] = $this->option_display($user);
        }
        $links = array();
        $gen_url = function($type) use ($COURSE) {
            $email_param = array('courseid' => $COURSE->id, 'type' => $type);
            return new moodle_url('emaillog.php', $email_param);
        };

        $draft_link = html_writer::link ($gen_url('drafts'), quickmail::_s('drafts'));
        $links[] =& $mform->createElement('static', 'draft_link', '', $draft_link);

        $context = context_course::instance($COURSE->id);
        
        $config = quickmail::load_config($COURSE->id);

        $can_send = (
            has_capability('block/quickmail:cansend', $context) or
            !empty($config['allowstudents'])
        );

        if ($can_send) {
            $history_link = html_writer::link($gen_url('log'), quickmail::_s('history'));
            $links[] =& $mform->createElement('static', 'history_link', '', $history_link);
        }

        $mform->addGroup($links, 'links', '&nbsp;', array(' | '), false);

        $table = new html_table();
        $table->attributes['class'] = 'emailtable';

        $selected_label = new html_table_cell();
        $selected_label->text = html_writer::tag('strong',
            quickmail::_s('selected') . " ");

        $select_filter = new html_table_cell();
        $select_filter->text = html_writer::tag('select',
            array_reduce($this->_customdata['selected'], array($this, 'reduce_users'), ''),
            array('id' => 'mail_users', 'multiple' => 'multiple', 'size' => 30));

        $embed = function ($text, $id) {
            return html_writer::empty_tag('input', array(
                    'value' => $text, 'title' => $text, 'type' => 'button', 'id' => $id
                )
            );
        };
				
        $embed_quick = function ($text) use ($embed) {
            return $embed(quickmail::_s($text), $text);
        };
				
				
				
        $center_buttons = new html_table_cell();
        $center_buttons->text = (
            $embed($OUTPUT->larrow() . ' ' . quickmail::_s('add_button'), 'add_button') .
						$embed_quick('add_all') .
            $embed(quickmail::_s('remove_button') . ' ' . $OUTPUT->rarrow(), 'remove_button') .
            $embed_quick('remove_all')
        );

        $filters = new html_table_cell();
        $filters->text = html_writer::tag('div',
            quickmail::_s('potential_users'),
                array('class' => 'object_labels')
        ) . html_writer::tag('div',
            html_writer::select($user_options, '', '', null,
            array('id' => 'from_users', 'multiple' => 'multiple', 'size' => 30))
        );

        // DWE -> NON REQUIRED VERSION
        $table->data[] = new html_table_row(array($selected_label,''));

        //$table->data[] = new html_table_row(array($selected_required_label, $role_filter_label));
        $table->data[] = new html_table_row(array($select_filter, $center_buttons, $filters));
				
				
				
				
				
				
				
				
				
				
				$destinataires = html_writer::tag('select',
            array_reduce($this->_customdata['selected'], array($this, 'reduce_users'), ''),
            array('id' => 'mail_users', 'multiple' => 'multiple', 'size' => 30, 'class'=>'selectorsLeft'));
				$destinataires .=  html_writer::tag('div', 
					$embed(quickmail::_s('add_button'), 'add_button') .
					$embed_quick('add_all') .
          $embed(quickmail::_s('remove_button'), 'remove_button') .
          $embed_quick('remove_all'),
				array('class'=>'selectorsCenter'));
				$destinataires .=	html_writer::select($user_options, '', '', null, array('id' => 'from_users', 'multiple' => 'multiple', 'size' => 30, 'class'=>'selectorsRight'));

				
				
				
        if (has_capability('block/quickmail:allowalternate', $context)) {
            $alternates = $this->_customdata['alternates'];
        } else {
            $alternates = array();
        }
        $mform->addElement('static', 'from', quickmail::_s('from'), $USER->email);

        //$mform->addElement('static', 'selectorsss', '', html_writer::table($table));

				$mform->addElement('static', 'selectors', quickmail::_s('to'), $destinataires);
        
        $mform->addElement('text', 'additional_emails', quickmail::_s('additional_emails'), array('style'=>'width: 50%;'));
        $mform->setType('additional_emails', PARAM_TEXT);                
        $mform->addRule('additional_emails', 'One or more email addresses is invalid', 'callback', 'mycallback', 'client');
        $mform->addHelpButton('additional_emails', 'additional_emails', 'block_quickmail');
        $mform->addElement(
            'filemanager', 'attachments', quickmail::_s('attachment'),
            null, array('subdirs' => 1, 'accepted_types' => '*')
        );

        $mform->addElement('text', 'subject', quickmail::_s('subject'), array('style'=>'width: 50%;'));
        $mform->setType('subject', PARAM_TEXT);
        $mform->addRule('subject', null, 'required');

        $mform->addElement('editor', 'message_editor', quickmail::_s('message'),
            null, $this->_customdata['editor_options']);

        // Hide signature
        /*
        $options = $this->_customdata['sigs'] + array(-1 => 'No '. quickmail::_s('sig'));
        $mform->addElement('select', 'sigid', quickmail::_s('signature'), $options);
*/

       /* $radio = array(
            $mform->createElement('radio', 'receipt', '', get_string('yes'), 1),
            $mform->createElement('radio', 'receipt', '', get_string('no'), 0)
        );

        $mform->addGroup($radio, 'receipt_action', quickmail::_s('receipt'), array(' '), false);
        $mform->addHelpButton('receipt_action', 'receipt', 'block_quickmail');
        $mform->setDefault('receipt', !empty($config['receipt']));
*/
        $buttons = array();
        $buttons[] =& $mform->createElement('submit', 'send', quickmail::_s('send_email'));
        //$buttons[] =& $mform->createElement('submit', 'draft', quickmail::_s('save_draft'));
        $buttons[] =& $mform->createElement('cancel');

        $mform->addElement('date_time_selector', 'sendtime', get_string('senddate', 'block_quickmail'),
            self::$datefieldoptions);

        $mform->addGroup($buttons, 'buttons', quickmail::_s('actions'), array(' '), false);
    }
}
