<?php

// Written at Louisiana State University
$plugin->version = 2014043025;
$plugin->requires = 2013051400;
$plugin->release = "v1.5.0";
$plugin->maturity = MATURITY_STABLE;
$plugin->cron = 30;
