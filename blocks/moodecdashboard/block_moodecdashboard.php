<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Version details
 *
 * @package    block_moodecdashboard
 * @copyright  2015 - Martin Tazlari (http://cyberlearn.hes-so.ch)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot.'/blocks/moodecdashboard/lib.php');



class block_moodecdashboard extends block_base {

    function init() {
        $this->title = get_string('pluginname', 'block_moodecdashboard');


    }


    function get_content() { // Block Content
        global $PAGE;

        $PAGE->requires->js('/blocks/moodecdashboard/script/lib.js') ;

			
        if ($this->content !== null) {
            return $this->content;
        }

        if (empty($this->instance)) {
            $this->content = '';
            return $this->content;
        }

        // Object content (Add elements, element text = content that will be displayed)
        $this->content = new stdClass();
        $this->content->items = array();
        $this->content->icons = array();
        $this->content->text = '' ;
        $this->content->footer = '';

        $currentcontext = $PAGE->context ;

        if (empty($currentcontext)) {
            return $this->content;
        }


        if (!empty($this->config->text)) {
            // Don't display anything
        }else {

           
        	$this->content->text .='<script type="text/javascript">var switchTo5x=true;</script>
                                    <script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
                                    <script type="text/javascript">stLight.options({publisher: "cc4d7762-4ab2-4119-9113-8231471a2899", doNotHash: false, doNotCopy: false, hashAddressBar: false});</script>' ;
			$this->content->text.= get_user_tab();
        }

        return $this->content;
    }

    // my moodle can only have SITEID and it's redundant here, so take it away
    public function applicable_formats() {
        return array('all' => true,
                     'site' => true,
                     'site-index' => true,
                     'course-view' => false, // Doesn't allow to add it in a course
                     'course-view-social' => false,  // Doesn't allow to add it in a course
                     'mod' => true, 
                     'mod-quiz' => false);
    }

    public function instance_allow_multiple() {
          return true;
    }

    function has_config() {return true;}

    public function cron() {
            mtrace( "Hey, my cron script is running" );
             
                 // do something
                  
                      return true;
    }
}
