<?php
/**
 * Version details
 *
 * @package    block_moodecdashboard
 * @copyright  2015 - Martin Tazlari (http://cyberlearn.hes-so.ch)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
require_once ($CFG->dirroot . '/blocks/moodecprogress/lib.php');

require_once ($CFG->dirroot . '/blocks/course_overview/renderer.php');

require_once ($CFG->dirroot . '/blocks/course_overview/locallib.php');
//require_once ($CFG->dirroot . '/blocks/moodecdashboard/script/lib.js');
//$PAGE->requires->js_init_call('M.mod_foobar.init');



function get_user_tab()
{


    global $PAGE;
    $content = "";
    $PAGE->requires->css('/blocks/moodecdashboard/lib/style.css');
		$PAGE->requires->js('/blocks/moodecdashboard/script/lib.js');

    $content.= HTML_WRITER::start_tag('div', array(
       'id' => 'dashboardTableCours' 
    ));


    $content.= HTML_WRITER::start_tag('div', array(
        'class' => 'systeme_onglets'
    ));
    $content.= HTML_WRITER::start_tag('div', array(
        'class' => 'onglets'
    ));
    $content.= HTML_WRITER::start_tag('span', array(
        'class' => 'onglet_0 onglet',
        'id' => 'onglet_actual_course',
        'onclick' => "change_onglet('actual_course')",
        'value' => 'actual_course'
    ));
    $content.= get_string('title_course', 'block_moodecdashboard');
	

    $content.= HTML_WRITER::end_tag('span');
    $content.= HTML_WRITER::start_tag('span', array(
        'class' => 'onglet_0 onglet',
        'id' => 'onglet_archived_course',
        'onclick' => "change_onglet('archived_course')",
        'value' => 'archived_course'
    ));
    $content.= get_string('title_archived_course', 'block_moodecdashboard');
    $content.= HTML_WRITER::end_tag('span');
    $content.= HTML_WRITER::start_tag('span', array(
        'class' => 'onglet_0 onglet',
        'id' => 'onglet_upcoming_course',
        'onclick' => "change_onglet('upcoming_course')",
        'value' => 'upcoming_course'
    ));
    $content.= get_string('title_upcoming_course', 'block_moodecdashboard');
    $content.= HTML_WRITER::end_tag('span');
    $content.= HTML_WRITER::end_tag('div');
    $content.= HTML_WRITER::start_tag('div', array(
        'class' => 'contenu_onglets'
    ));
    $content.= HTML_WRITER::start_tag('div', array(
        'class' => 'contenu_onglet',
        'id' => 'contenu_onglet_actual_course'
    ));

    // a) Tabs actual courses
    $content.= "";
    $content.= get_user_current_course();
    $content.= HTML_WRITER::end_tag('div');
    $content.= HTML_WRITER::start_tag('div', array(
        'class' => 'contenu_onglet',
        'id' => 'contenu_onglet_archived_course'
    ));

    // b) Tabs archived
    $content.= "";
    $content.= get_user_archived_course();
    $content.= HTML_WRITER::end_tag('div');
    $content.= HTML_WRITER::start_tag('div', array(
        'class' => 'contenu_onglet',
        'id' => 'contenu_onglet_upcoming_course'
    ));

    // b) Tabs upcoming
    $content.= "";
    $content.= get_user_coming_course();

    $content.= HTML_WRITER::end_tag('div');
    $content.= HTML_WRITER::end_tag('div');
    $content.= HTML_WRITER::end_tag('div');
    $content.= HTML_WRITER::end_tag('div');

    /*
		$content.= " <script type='text/javascript'>
                var anc_onglet = 'actual_course';
                change_onglet(anc_onglet);
         </script>";
				 */
    return $content;
}

function get_user_current_course()
{
    global $DB, $USER ;
    $content = '';
   


    $sql = "SELECT DISTINCT mc.id,mc.fullname, mc.shortname, mc.startdate, mc.summary FROM {course} mc, {enrol} me, {user_enrolments} ue, {moodecdescription} md WHERE mc.id = me.courseid AND mc.id = md.course " .
        "AND md.endcoursedate > UNIX_TIMESTAMP() AND mc.startdate < UNIX_TIMESTAMP() AND me.id = ue.enrolid AND ue.userid = ? AND mc.visible=1";
	
	
    $courses = $DB->get_records_sql($sql, array($USER->id));
	
    foreach($courses as $course)
    {
        // Affichage des descriptions de cours
        $content.= moodecdashboard_display_course_description($course);
        // Création d'une barre de progression (reprise des données du % de la progress bar)
    }
    if(!$courses){
        $content .= display_no_courses_message();
    }
    return $content;
}

function get_user_archived_course() {
    global $DB, $USER;
    $content = '';

    $sql = "SELECT DISTINCT mc.id,mc.fullname, mc.shortname, mc.startdate, mc.summary  FROM {course} mc, {enrol} me, {user_enrolments} ue, {moodecdescription} md WHERE mc.id = me.courseid AND md.course = mc.id" .
        " AND md.endcoursedate < UNIX_TIMESTAMP() AND me.id = ue.enrolid AND ue.userid = ? AND mc.visible=1";
    $courses = $DB->get_records_sql($sql, array($USER->id));

    foreach($courses as $course)
    {
        // Affichage des descriptions de cours
        $content.= moodecdashboard_display_course_description($course);
        // Création d'une barre de progression (reprise des données du % de la progress bar)

    }
    if(!$courses){
        $content .= display_no_courses_message();
    }

    return $content;
}

function get_user_coming_course() {
    global $DB, $CFG, $USER;
    $content = '';

    $sql = "SELECT DISTINCT mc.id,mc.fullname, mc.shortname, mc.startdate, mc.summary  FROM {course} mc, {enrol} me, {user_enrolments} ue WHERE mc.id = me.courseid " .
        " AND mc.startdate > UNIX_TIMESTAMP() AND me.id = ue.enrolid AND ue.userid = ? AND mc.visible=1";
    $courses = $DB->get_records_sql($sql, array($USER->id));

    foreach($courses as $course)
    {
        // Affichage des descriptions de cours
        $content.= moodecdashboard_display_course_description($course);
        // Création d'une barre de progression (reprise des données du % de la progress bar)
    }


    if(!$courses){
        $content .= display_no_courses_message();
    }

    return $content;
}

function display_no_courses_message() {

    $content = '' ;
    $content.= HTML_WRITER::start_tag('div', array(
        'class' => 'alert alert-info titlecourse'
    ));
            $content.= HTML_WRITER::start_tag('span', array(
                'class' => ''
            ));
                $content.= get_string('no_courses_message', 'block_moodecdashboard');
                 $content .= HTML_WRITER::empty_tag('br');
                $url = new moodle_url('/course?categoryid=0');
                $content .= HTML_WRITER::start_tag('a', array(
                    'href' => $url
                ));
                $content .= get_string('see_catalogue','block_moodecdashboard') ;
                $content .= HTML_WRITER::end_tag('a');
                        $content.= HTML_WRITER::end_tag('span');
    $content.= HTML_WRITER::end_tag('div');
    return $content ;
}

function display_progress_bar($course)
{
    global $DB, $USER;
    $content = "";
    $sql = "SELECT bi.id FROM {block_instances} AS bi, {context} AS c WHERE bi.parentcontextid = c.id AND c.instanceid = ? AND bi.blockname = 'moodecprogress'";
    $param = array(
        $course->id
    );
    $id = $DB->get_fieldset_sql($sql, $param);
    $progressvalue = '';
    $progress = '';
    if (!empty($id))
    {
        $id = $id[0];
        $progressblock = $DB->get_record('block_instances', array(
            'id' => $id
        ) , '*', MUST_EXIST);
        $progressconfig = unserialize(base64_decode($progressblock->configdata));
        $context = block_moodecprogress_get_course_context($course->id);
        $modules = block_moodecprogress_modules_in_use($course->id);
        $events = block_moodecprogress_event_information($progressconfig, $modules, $course->id);
        $userevents = block_moodecprogress_filter_visibility($events, $USER->id, $context, $course);
        if (!empty($userevents))
        {
            $attempts = block_moodecprogress_attempts($modules, $progressconfig, $userevents, $USER->id, $course->id);
            $progressbar = block_moodecprogress_bar($modules, $progressconfig, $userevents, $USER->id, $progressblock->id, $attempts, $course->id, true);
            $progressvalue = block_moodecprogress_percentage($userevents, $attempts, true);
            if(empty($progressvalue)){
                $progressvalue = 0 ;
            }
            $progress = $progressvalue . '%';
        }else {
			$progress = '0%';
		}


        $content.= '<div class="progress" onmouseover="displayprogress('.$course->id.')" onmouseout="hideprogress('.$course->id.')">';
					
					$content.= '<div class="progress-bar" role="progressbar" aria-valuenow="70"
          aria-valuemin="0" aria-valuemax="100"  style="width:' . $progressvalue . '%">
          </div>';
				
					$content.= '<div id="progressvalue'.$course->id.'" class="progressvaluebloc progressvalue" style="float:';
					if($progressvalue>90)
					{
						$content.= 'right; margin-top: -30px;">';
						$content.= '<div class="arrow3 arrow3Right">'. $progress . '</div>';
					}	
					else if($progressvalue<10)
					{
						$content.= 'left; margin-top: -25px;">';
						$content.= '<div class="arrow3 arrow3Left">'. $progress . '</div>';
					}	
					else{
						$content.= 'left; margin-top: -25px;">';
						$content.= '<div class="arrow3">'. $progress . '</div>';
						
					}
				$content.= '<div id="clear"></div></div>';
					
        $content.= '</div>';
	
    }

    return $content;
}

function moodecdashboard_display_course_description($course)
{
    global $DB, $CFG;
	
    $content = '';
    $sql = "SELECT * FROM {moodecdescription} WHERE course = ?";
    $param = array($course->id);
		
		$url = '';
		$linkurl = '';
		$coursinstitution = '';
    if ($DB->record_exists_sql($sql, $param))
    {
        $sql = "SELECT institution, picture FROM {moodecdescription} WHERE course = ?";
        $param = array(
            $course->id
        );
        $coursedescriptions = $DB->get_records_sql($sql, $param);
	
        foreach($coursedescriptions as $coursedescription)
        {
			
            // Check if record exist
            $sql = "SELECT * FROM {files} WHERE component = ? AND filearea = ? AND filename = ? AND itemid = ?";
            $param = array(
                'mod_moodecdescription',
                'thumbnail',
                $coursedescription->picture,
                0
            );
						
						if ($DB->record_exists_sql($sql, $param))
            {
						$file = $DB->get_record_sql($sql, $param);
						$url = moodle_url::make_pluginfile_url($file->contextid, $file->component, $file->filearea, $file->itemid, $file->filepath, $file->filename);
						}
						else
						{
							$url = new moodle_url('/mod/moodecdescription/pix/default_course_image.png');
						}
						$linkurl = new moodle_url('/course/view.php?id=' . $course->id) ;
						$coursedescriptions = $coursedescription->institution. ' - ';
				}
		}		
		else
    {
			$url = new moodle_url('/mod/moodecdescription/pix/default_course_image.png');
			$linkurl = new moodle_url('/course/view.php?id=' . $course->id) ;
			$coursinstitution = '';
		}
		
		
		$content.= HTML_WRITER::start_tag('div', array('class' => 'coursRow'));
		
		/********************************   Cours Text   *******************************/
		
		
		
		$content.= HTML_WRITER::start_tag('div', array('class' => 'coursTxt'));
		
		 $coursename = $course->fullname ;
	
			
		
			$content.= HTML_WRITER::link(new moodle_url('/course/view.php?id=' . $course->id) , $coursename, array('class' => 'titlecourse'));
	
			$content.= HTML_WRITER::start_tag('div', array(
					'class' => 'moodecdashboard_description_line'
			));
				$content.= $coursinstitution;
				$content.= get_string('startdate', 'block_moodecdashboard') . date("d.m.y",$course->startdate);
			$content.= HTML_WRITER::end_tag('div');

			$content.= HTML_WRITER::start_tag('div', array('class' => 'summary'));
			
				if (strlen(substr(strip_tags($course->summary), 0, 150)) >= 150)
				{
					$content.= substr(strip_tags($course->summary), 0, 150) . '...';
				}
			$content.= HTML_WRITER::end_tag('div');
		$content.= HTML_WRITER::end_tag('div');
		
		/********************************   Coltumbnail   *******************************/
		$content.= HTML_WRITER::start_tag('div', array('class' => 'coursPicBloc coltumbnail'));
			$content.= HTML_WRITER::start_tag('a', array(
				'href'=>$linkurl, 
				'class'=>'thumbnailimage', 
				'style'=>'background: url(\''.$url.'\') no-repeat center; background-size: 105% auto'
			));
			$content.='<div class="fdhumbnail"><div class="borderWhite"><p>'.get_string('accesscours', 'block_moodecdashboard').'</p></div></div>';	
			$content.= HTML_WRITER::end_tag('a');
		$content.= HTML_WRITER::end_tag('div');
		
		
		
		/********************************   Progress Bar   *******************************/
		$content.= HTML_WRITER::start_tag('div', array('class' => 'coursProgressBar'));
				$content.= display_progress_bar($course);
		$content.= HTML_WRITER::end_tag('div');

		$content.= HTML_WRITER::end_tag('div');

    return $content;
}

/**
 * Serves the files from the moodecdashboard file areas
 *
 * @package mod_moodecdescription
 * @category files
 *
 * @param stdClass $course the course object
 * @param stdClass $cm the course module object
 * @param stdClass $context the moodecdescription's context
 * @param string $filearea the name of the file area
 * @param array $args extra arguments (itemid, path)
 * @param bool $forcedownload whether or not force download
 * @param array $options additional options affecting the file serving
 */

function moodecdashboard_pluginfile($course, $cm, $context, $filearea, array $args, $forcedownload, array $options = array())
{
    global $DB, $CFG;
    error_log("Je passe dans la méthode", 0);

    // Check the contextlevel is as expected - if your plugin is a block, this becomes CONTEXT_BLOCK, etc.

    if ($context->contextlevel != CONTEXT_MODULE)
    {
        return false;
    }

    error_log("Je passe dans le contexte", 0);

    // Make sure the filearea is one of those used by the plugin.

    if ($filearea != 'thumbnail')
    {
        return false;
    }

    // Make sure the user is logged in and has access to the module (plugins that are not course modules should leave out the 'cm' part).

    require_login($course, true, $cm);

    // Check the relevant capabilities - these may vary depending on the filearea being accessed.

    if (!has_capability('block/moodecdashboard:myaddinstance', $context))
    {
        return false;
    }

    // Leave this line out if you set the itemid to null in make_pluginfile_url (set $itemid to 0 instead).

    $itemid = array_shift($args); // The first item in the $args array.

    // Use the itemid to retrieve any relevant data records and perform any security checks to see if the
    // user really does have access to the file in question.
    // Extract the filename / filepath from the $args array.

    $filename = array_pop($args); // The last item in the $args array.
    if (!$args)
    {
        $filepath = '/'; // $args is empty => the path is '/'
    }
    else
    {
        $filepath = '/' . implode('/', $args) . '/'; // $args contains elements of the filepath
    }

    // Retrieve the file from the Files API.

    $fs = get_file_storage();
    $file = $fs->get_file($context->id, 'block_moodecdashboard', $filearea, $itemid, $filepath, $filename);
    if (!$file)
    {
        return false; // The file does not exist.
    }

    // We can now send the file back to the browser - in this case with a cache lifetime of 1 day and no filtering.
    // From Moodle 2.3, use send_stored_file instead.

    send_stored_file($file, 86400, 0, $forcedownload, $options);
}

