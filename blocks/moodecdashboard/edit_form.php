<?php

/**
 * Version details
 *
 * @package    block_moodecdashboard
 * @copyright  2015 - Martin Tazlari (http://cyberlearn.hes-so.ch)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

/*
 * File for the block settings
 */

class block_moodecdashboard_edit_form extends block_edit_form {

    protected function specific_definition($mform) {

    }
}
