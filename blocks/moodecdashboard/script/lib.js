var anc_onglet = 'actual_course';
$( document ).ready(function() {
	
  change_onglet(anc_onglet);
});

function change_onglet(name)
{
    
	document.getElementById('onglet_'+anc_onglet).className = 'onglet_0 onglet';
    document.getElementById('onglet_'+name).className = 'onglet_1 onglet';
    document.getElementById('contenu_onglet_'+anc_onglet).style.display = 'none';
    document.getElementById('contenu_onglet_'+name).style.display = 'block';
    anc_onglet = name;
}

function displayprogress(id) {
    document.getElementById('progressvalue'+id).className = 'progressvaluebloc progressvaluevisible';
}

function hideprogress(id) {
    document.getElementById('progressvalue'+id).className = 'progressvaluebloc progressvalue';
}