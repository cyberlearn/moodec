<?php

/**
 * Version details
 *
 * @package     $this->content->text .= "Coucou";
 * @copyright  2015 - Martin Tazlari (http://cyberlearn.hes-so.ch)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

/*
 * Traduction file (English)
 */

$string['moodecdashboard:addinstance'] = 'Add a dashboard block';
$string['moodecdashboard:myaddinstance'] = 'Add a dashboard block to my moodle';
$string['pluginname'] = 'My dashboard';
$string['numbermoodecdashboard'] = "dashboard's number to display";
$string['coursename'] = "Course";

// Courses
$string['title_course'] = "My current Courses";

// Upcoming Course
$string['title_upcoming_course'] = "My upcoming Courses";

// Archived Courses
$string['title_archived_course'] = "My archived Courses";

$string['no_courses_message'] = "There is no courses in this section yet";
$string['see_catalogue'] = "See the Catalog";

$string['startdate']= "Start date : ";
$string['accesscours'] = 'Access to the course';



