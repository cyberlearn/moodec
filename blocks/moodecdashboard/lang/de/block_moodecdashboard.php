<?php

/**
 * Version details
 *
 * @package     $this->content->text .= "Coucou";
 * @copyright  2015 - Martin Tazlari (http://cyberlearn.hes-so.ch)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

/*
 * Traduction file (English)
 */

$string['moodecdashboard:addinstance'] = 'Eine Instanz zum Dashboard hinf�gen';
$string['moodecdashboard:myaddinstance'] = 'Ein Dashboard an meinem moodle hinf�gen';
$string['pluginname'] = 'Mein Dashboard';
$string['numbermoodecdashboard'] = "Anzahl von Dashboard zum anzeigen";
$string['coursename'] = "Kurs";

// Courses
$string['title_course'] = "Meine aktuelle  Kurse";

// Upcoming Course
$string['title_upcoming_course'] = "Meine kommenden Kurse";

// Archived Courses
$string['title_archived_course'] = "Meine archivierte Kurse";

$string['no_courses_message'] = "Es hat momentan kein Kurs in dieser Sektionn";
$string['see_catalogue'] = "Zum Katalog schauen ";

$string['startdate']= "Anfangsdatum : ";
$string['accesscours'] = 'Zugriff zum Kurs';


