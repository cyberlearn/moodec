<?php

/**
 * Version details
 *
 * @package     $this->content->text .= "Coucou";
 * @copyright  2015 - Martin Tazlari (http://cyberlearn.hes-so.ch)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

/*
* Traduction file (English)
*/

$string['moodecdashboard:addinstance'] = 'Ajouter une instance du dashboard';
$string['moodecdashboard:myaddinstance'] = 'Ajouter un dashboard à mon moodle';
$string['pluginname'] = 'Mon dashboard';
$string['numbermoodecdashboard'] = "Nombre de dashboard à afficher";
$string['coursename'] = "Cours";

// Courses
$string['title_course'] = "Mes cours actuels";

// Upcoming Course
$string['title_upcoming_course'] = "Mes cours à venir";

// Archived Courses
$string['title_archived_course'] = "Mes cours archivés";

$string['no_courses_message'] = "Il n'y a aucun cours dans cette section actuellement";
$string['see_catalogue'] = "Voir le catalogue";

$string['startdate']= "Date de début : ";
$string['accesscours'] = 'Accéder au cours';




