M.block_moodecprogress = {
    moodecprogressBarLast: new Array(),

    init: function (YUIObject, instances, users) {
        for (instance = 0; instance < instances.length; instance++) {
            for (user = 0; user < users.length; user++) {
                this.moodecprogressBarLast[instances[instance] + '-' + users[user]] = 'info';
            }
        }
    },

    showInfo: function (instance, user, id) {
        var last = this.moodecprogressBarLast[instance + '-' + user];
        document.getElementById('moodecprogressBarInfo' + instance + '-' + user + '-' + last).style.display = 'none';
        document.getElementById('moodecprogressBarInfo' + instance + '-' + user + '-' + id).style.display = 'block';
        this.moodecprogressBarLast[instance + '-' + user] = id;
    },
		
		inputCheckAll: function(s, des)
		{
			if($('#checkAllNone').is(':checked'))
			{
				checkall();
				$('#checkAllNone').attr('title', des);
			}
			else
			{
				checknone();
				$('#checkAllNone').attr('title', s);
			}
			
			
		}
};