<?php
/**
 * Created by PhpStorm.
 * User: luca.morganel
 * Date: 07.12.2015
 * Time: 15:48
 */

require_once('../../../config.php');
require_once('../lib.php');

/// get url variables
$id = optional_param('id', 0, PARAM_INT);


$PAGE->set_pagelayout('admin');



$records = display_group_ip_location($id);

echo json_encode($records);