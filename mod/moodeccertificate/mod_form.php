<?php

// This file is part of the moodeccertificate module for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
* Instance add/edit form
*
* @package    mod_moodeccertificate
* @copyright  Mark Nelson <markn@moodle.com>
* @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
*/

if (!defined('MOODLE_INTERNAL')) {
    die('Direct access to this script is forbidden.');    ///  It must be included from a Moodle page
}

require_once ($CFG->dirroot.'/course/moodleform_mod.php');
require_once($CFG->dirroot.'/mod/moodeccertificate/locallib.php');

class mod_moodeccertificate_mod_form extends moodleform_mod {

    function definition() {
        global $CFG;

        $mform =& $this->_form;

        $mform->addElement('header', 'general', get_string('general', 'form'));

        $mform->addElement('text', 'name', get_string('moodeccertificatename', 'moodeccertificate'), array('size'=>'64'));
        if (!empty($CFG->formatstringstriptags)) {
            $mform->setType('name', PARAM_TEXT);
        } else {
            $mform->setType('name', PARAM_CLEAN);
        }
        $mform->addRule('name', null, 'required', null, 'client');
        $mform->addRule('name', null, 'maxlength', '250', 'client');
	
		$this->standard_intro_elements();
      

        // Issue options
        $mform->addElement('header', 'issueoptions', get_string('issueoptions', 'moodeccertificate'));
        $ynoptions = array( 0 => get_string('no'), 1 => get_string('yes'));
        $mform->addElement('select', 'emailteachers', get_string('emailteachers', 'moodeccertificate'), $ynoptions);
        $mform->setDefault('emailteachers', 0);
        $mform->addHelpButton('emailteachers', 'emailteachers', 'moodeccertificate');

        $mform->addElement('text', 'emailothers', get_string('emailothers', 'moodeccertificate'), array('size'=>'40', 'maxsize'=>'200'));
        $mform->setType('emailothers', PARAM_TEXT);
        $mform->addHelpButton('emailothers', 'emailothers', 'moodeccertificate');

        $deliveryoptions = array( 0 => get_string('openbrowser', 'moodeccertificate'), 1 => get_string('download', 'moodeccertificate'));
        $mform->addElement('select', 'delivery', get_string('delivery', 'moodeccertificate'), $deliveryoptions);
        $mform->setDefault('delivery', 0);
        $mform->addHelpButton('delivery', 'delivery', 'moodeccertificate');

        $mform->addElement('select', 'savecert', get_string('savecert', 'moodeccertificate'), $ynoptions);
        $mform->setDefault('savecert', 0);
        $mform->addHelpButton('savecert', 'savecert', 'moodeccertificate');

        $reportfile = "$CFG->dirroot/moodeccertificates/index.php";
        if (file_exists($reportfile)) {
            $mform->addElement('select', 'reportcert', get_string('reportcert', 'moodeccertificate'), $ynoptions);
            $mform->setDefault('reportcert', 0);
            $mform->addHelpButton('reportcert', 'reportcert', 'moodeccertificate');
        }

        /*$mform->addElement('text', 'requiredtime', get_string('coursetimereq', 'moodeccertificate'), array('size'=>'3'));
        $mform->setType('requiredtime', PARAM_INT);
        $mform->addHelpButton('requiredtime', 'coursetimereq', 'moodeccertificate');*/

        // Text Options
        $mform->addElement('header', 'textoptions', get_string('textoptions', 'moodeccertificate'));

        $modules = moodeccertificate_get_mods();
        $dateoptions = moodeccertificate_get_date_options(); // + $modules;
        $mform->addElement('select', 'printdate', get_string('printdate', 'moodeccertificate'), $dateoptions);
        $mform->setDefault('printdate', 'N');
       // $mform->addHelpButton('printdate', 'printdate', 'moodeccertificate');

        /*$dateformatoptions = array( 1 => 'January 1, 2000', 2 => 'January 1st, 2000', 3 => '1 January 2000',
            4 => 'January 2000', 5 => get_string('userdateformat', 'moodeccertificate'));
        $mform->addElement('select', 'datefmt', get_string('datefmt', 'moodeccertificate'), $dateformatoptions);
        $mform->setDefault('datefmt', 0);
        $mform->addHelpButton('datefmt', 'datefmt', 'moodeccertificate');*/

        $mform->addElement('select', 'printnumber', get_string('printnumber', 'moodeccertificate'), $ynoptions);
        $mform->setDefault('printnumber', 0);
        $mform->addHelpButton('printnumber', 'printnumber', 'moodeccertificate');

        /*$gradeoptions = moodeccertificate_get_grade_options() ; // + moodeccertificate_get_grade_categories($this->current->course) + $modules;
        $mform->addElement('select', 'printgrade', get_string('printgrade', 'moodeccertificate'),$gradeoptions);
        $mform->setDefault('printgrade', 0);
        $mform->addHelpButton('printgrade', 'printgrade', 'moodeccertificate');*/

        /*$gradeformatoptions = array( 1 => get_string('gradepercent', 'moodeccertificate'), 2 => get_string('gradepoints', 'moodeccertificate'),
            3 => get_string('gradeletter', 'moodeccertificate'));
        $mform->addElement('select', 'gradefmt', get_string('gradefmt', 'moodeccertificate'), $gradeformatoptions);
        $mform->setDefault('gradefmt', 0);
        $mform->addHelpButton('gradefmt', 'gradefmt', 'moodeccertificate');*/

        /*$outcomeoptions = moodeccertificate_get_outcomes();
        $mform->addElement('select', 'printoutcome', get_string('printoutcome', 'moodeccertificate'),$outcomeoptions);
        $mform->setDefault('printoutcome', 0);
        $mform->addHelpButton('printoutcome', 'printoutcome', 'moodeccertificate');*/

       /* $mform->addElement('text', 'printhours', get_string('printhours', 'moodeccertificate'), array('size'=>'5', 'maxlength' => '255'));
        $mform->setType('printhours', PARAM_TEXT);
        $mform->addHelpButton('printhours', 'printhours', 'moodeccertificate');*/

        $mform->addElement('select', 'printteacher', get_string('printteacher', 'moodeccertificate'), $ynoptions);
        $mform->setDefault('printteacher', 0);
        $mform->addHelpButton('printteacher', 'printteacher', 'moodeccertificate');

        $mform->addElement('textarea', 'customtext', get_string('customtext', 'moodeccertificate'), array('cols'=>'40', 'rows'=>'4', 'wrap'=>'virtual'));
        $mform->setType('customtext', PARAM_RAW);
        $mform->addHelpButton('customtext', 'customtext', 'moodeccertificate');

        // Design Options
        $mform->addElement('header', 'designoptions', get_string('designoptions', 'moodeccertificate'));
        $mform->addElement('select', 'moodeccertificatetype', get_string('moodeccertificatetype', 'moodeccertificate'), moodeccertificate_types());
        $mform->setDefault('moodeccertificatetype', 'A4_non_embedded');
        $mform->addHelpButton('moodeccertificatetype', 'moodeccertificatetype', 'moodeccertificate');
	
        /*$orientation = array( 'L' => get_string('landscape', 'moodeccertificate'), 'P' => get_string('portrait', 'moodeccertificate'));
        $mform->addElement('select', 'orientation', get_string('orientation', 'moodeccertificate'), $orientation);
        $mform->setDefault('orientation', 'L');
        $mform->addHelpButton('orientation', 'orientation', 'moodeccertificate');*/

        $mform->addElement('select', 'borderstyle', get_string('borderstyle', 'moodeccertificate'), moodeccertificate_get_images(CERT_IMAGE_BORDER));
        $mform->setDefault('borderstyle', '0');
        $mform->addHelpButton('borderstyle', 'borderstyle', 'moodeccertificate');

        $printframe = array( 0 => get_string('no'), 1 => get_string('borderblack', 'moodeccertificate'), 2 => get_string('borderbrown', 'moodeccertificate'),
            3 => get_string('borderblue', 'moodeccertificate'), 4 => get_string('bordergreen', 'moodeccertificate'));
        $mform->addElement('select', 'bordercolor', get_string('bordercolor', 'moodeccertificate'), $printframe);
        $mform->setDefault('bordercolor', '0');
        $mform->addHelpButton('bordercolor', 'bordercolor', 'moodeccertificate');

        $mform->addElement('select', 'printwmark', get_string('printwmark', 'moodeccertificate'), moodeccertificate_get_images(CERT_IMAGE_WATERMARK));
        $mform->setDefault('printwmark', '0');
        $mform->addHelpButton('printwmark', 'printwmark', 'moodeccertificate');

        $mform->addElement('select', 'printseal', get_string('printseal', 'moodeccertificate'), moodeccertificate_get_images(CERT_IMAGE_SEAL));
        $mform->setDefault('printseal', '0');
        $mform->addHelpButton('printseal', 'printseal', 'moodeccertificate');

        $mform->addElement('selectyesno', 'printsignature', get_string('printsignature', 'moodeccertificate'));
        $mform->setDefault('printsignature', '0');
        $mform->addHelpButton('printsignature', 'printsignature', 'moodeccertificate');


        $mform->addElement('filepicker', 'moodeccertificatesignature', '', null,
            array('accepted_types' => array('image')));
        $mform->disabledIf('moodeccertificatesignature', 'printsignature', 'eq', 0);


        $this->standard_coursemodule_elements();

        $this->add_action_buttons();
    }

    /**
     * Some basic validation
     *
     * @param $data
     * @param $files
     * @return array
     */
    public function validation($data, $files) {
        $errors = parent::validation($data, $files);

        // Check that the required time entered is valid
      //  if ((!is_number($data['requiredtime']) || $data['requiredtime'] < 0)) {
      //      $errors['requiredtime'] = get_string('requiredtimenotvalid', 'moodeccertificate');
      //  }

        return $errors;
    }
}
