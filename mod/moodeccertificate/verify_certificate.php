<?php
require_once("../../config.php");
require_once("$CFG->dirroot/mod/moodeccertificate/locallib.php");
require_once("$CFG->dirroot/mod/moodeccertificate/deprecatedlib.php");
require_once("$CFG->libdir/pdflib.php");

$title = get_string("verifymoodeccertificate", "moodeccertificate");
$PAGE->set_url('/mod/moodeccertificate/verify_certificate.php');
echo $OUTPUT->header();


echo "    <div id=\"frmCertificate\">\n"; 
echo "		<h1 id=\"titleCertificate\">" . get_string('verifymoodeccertificate', 'moodeccertificate'). "</h1>\n"; 
echo '<div id="info"></div>'; 
echo '<div id="infoCertificate">'
				.'<div class="formGroup">'
						.'<label>'.get_string('codeverify', 'moodeccertificate').'</label><span id="info_verify_certificate"></span>'
				.'</div>'
				.'<div class="formGroup">'
						.'<label>'.get_string('toverify', 'moodeccertificate').'</label><span id="info_user_certificate"></span>'
				.'</div>'
				.'<div class="formGroup">'
						.'<label >'.get_string('courseverify', 'moodeccertificate').'</label><span id="info_course_certificate"></span>'
				.'</div>'
				.'<div class="formGroup">'
						.'<label>'. get_string('myrecievedate', 'moodeccertificate').'</label><span id="info_date_certificate"></span>'
				.'</div>'
				.'<div class="formGroup">'
						.'<label >'. get_string('gradeverify', 'moodeccertificate').'</label><span id="info_grade_certificate"></span>'
				.'</div>'
		.'</div>';

		echo '<div id="frm">'
					.'<div class="formGroup">' 
						.'<label>'. get_string('entercode', 'moodeccertificate') . '</label>'
						.'<input type="text" name="certnumber" id="certnumber" size="10" value="" class="formInput">'
					.'</div>'
				.'</div>'; 
		echo '<button name="submit" class="btnAction" onClick="verifyCertificate();">'.   get_string('validate', 'moodeccertificate').'</button>'; 


	echo '</div>'; 
echo '	<input type="hidden" id="validCertificate" value="'. get_string("validcertificate", "moodeccertificate").'">'; 
echo '  <input type="hidden" id="errorCertificate" value="'. get_string("error", "moodeccertificate").'"/>'; 
echo '	<input type="hidden" id="mandatoryfield" value="'.get_string("mandatoryfield", "moodeccertificate").'">';
echo '	<input type="hidden" id="rootMoodec" value="' . new moodle_url('/') . '"/>'; 


echo $OUTPUT->footer();
echo '<script>';
include_once 'moodeccertificates.js';
echo '</script>';

?>