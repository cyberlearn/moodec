<?php

// This file is part of the moodeccertificate module for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @package    mod_moodeccertificate
 * @subpackage backup-moodle2
 * @copyright 2010 onwards Eloy Lafuente (stronk7) {@link http://stronk7.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

/**
 * Define all the restore steps that will be used by the restore_moodeccertificate_activity_task
 */

/**
 * Structure step to restore one moodeccertificate activity
 */
class restore_moodeccertificate_activity_structure_step extends restore_activity_structure_step {

    protected function define_structure() {

        $paths = array();
        $userinfo = $this->get_setting_value('userinfo');

        $paths[] = new restore_path_element('moodeccertificate', '/activity/moodeccertificate');

        if ($userinfo) {
            $paths[] = new restore_path_element('moodeccertificate_issue', '/activity/moodeccertificate/issues/issue');
        }

        // Return the paths wrapped into standard activity structure
        return $this->prepare_activity_structure($paths);
    }

    protected function process_moodeccertificate($data) {
        global $DB;

        $data = (object)$data;
        $oldid = $data->id;
        $data->course = $this->get_courseid();
        $data->timecreated = $this->apply_date_offset($data->timecreated);
        $data->timemodified = $this->apply_date_offset($data->timemodified);

        // insert the moodeccertificate record
        $newitemid = $DB->insert_record('moodeccertificate', $data);
        // immediately after inserting "activity" record, call this
        $this->apply_activity_instance($newitemid);
    }

    protected function process_moodeccertificate_issue($data) {
        global $DB;

        $data = (object)$data;
        $oldid = $data->id;

        $data->moodeccertificateid = $this->get_new_parentid('moodeccertificate');
        $data->timecreated = $this->apply_date_offset($data->timecreated);

        $newitemid = $DB->insert_record('moodeccertificate_issues', $data);
        $this->set_mapping('moodeccertificate_issue', $oldid, $newitemid);
    }

    protected function after_execute() {
        // Add moodeccertificate related files, no need to match by itemname (just internally handled context)
        $this->add_related_files('mod_moodeccertificate', 'issue', 'moodeccertificate_issue');
    }
}
