<?php

// This file is part of the moodeccertificate module for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * moodeccertificate module core interaction API
 *
 * @package    mod_moodeccertificate
 * @copyright  Mark Nelson <markn@moodle.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

/**
* Get Certificate by USER ID
*/
function get_user_certificates() {
	
    global $DB, $USER;
	//$idUser = 2;

	$list = array();
	
	$certificates = $DB->get_records_sql("SELECT mc.id, md.picture, c.fullname, mci.timecreated, cm.id as cmid, c.id as courseid
                                    FROM mdl_moodeccertificate_issues mci, mdl_moodeccertificate mc, mdl_course c, mdl_moodecdescription md, mdl_course_modules cm
                                    WHERE mc.id = mci.moodeccertificateid
									AND mc.course = c.id
									AND c.id = md.course
									AND cm.course = c.id
									AND module = ?
									AND userid = ?", array(30,$USER->id));
    
	$certificates_dispo = array_values($certificates);
    $count_certificates_dispo = count($certificates_dispo);
    
	for($i = 0 ; $i < $count_certificates_dispo ; $i++){
		$certificate = new stdClass();
		$certificate  -> id = $certificates_dispo[$i] -> {'id'};
		$certificate  -> picture = $certificates_dispo[$i] -> {'picture'};
        $certificate  -> fullname = $certificates_dispo[$i] -> {'fullname'};
        $certificate  -> timecreated = $certificates_dispo[$i] -> {'timecreated'};
		$certificate  -> cmid = $certificates_dispo[$i] -> {'cmid'};
		$certificate  -> courseid = $certificates_dispo[$i] -> {'courseid'};
		array_push($list, $certificate);
	}

    return $list;
}

/**
 * Add moodeccertificate instance.
 *
 * @param stdClass $moodeccertificate
 * @return int new moodeccertificate instance id
 */
function moodeccertificate_add_instance($moodeccertificate, mod_moodeccertificate_mod_form $mform = null) {
    global $DB, $CFG;


if($moodeccertificate->printsignature==1){
	
    $filename = $mform->get_new_filename('moodeccertificatesignature');

    if($filename){
        $extension = substr($filename, strrpos($filename, '.') + 1);
        $name = time()."signature.".$extension;
        $uploaddir = "mod/moodeccertificate/pix/signatures/" ;
        make_upload_directory($uploaddir);
        $destination = $CFG->dataroot . '/' .$uploaddir.'/' . $name;
        $mform->save_file('moodeccertificatesignature', $destination, true);

        // Create the moodeccertificate.
        $moodeccertificate->printsignature = $name;
    }
}
	
	// Default Options
    $moodeccertificate->orientation = "L";
    $moodeccertificate->datefmt = 5 ;
    //$moodeccertificate->printdate = 1 ;
    //$moodeccertificate->printgrade = 1 ;
    $moodeccertificate->printoutcome = 0 ;
    $moodeccertificate->printhours = 0 ;
    $moodeccertificate->requiredtime = 0 ;
    $moodeccertificate->gradefmt = 0 ;
    $moodeccertificate->printgrade = 0 ;
    //$moodeccertificate->moodeccertificatetype = "A4_non_embedded";
	
    $moodeccertificate->timecreated = time();
    $moodeccertificate->timemodified = $moodeccertificate->timecreated;

    return $DB->insert_record('moodeccertificate', $moodeccertificate);
}

/**
 * Update moodeccertificate instance.
 *
 * @param stdClass $moodeccertificate
 * @return bool true
 */
function moodeccertificate_update_instance($moodeccertificate, mod_moodeccertificate_mod_form $mform = null) {
    global $DB, $CFG;

    deletefile($moodeccertificate->instance) ;
if($moodeccertificate->printsignature==1){
    $filename = $mform->get_new_filename('moodeccertificatesignature');

    if($filename){

    $extension = substr($filename, strrpos($filename, '.') + 1);
    $name = time() . "signature." . $extension;
        $uploaddir = "mod/moodeccertificate/pix/signatures/";
        make_upload_directory($uploaddir);
        chmod($uploaddir, 777);
        $destination = $CFG->dataroot . '/' . $uploaddir . '/' . $name;
    $mform->save_file('moodeccertificatesignature', $destination, true);
    // Create the moodeccertificate.
    $moodeccertificate->printsignature = $name;
} else {
        $moodeccertificate->printsignature = 0 ;
    }
}	

	// Default Options
    $moodeccertificate->orientation = "L";
    $moodeccertificate->datefmt = 5 ;
    //$moodeccertificate->printdate = 1 ;
    //$moodeccertificate->printgrade = 1 ;
    $moodeccertificate->printoutcome = 0 ;
    $moodeccertificate->printhours = 0 ;
    $moodeccertificate->requiredtime = 0 ;
    $moodeccertificate->gradefmt = 0 ;
    $moodeccertificate->printgrade = 0 ;
   // $moodeccertificate->moodeccertificatetype = "A4_non_embedded";
	
    // Update the moodeccertificate.
    $moodeccertificate->timemodified = time();
    $moodeccertificate->id = $moodeccertificate->instance;

    return $DB->update_record('moodeccertificate', $moodeccertificate);
}

function deletefile($id){
    global $DB, $CFG;

    $param = array($id);
    $query = "SELECT printsignature FROM {moodeccertificate} WHERE id= ? ";
    $signatureid = $DB->get_fieldset_sql($query, $param);


    $uploaddir = "mod/moodeccertificate/pix/signatures/";
    $destination = $CFG->dataroot . '/' . $uploaddir . '/' . $signatureid[0];

    if($signatureid[0]){
        unlink($destination);
    }



}

/**
 * Given an ID of an instance of this module,
 * this function will permanently delete the instance
 * and any data that depends on it.
 *
 * @param int $id
 * @return bool true if successful
 */
function moodeccertificate_delete_instance($id) {
    global $DB;

    // Ensure the moodeccertificate exists
    if (!$moodeccertificate = $DB->get_record('moodeccertificate', array('id' => $id))) {
        return false;
    }

    // Prepare file record object
    if (!$cm = get_coursemodule_from_instance('moodeccertificate', $id)) {
        return false;
    }



    $result = true;

    deletefile($id);

    $DB->delete_records('moodeccertificate_issues', array('moodeccertificateid' => $id));
    if (!$DB->delete_records('moodeccertificate', array('id' => $id))) {
        $result = false;
    }

    // Delete any files associated with the moodeccertificate
    $context = context_module::instance($cm->id);
    $fs = get_file_storage();
    $fs->delete_area_files($context->id);

    return $result;
}

/**
 * This function is used by the reset_course_userdata function in moodlelib.
 * This function will remove all posts from the specified moodeccertificate
 * and clean up any related data.
 *
 * Written by Jean-Michel Vedrine
 *
 * @param $data the data submitted from the reset course.
 * @return array status array
 */
function moodeccertificate_reset_userdata($data) {
    global $DB;

    $componentstr = get_string('modulenameplural', 'moodeccertificate');
    $status = array();

    if (!empty($data->reset_moodeccertificate)) {
        $sql = "SELECT cert.id
                  FROM {moodeccertificate} cert
                 WHERE cert.course = :courseid";
        $params = array('courseid' => $data->courseid);
        $moodeccertificates = $DB->get_records_sql($sql, $params);
        $fs = get_file_storage();
        if ($moodeccertificates) {
            foreach ($moodeccertificates as $certid => $unused) {
                if (!$cm = get_coursemodule_from_instance('moodeccertificate', $certid)) {
                    continue;
                }
                $context = context_module::instance($cm->id);
                $fs->delete_area_files($context->id, 'mod_moodeccertificate', 'issue');
            }
        }

        $DB->delete_records_select('moodeccertificate_issues', "moodeccertificateid IN ($sql)", $params);
        $status[] = array('component' => $componentstr, 'item' => get_string('removecert', 'moodeccertificate'), 'error' => false);
    }
    // Updating dates - shift may be negative too
    if ($data->timeshift) {
        shift_course_mod_dates('moodeccertificate', array('timeopen', 'timeclose'), $data->timeshift, $data->courseid);
        $status[] = array('component' => $componentstr, 'item' => get_string('datechanged'), 'error' => false);
    }

    return $status;
}

/**
 * Implementation of the function for printing the form elements that control
 * whether the course reset functionality affects the moodeccertificate.
 *
 * Written by Jean-Michel Vedrine
 *
 * @param $mform form passed by reference
 */
function moodeccertificate_reset_course_form_definition(&$mform) {
    $mform->addElement('header', 'moodeccertificateheader', get_string('modulenameplural', 'moodeccertificate'));
    $mform->addElement('advcheckbox', 'reset_moodeccertificate', get_string('deletissuedmoodeccertificates', 'moodeccertificate'));
}

/**
 * Course reset form defaults.
 *
 * Written by Jean-Michel Vedrine
 *
 * @param stdClass $course
 * @return array
 */
function moodeccertificate_reset_course_form_defaults($course) {
    return array('reset_moodeccertificate' => 1);
}

/**
 * Returns information about received moodeccertificate.
 * Used for user activity reports.
 *
 * @param stdClass $course
 * @param stdClass $user
 * @param stdClass $mod
 * @param stdClass $moodeccertificate
 * @return stdClass the user outline object
 */
function moodeccertificate_user_outline($course, $user, $mod, $moodeccertificate) {
    global $DB;

    $result = new stdClass;
    if ($issue = $DB->get_record('moodeccertificate_issues', array('moodeccertificateid' => $moodeccertificate->id, 'userid' => $user->id))) {
        $result->info = get_string('issued', 'moodeccertificate');
        $result->time = $issue->timecreated;
    } else {
        $result->info = get_string('notissued', 'moodeccertificate');
    }

    return $result;
}

/**
 * Returns information about received moodeccertificate.
 * Used for user activity reports.
 *
 * @param stdClass $course
 * @param stdClass $user
 * @param stdClass $mod
 * @param stdClass $moodeccertificate
 * @return string the user complete information
 */
function moodeccertificate_user_complete($course, $user, $mod, $moodeccertificate) {
    global $DB, $OUTPUT, $CFG;
    require_once($CFG->dirroot.'/mod/moodeccertificate/locallib.php');

    if ($issue = $DB->get_record('moodeccertificate_issues', array('moodeccertificateid' => $moodeccertificate->id, 'userid' => $user->id))) {
        echo $OUTPUT->box_start();
        echo get_string('issued', 'moodeccertificate') . ": ";
        echo userdate($issue->timecreated);
        $cm = get_coursemodule_from_instance('moodeccertificate', $moodeccertificate->id, $course->id);
        moodeccertificate_print_user_files($moodeccertificate, $user->id, context_module::instance($cm->id)->id);
        echo '<br />';
        echo $OUTPUT->box_end();
    } else {
        print_string('notissuedyet', 'moodeccertificate');
    }
}

/**
 * Must return an array of user records (all data) who are participants
 * for a given instance of moodeccertificate.
 *
 * @param int $moodeccertificateid
 * @return stdClass list of participants
 */
function moodeccertificate_get_participants($moodeccertificateid) {
    global $DB;

    $sql = "SELECT DISTINCT u.id, u.id
              FROM {user} u, {moodeccertificate_issues} a
             WHERE a.moodeccertificateid = :moodeccertificateid
               AND u.id = a.userid";
    return  $DB->get_records_sql($sql, array('moodeccertificateid' => $moodeccertificateid));
}

/**
 * @uses FEATURE_GROUPS
 * @uses FEATURE_GROUPINGS
 * @uses FEATURE_GROUPMEMBERSONLY
 * @uses FEATURE_MOD_INTRO
 * @uses FEATURE_COMPLETION_TRACKS_VIEWS
 * @uses FEATURE_GRADE_HAS_GRADE
 * @uses FEATURE_GRADE_OUTCOMES
 * @param string $feature FEATURE_xx constant for requested feature
 * @return mixed True if module supports feature, null if doesn't know
 */
function moodeccertificate_supports($feature) {
    switch ($feature) {
        case FEATURE_GROUPS:                  return true;
        case FEATURE_GROUPINGS:               return true;
        case FEATURE_GROUPMEMBERSONLY:        return true;
        case FEATURE_MOD_INTRO:               return true;
        case FEATURE_COMPLETION_TRACKS_VIEWS: return true;
        case FEATURE_BACKUP_MOODLE2:          return true;

        default: return null;
    }
}

/**
 * Serves moodeccertificate issues and other files.
 *
 * @param stdClass $course
 * @param stdClass $cm
 * @param stdClass $context
 * @param string $filearea
 * @param array $args
 * @param bool $forcedownload
 * @return bool|nothing false if file not found, does not return anything if found - just send the file
 */
function moodeccertificate_pluginfile($course, $cm, $context, $filearea, $args, $forcedownload) {
    global $CFG, $DB, $USER;

    if ($context->contextlevel != CONTEXT_MODULE) {
        return false;
    }

    if (!$moodeccertificate = $DB->get_record('moodeccertificate', array('id' => $cm->instance))) {
        return false;
    }

    require_login($course, false, $cm);

    require_once($CFG->libdir.'/filelib.php');

    if ($filearea === 'issue') {
        $certrecord = (int)array_shift($args);

        if (!$certrecord = $DB->get_record('moodeccertificate_issues', array('id' => $certrecord))) {
            return false;
        }

        if ($USER->id != $certrecord->userid and !has_capability('mod/moodeccertificate:manage', $context)) {
            return false;
        }

        $relativepath = implode('/', $args);
        $fullpath = "/{$context->id}/mod_moodeccertificate/issue/$certrecord->id/$relativepath";

        $fs = get_file_storage();
        if (!$file = $fs->get_file_by_hash(sha1($fullpath)) or $file->is_directory()) {
            return false;
        }
        send_stored_file($file, 0, 0, true); // download MUST be forced - security!
    }
}

/**
 * Used for course participation report (in case moodeccertificate is added).
 *
 * @return array
 */
function moodeccertificate_get_view_actions() {
    return array('view', 'view all', 'view report');
}

/**
 * Used for course participation report (in case moodeccertificate is added).
 *
 * @return array
 */
function moodeccertificate_get_post_actions() {
    return array('received');
}
