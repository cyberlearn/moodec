<?php
/**
 * Created by PhpStorm.
 * User: Fabio
 * Date: 25.09.2015
 * Time: 15:56
 */

/**
 * Structure step to restore one moodecforum activity
 */
class restore_moodecforum_activity_structure_step extends restore_activity_structure_step {

    protected function define_structure() {

        $paths = array();
        $userinfo = $this->get_setting_value('userinfo');

        $paths[] = new restore_path_element('moodecforum', '/activity/moodecforum');
        $paths[] = new restore_path_element('moodecforum_categorie', '/activity/moodecforum/categories/categorie');
        $paths[] = new restore_path_element('moodecforum_post', '/activity/moodecforum/categories/categorie/posts/post');
        $paths[] = new restore_path_element('moodecforum_userpoint', '/activity/moodecforum/userpoints/userpoint');

        // Return the paths wrapped into standard activity structure
        return $this->prepare_activity_structure($paths);
    }

    protected function process_moodecforum($data) {
        global $DB;

        $data = (object)$data;
        $oldid = $data->id;
        $data->course = $this->get_courseid();
        $data->timemodified = $this->apply_date_offset($data->timemodified);
        // insert the moodecforum record
        $newitemid = $DB->insert_record('moodecforum', $data);
        // immediately after inserting "activity" record, call this
        $this->apply_activity_instance($newitemid);
    }

    protected function process_moodecforum_categorie($data) {
        global $DB;

        $data = (object)$data;
        $oldid = $data->id;
        $newitemid = $DB->insert_record('moodecforum_categories', $data);
        $this->set_mapping('moodecforum_categorie', $oldid, $newitemid);
        $data->tags = 'c'.$newitemid;
        $data->backpath = 'c'.$newitemid;
        $data->id = $newitemid;
        $data->categoryid = $newitemid;
        $data->moodecforumid = $this->get_new_parentid('moodecforum');
        $DB->update_record('moodecforum_categories', $data,  $bulk=false);
    }

    protected function process_moodecforum_post($data)
    {
        global $DB;
/*
        $data = (object)$data;
      //  $data->postid = $data->id ;
        $oldid = $data->postid;

        error_log(print_r($data,true));
        error_log('posts 2: ', 0);
        error_log('data 2 : '.var_dump($data), 0);



        $data->posts = $this->get_new_parentid('moodecforum_post');
        $newitemid = $DB->insert_record('moodecforum_posts', $data ,$returnid=true, $primarykey='postid');
        $this->set_mapping('moodecforum_post', $oldid, $newitemid,true);*/
    }

    protected function process_moodecforum_userpoint($data) {
        global $DB;
      /*  error_log(var_dump($data), 0);
        $data = (object)$data;
        $oldid = $data->userid;

        $data->moodecforumpostid = $this->get_mappingid('moodecforum_post', $data->moodecforumtagwordid);

        $newitemid = $DB->insert_record('moodecforum_userpoints', $data);
        $this->set_mapping('moodecforum_userpoint', $oldid, $newitemid);*/
    }


    protected function after_execute() {
        // Add moodecforum related files, no need to match by itemname (just internally handled context)
        //$this->add_related_files('mod_moodecforum', 'intro', null);
    }
}