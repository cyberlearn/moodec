<?php
/**
 * Created by PhpStorm.
 * User: Fabio
 * Date: 25.09.2015
 * Time: 14:48
 * Backup moodecforum activity task
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot . '/mod/moodecforum/backup/moodle2/backup_moodecforum_stepslib.php');
require_once($CFG->dirroot . '/mod/moodecforum/backup/moodle2/backup_moodecforum_settingslib.php');

/**
 * Provides the steps to perform one complete backup of the forum instance
 */
class backup_moodecforum_activity_task extends backup_activity_task {

    /**
     * No specific settings for this activity
     */
    protected function define_my_settings() {
    }

    /**
     * Defines a backup step to store the instance data in the moodecforum.xml file
     */
    protected function define_my_steps() {
        $this->add_step(new backup_moodecforum_activity_structure_step('moodecforum_structure', 'moodecforum.xml'));
    }

    /**
     * Encodes URLs to the index.php and view.php scripts
     *
     * @param string $content some HTML text that eventually contains URLs to the activity instance scripts
     * @return string the content with the URLs encoded
     */
    static public function encode_content_links($content) {
        global $CFG;

        $base = preg_quote($CFG->wwwroot,"/");

        // Link to the list of forums
        $search="/(".$base."\/mod\/moodecforum\/index.php\?id\=)([0-9]+)/";
        $content= preg_replace($search, '$@MOODECFORUMINDEX*$2@$', $content);

        // Link to forum view by moduleid
        $search="/(".$base."\/mod\/moodecforum\/view.php\?id\=)([0-9]+)/";
        $content= preg_replace($search, '$@MOODECFORUMVIEWBYID*$2@$', $content);

        return $content;
    }
}