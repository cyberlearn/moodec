<?php
/**
 * Created by PhpStorm.
 * User: Fabio
 * Date: 25.09.2015
 * Time: 15:18
 */

/**
 * Define the complete forum structure for backup, with file and id annotations
 */
class backup_moodecforum_activity_structure_step extends backup_activity_structure_step {

    protected function define_structure() {

        // To know if we are including userinfo
        $userinfo = $this->get_setting_value('userinfo');

        // Define each element separated
       $moodecforum = new backup_nested_element('moodecforum', array('id'), array(
            'course', 'name', 'intro', 'introformat',
            'timecreated', 'timemodified'));

        $categories = new backup_nested_element('categories');

        $categorie = new backup_nested_element('categorie', array('id'), array(
            'categoryid', 'parentid', 'moodecforumid', 'tags', 'content', 'qcount',
            'position', 'backpath'));

        $moodecforum->add_child($categories);
        $categories->add_child($categorie);

        $moodecforum->set_source_table('moodecforum', array('id' => backup::VAR_ACTIVITYID));

        $categorie->set_source_sql('
            SELECT *
            FROM {moodecforum_categories}
            WHERE moodecforumid = ?
            ORDER BY id',
            array(backup::VAR_PARENTID));

        // Define file annotations
        $moodecforum->annotate_files('mod_moodecforum', 'intro', null); // This file area hasn't itemid

        // Return the root element (moodecforum), wrapped into standard activity structure
        return $this->prepare_activity_structure($moodecforum);
    }
}