<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.


/**
 * English strings for moodecdescription
 *
 * You can have a rather longer description of the file as well,
 * if you like, and it can span multiple lines.
 *
 * @package    mod_moodecdescription
 * @copyright  2011 Your Name
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['modulename'] = 'Moodec Beschreibung';
$string['name'] = 'Name der Beschreibung';
$string['coursename'] = "Name des Kurses";
$string['courseshortname'] = "Kurzname des Kurses ";
$string['moodecdescriptionname'] = 'Anstalt';
$string['thumbnail'] = 'Kurs Bild ';
$string['course_summary'] = 'Kurs Inhalt';
$string['efforthours'] = "Leistung pro Woche (Stunden)";
$string['courseduration'] = "Kurs Dauer";
$string['moodecdescription:addinstance'] = "Eine Instanz des Moodec�s Beschreibung hinf�gen";
$string['moodecdescription:submit'] = "Eine Moodec Beschreibung senden (best�tigen)";
$string['moodecdescription:view'] = "Kurs Beschreibung anschauen";
$string['header_informations'] = 'Fortgeschritten';
$string['syllabuseditor'] = 'Programm';
$string['syllabuseditor_help'] = 'Hilfe Programm';
$string['prerequisiteeditor'] = 'Voraussetzung';
$string['prerequisiteeditor_help'] = 'Hilfe Voraussetzung';
$string['faqeditor'] = 'FAQ';
$string['faqeditor_help'] = 'Hilfe FAQ';
$string['readingeditor'] = 'Lesungen';
$string['readingeditor_help'] = 'Hilfe Lesungen';
$string['modulenameplural'] = 'Moodec Beschreibung';
$string['modulename_help'] = 'Verwendung des Plugin moodecbeschreibung um�| Da Plugin moodecbeschreibung  plugin moodecdescription erlaubt...';
$string['moodecdescriptionfieldset'] = 'Gesamtheit der benutzerdefinierten Felder';
$string['moodecdescriptionname_help'] = 'Das ist der Inhalt der Hilfe Blase des verbundenes Feld Name von moodecbeschreibung. Bemerken Sie, dass die Syntax unterst�tzt ist.';
$string['moodecdescription'] = 'Moodec Beschreibung';
$string['pluginadministration'] = 'Verwaltung moodecbeschreibung';
$string['pluginname'] = 'Moodec Beschreibung';
$string['weektime'] = 'S/Wo';
$string['week'] = 'Wochen';
$string['errorinstance'] = 'Es gibt schon eine Instanz f�r eine Aktivit�t in diesem Kurs, Sie werden weitergeleited';
$string['erroraccess'] = 'Nur die Verwalter k�nnen diese Aktivit�t �ndern';
$string['institutionheader'] = 'Anstalt';
$string['effortheader'] = 'Leistung';
$string['durationheader'] = 'Dauer';
$string['coursesummaryheader'] = 'Kurs Inhalt';
$string['teacherheader'] = 'Dozenten';
$string['video'] = 'Iframe Video';
$string['coursetags'] = 'Zeichen';
$string['coursetags_help'] = 'Zeichen getrennt mit Komas';
$string['endcoursedate'] = 'Datum des Kurs Ende';
$string['reset'] = 'Zur�ckstellung der Kursbeschreibung';
$string['teacher'] = 'Dozent';
