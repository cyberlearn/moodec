<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.


/**
 * English strings for moodecdescription
 *
 * You can have a rather longer description of the file as well,
 * if you like, and it can span multiple lines.
 *
 * @package    mod_moodecdescription
 * @copyright  2011 Your Name
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['modulename'] = 'Moodec Description';
$string['name'] = 'Description name';
$string['coursename'] = "Course's name";
$string['courseshortname'] = "Course's short name";
$string['moodecdescriptionname'] = 'Institution';
$string['thumbnail'] = 'Course picture';
$string['course_summary'] = 'Course summary';
$string['efforthours'] = 'Effort in hours per week';
$string['courseduration'] = "Course's duration";
$string['moodecdescription:addinstance'] = "Add a Moodec description";
$string['moodecdescription:submit'] = "Submit a Moodec description";
$string['moodecdescription:view'] = "See the course description";
$string['header_informations'] = 'Advanced';
$string['syllabuseditor'] = 'Syllabus';
$string['syllabuseditor_help'] = 'Syllabus help';
$string['prerequisiteeditor'] = 'Prerequisite';
$string['prerequisiteeditor_help'] = 'prerequisite help';
$string['faqeditor'] = 'FAQ';
$string['faqeditor_help'] = 'FAQ help';
$string['readingeditor'] = 'Readings';
$string['readingeditor_help'] = 'Readings help';




$string['modulenameplural'] = 'Moodecdescriptions';
$string['modulename_help'] = 'Use the moodecdescription module for... | The moodecdescription module allows...';
$string['moodecdescriptionfieldset'] = 'Custom example fieldset';

$string['moodecdescriptionname_help'] = 'This is the content of the help tooltip associated with the moodecdescription name field. Markdown syntax is supported.';
$string['moodecdescription'] = 'moodecdescription';
$string['pluginadministration'] = 'moodecdescription administration';
$string['pluginname'] = 'moodecdescription';
$string['weektime'] = 'h/week';
$string['week'] = 'week';


$string['errorinstance'] = 'There is already an instance of the activity in this course, you will be redirected';
$string['erroraccess'] = 'Only Administrator can modify this activity';

$string['institutionheader'] = 'Institution';
$string['effortheader'] = 'Effort';
$string['durationheader'] = 'Duration';
$string['coursesummaryheader'] = 'Course summary';
$string['teacherheader'] = 'Teachers';
$string['video'] = 'Iframe video';
$string['coursetags'] = 'Tags';
$string['coursetags_help'] = 'Course tags (separated by comma)';
$string['endcoursedate'] = 'End course date';
$string['reset'] = 'Reset course description';
$string['teacher'] = 'Teacher';