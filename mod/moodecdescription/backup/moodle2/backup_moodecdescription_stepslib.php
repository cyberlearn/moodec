<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @package   mod_page
 * @category  backup
 * @copyright 2010 onwards Eloy Lafuente (stronk7) {@link http://stronk7.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die;

/**
 * Define all the backup steps that will be used by the backup_page_activity_task
 */

/**
 * Define the complete page structure for backup, with file and id annotations
 */
class backup_moodecdescription_activity_structure_step extends backup_activity_structure_step {

    protected function define_structure() {

        // To know if we are including userinfo
       // $userinfo = $this->get_setting_value('userinfo');

        // Define each element separated
        $moodecdescription = new backup_nested_element('moodecdescription', array('id'), array(
            'course','name', 'intro', 'introformat','institution','picture','video','effort',
            'endcoursedate','duration','prerequisite','prerequisiteformat',
            'syllabus','syllabusformat','reading','readingformat','faq','faqformat',
            'tags','timecreated', 'timemodified'));

        // Build the tree
        // (love this)

        // Define sources
        $moodecdescription->set_source_table('moodecdescription', array('id' => backup::VAR_ACTIVITYID));

        // Define id annotations
        // (none)

        // Define file annotations
        $moodecdescription->annotate_files('mod_moodecdescription', 'intro', null); // This file areas haven't itemid
        $moodecdescription->annotate_files('mod_moodecdescription', 'prerequisite', null); // This file areas haven't itemid
        $moodecdescription->annotate_files('mod_moodecdescription', 'syllabus', null); // This file areas haven't itemid
        $moodecdescription->annotate_files('mod_moodecdescription', 'reading', null); // This file areas haven't itemid
        $moodecdescription->annotate_files('mod_moodecdescription', 'faq', null); // This file areas haven't itemid

        // Return the root element (moodecdescription), wrapped into standard activity structure
        return $this->prepare_activity_structure($moodecdescription);
    }
}
