<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.
/**
 * The main moodecdescription configuration form
 *
 * It uses the standard core Moodle formslib. For more info about them, please
 * visit: http://docs.moodle.org/en/Development:lib/formslib.php
 *
 * @package    mod_moodecdescription
 * @copyright  2011 Your Name
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot.'/course/moodleform_mod.php');
require_once($CFG->dirroot.'/mod/moodecdescription/locallib.php');

/**
 * Module instance settings form
 */
class mod_moodecdescription_mod_form extends moodleform_mod {


    /**
     * Defines forms elements
     */
    public function definition() {
        global $CFG, $COURSE ;

        $mform = $this->_form;
error_reporting(0);
        // Adding the "general" fieldset, where all the common settings are showed.
        $mform->addElement('header', 'general', get_string('general', 'form'));

        $mform->addElement('text', 'name', get_string('name', 'moodecdescription'), array('size'=>'50'));
        if (!empty($CFG->formatstringstriptags)) {
            $mform->setType('name', PARAM_TEXT);
        } else {
            $mform->setType('name', PARAM_CLEANHTML);
        }
        $mform->addRule('name', null, 'required', null, 'client');
        $mform->addRule('name', null, 'maxlength', '50', 'client');

		//$this->standard_intro_elements();
		
        $mform->addElement('text', 'coursename', get_string('coursename', 'moodecdescription'),array('size'=>'64'));
        $mform->setDefault('coursename', $COURSE->fullname);
        $mform->setType('coursename', PARAM_RAW);
        $mform->addRule('coursename', null, 'required', null, 'client');
        $mform->addRule('coursename', null, 'maxlength', '250', 'client');


        $mform->addElement('text', 'courseshortname', get_string('courseshortname', 'moodecdescription'),array('size'=>'64'));
        $mform->setDefault('courseshortname', $COURSE->shortname);
        $mform->setType('courseshortname', PARAM_RAW);
        $mform->addRule('courseshortname', null, 'required', null, 'client');
        $mform->addRule('courseshortname', null, 'maxlength', '250', 'client');

        $mform->addElement('hidden', 'course',$COURSE->id );
        $mform->setType('course', PARAM_RAW);

	
        $mform->addElement('hidden', 'introeditor',"" );
        $mform->setType('introeditor', PARAM_RAW);


        // Adding the standard "name" field.
        $mform->addElement('text', 'institution', get_string('moodecdescriptionname', 'moodecdescription'), array('size' => '64'));
        if (!empty($CFG->formatstringstriptags)) {
            $mform->setType('institution', PARAM_TEXT);
        } else {
            $mform->setType('institution', PARAM_CLEAN);
        }

        $mform->addRule('institution', null, 'required', null, 'client');
        $mform->addRule('institution', get_string('maximumchars', '', 255), 'maxlength', 255, 'client');
        $mform->addHelpButton('institution', 'moodecdescriptionname', 'moodecdescription');



        $mform->addElement('editor', 'course_summaryeditor',get_string('course_summary', 'moodecdescription'),null, moodecdescription_get_editor_options($this->context) )->setValue((array('text'=> $COURSE->summary, 'format'=> $COURSE->summaryformat)));

        // Image
        $mform->addElement('filepicker', 'picture',get_string('thumbnail', 'moodecdescription') );



        $efforthours = array();
        for ($i = 1; $i <= 20; $i++) {
            $efforthours[$i] = $i;
        }

        $mform->addElement('select', 'effort', get_string('efforthours', 'moodecdescription'), $efforthours);
        $mform->setDefault('effort', 4);

        $courseduration = array();
        for ($i = 1; $i <= 12; $i++) {
            $courseduration[$i] = $i;
        }

        $mform->addElement('select', 'duration', get_string('courseduration', 'moodecdescription'), $courseduration);
        $mform->setDefault('duration', 4);

        // End date
        $mform->addElement('date_selector', 'endcoursedate', get_string('endcoursedate','moodecdescription'));

        // Course Video
        $attributes = array('placeholder' => '<iframe', 'maxlength' => '500', 'size' => '100'); // masque du champ
        $mform->addElement('text', 'video', get_string('video', 'moodecdescription'), $attributes, get_string('video', 'moodecdescription')); // Ajout champ
        $mform->setType('video', PARAM_RAW); // type du champ

        // Complementary Informations
        $mform->addElement('header', 'general', get_string('header_informations', 'moodecdescription'));

        $mform->addElement('editor', 'prerequisiteeditor', get_string("prerequisiteeditor", "moodecdescription"), null,  $this->_customdata['editor_options']);
        $mform->addHelpButton('prerequisiteeditor', 'prerequisiteeditor', 'moodecdescription');

        $mform->addElement('editor', 'syllabuseditor', get_string("syllabuseditor", "moodecdescription"), null,  $this->_customdata['editor_options']);
        $mform->addHelpButton('syllabuseditor', 'syllabuseditor', 'moodecdescription');


        $mform->addElement('editor', 'readingeditor', get_string("readingeditor", "moodecdescription"), null,  $this->_customdata['editor_options']);
        $mform->addHelpButton('readingeditor', 'readingeditor', 'moodecdescription');


        $mform->addElement('editor', 'faqeditor', get_string("faqeditor", "moodecdescription"), null,  $this->_customdata['editor_options']);
        $mform->addHelpButton('faqeditor', 'faqeditor', 'moodecdescription');

        $mform->addElement('text', 'tags', get_string('coursetags', 'moodecdescription'), array('display' => 'noofficial','size' => '64'));
        $mform->addHelpButton('tags', 'coursetags', 'moodecdescription');
		$mform->setType('tags', PARAM_RAW); // type du champ

        $this->standard_coursemodule_elements();
        // Add standard buttons, common to all modules.
        $this->add_action_buttons();
    }

    function data_preprocessing(&$default_values)
    {
        global $COURSE ;


       $context = context_course::instance($COURSE->id);

        if(!has_capability('mod/moodecdescription:addinstance', $context)){
            error(get_string("erroraccess", "moodecdescription"));
        }


        if(moodecdescription_check_instance()) {
            $editvalue = moodecdescription_check_instance();
            $url = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]" ;
            if(strpos($url,'add')){
                echo (get_string("errorinstance", "moodecdescription"));
                redirect(new moodle_url('/course/modedit.php?update='.$editvalue));
            }

        }

        if ($this->current->instance) {

            $draftitemidpre = file_get_submitted_draft_itemid('prerequisite');
            $default_values['prerequisiteeditor']['format'] = $default_values['prerequisiteformat'];
            $default_values['prerequisiteeditor']['text'] = file_prepare_draft_area($draftitemidpre, $this->context->id, 'mod_moodecdescription', 'prerequisite', 0, moodecdescription_get_editor_options($this->context), $default_values['prerequisite']);
            $default_values['prerequisiteeditor']['itemid'] = $draftitemidpre;


            $draftitemidsy = file_get_submitted_draft_itemid('syllabus');
            $default_values['syllabuseditor']['format'] = $default_values['syllabusformat'];
            $default_values['syllabuseditor']['text'] = file_prepare_draft_area($draftitemidsy, $this->context->id, 'mod_moodecdescription', 'syllabus', 0, moodecdescription_get_editor_options($this->context), $default_values['syllabus']);
            $default_values['syllabuseditor']['itemid'] = $draftitemidsy;

            $draftitemidre = file_get_submitted_draft_itemid('reading');
            $default_values['readingeditor']['format'] = $default_values['readingformat'];
            $default_values['readingeditor']['text'] = file_prepare_draft_area($draftitemidre, $this->context->id, 'mod_moodecdescription', 'reading', 0, moodecdescription_get_editor_options($this->context), $default_values['reading']);
            $default_values['readingeditor']['itemid'] = $draftitemidre;

            $draftitemidfaq = file_get_submitted_draft_itemid('faq');
            $default_values['faqeditor']['format'] = $default_values['faqformat'];
            $default_values['faqeditor']['text'] = file_prepare_draft_area($draftitemidfaq, $this->context->id, 'mod_moodecdescription', 'faq', 0, moodecdescription_get_editor_options($this->context), $default_values['faq']);
            $default_values['faqeditor']['itemid'] = $draftitemidfaq;
			
			
        }


    }
}
