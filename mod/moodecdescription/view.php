<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.
/**
 * Prints a particular instance of moodecdescription
 *
 * You can have a rather longer description of the file as well,
 * if you like, and it can span multiple lines.
 *
 * @package    mod_moodecdescription
 * @copyright  2011 Your Name
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

// Replace moodecdescription with the name of your module and remove this line.

require_once(dirname(dirname(dirname(__FILE__))).'/config.php');
require_once(dirname(__FILE__).'/lib.php');

require_once($CFG->dirroot.'/mod/moodecdescription/lib.php');
require_once($CFG->dirroot.'/lib/weblib.php');

error_log("view.php", 0);

clearstatcache();

global $PAGE ;

$id = optional_param('id', 0, PARAM_INT); // Course_module ID, or
$n  = optional_param('n', 0, PARAM_INT);  // ... moodecdescription instance ID - it should be named as the first character of the module.

if ($id) {
    $cm         = get_coursemodule_from_id('moodecdescription', $id, 0, false, MUST_EXIST);
    $course     = $DB->get_record('course', array('id' => $cm->course), '*', MUST_EXIST);
    $moodecdescription  = $DB->get_record('moodecdescription', array('id' => $cm->instance), '*', MUST_EXIST);
} else if ($n) {
    $moodecdescription  = $DB->get_record('moodecdescription', array('id' => $n), '*', MUST_EXIST);
    $course     = $DB->get_record('course', array('id' => $moodecdescription->course), '*', MUST_EXIST);
    $cm         = get_coursemodule_from_instance('moodecdescription', $moodecdescription->id, $course->id, false, MUST_EXIST);
} else {
    error('You must specify a course_module ID or an instance ID');
}

require_login($course, true, $cm);
$context = context_module::instance($cm->id);

$event = \mod_moodecdescription\event\course_module_viewed::create(array(
    'objectid' => $PAGE->cm->instance,
    'context' => $PAGE->context,
));
$event->add_record_snapshot('course', $PAGE->course);
// In the next line you can use $PAGE->activityrecord if you have set it, or skip this line if you don't have a record.
//$event->add_record_snapshot($PAGE->cm->modname, $activityrecord);
$event->trigger();

// Print the page header.

$PAGE->set_url('/mod/moodecdescription/view.php', array('id' => $cm->id));
$PAGE->set_title(format_string($moodecdescription->name));
$PAGE->set_heading(format_string($course->fullname));
$PAGE->set_context($context);

/*
 * Other things you may want to set - remove if not needed.
 * $PAGE->set_cacheable(false);
 * $PAGE->set_focuscontrol('some-html-id');
 * $PAGE->add_body_class('moodecdescription-'.$somevar);
 */

// Output starts here.
echo $OUTPUT->header();

 //Conditions to show the intro can change to look for own settings or whatever.
/*if ($moodecdescription->intro) {
    echo $OUTPUT->box(format_module_intro('moodecdescription', $moodecdescription, $cm->id), 'generalbox mod_introbox', 'moodecdescriptionintro');
}*/



    


echo HTML_WRITER::start_tag('div',array('id'=>'moodecDescContent'));
    $itemid = file_get_submitted_draft_itemid('prerequisite');
    $image = file_rewrite_pluginfile_urls($moodecdescription->prerequisite, 'pluginfile.php', $context->id, 'mod_moodecdescription', 'prerequisite', 0);

    if(strcmp($moodecdescription->picture,'./pix/default_course_image.png')==0){
        $url = $moodecdescription->picture;
    }else {
        // Check if record exist
        $sql = "SELECT * FROM {files} WHERE contextid = ? AND component = ? AND filearea = ? AND filename = ? AND itemid = ?";
        $param = array($context->id, 'mod_moodecdescription','thumbnail', $moodecdescription->picture,0 );
        if ($DB->record_exists_sql($sql, $param)) {
            $file = $DB->get_record_sql($sql, $param);
            $url = moodle_url::make_pluginfile_url($file->contextid, $file->component, $file->filearea, $file->itemid, $file->filepath, $file->filename);
        } else {
            $fs = get_file_storage();
            $filename = $moodecdescription->picture;
            $file_record = array(
                'contextid' => $context->id,
                'component' => 'mod_moodecdescription',
                'filearea' => 'thumbnail',
                'itemid' => 0,
                'filepath' => '/',
                'filename' => $moodecdescription->picture
            );
            $filepath = $CFG->dataroot . "/mod/moodecdescription/pix/thumbnail/" . $filename;
            $file = $fs->create_file_from_pathname($file_record, $filepath);
            $url = moodle_url::make_pluginfile_url($file->get_contextid(), $file->get_component(), $file->get_filearea(), $file->get_itemid(), $file->get_filepath(), $file->get_filename());
        }
    }



	echo HTML_WRITER::start_tag('div',array('id'=>'mooc'));
	echo $OUTPUT->heading($course->fullname);
	echo HTML_WRITER::end_tag('div');

    echo HTML_WRITER::start_tag('div',array('id'=>'descriptionCours'));
      echo HTML_WRITER::start_tag('div',array('id'=>'texteDescription'));
				if($moodecdescription->video) {
						$labeltext = '';
						$videoEmbed = $moodecdescription->video; // Create and add iframe
						$labeltext .= html_writer::tag('div', $videoEmbed, array('id' => 'iframevideo'));
						echo $labeltext;
				}
				echo HTML_WRITER::start_tag('div',array('id'=>'descriptionSummary'));
						echo $course->summary ;
				echo HTML_WRITER::end_tag('div');
				
				echo HTML_WRITER::start_tag('div',array('id'=>'clear'));
				echo HTML_WRITER::end_tag('div');
      echo HTML_WRITER::end_tag('div');
    echo HTML_WRITER::end_tag('div');//descriptionCours
		
		echo HTML_WRITER::start_tag('div',array('id'=>'moocInfo'));
			echo HTML_WRITER::start_tag('img', array('src' => $url, 'class' => 'moocimg'));
			echo HTML_WRITER::end_tag('img');

			
			echo HTML_WRITER::start_tag('div',array('class'=>'moocinfo teacherbox'));
			echo HTML_WRITER::end_tag('div');
			
			echo HTML_WRITER::start_tag('div',array('id'=>'moodecDescIcons'));
				echo HTML_WRITER::start_tag('div',array('id'=>'school'));
						echo HTML_WRITER::start_tag('span',array('class'=>'iconTitle'));
						echo get_string("institutionheader", "moodecdescription") ;
						echo HTML_WRITER::end_tag('span');
						echo HTML_WRITER::start_tag('span',array('class'=>'iconDesc'));
						echo $moodecdescription->institution ;
						echo HTML_WRITER::end_tag('span');
				echo HTML_WRITER::end_tag('div');
				
				echo HTML_WRITER::start_tag('div',array('id'=>'effort'));
						echo HTML_WRITER::start_tag('span',array('class'=>'iconTitle'));
						 echo get_string("effortheader", "moodecdescription");
						echo HTML_WRITER::end_tag('span');
						echo HTML_WRITER::start_tag('span',array('class'=>'iconDesc'));
						echo $moodecdescription->effort .' '. get_string("weektime", "moodecdescription");
						echo HTML_WRITER::end_tag('span');
				echo HTML_WRITER::end_tag('div');
				
				echo HTML_WRITER::start_tag('div',array('id'=>'duration'));
						echo HTML_WRITER::start_tag('span',array('class'=>'iconTitle'));
						echo get_string("durationheader", "moodecdescription");
						echo HTML_WRITER::end_tag('span');
						echo HTML_WRITER::start_tag('span',array('class'=>'iconDesc'));
						 echo $moodecdescription->duration .' '.get_string("week", "moodecdescription");
						echo HTML_WRITER::end_tag('span');
				echo HTML_WRITER::end_tag('div');
				
			echo HTML_WRITER::end_tag('div');
			
			echo HTML_WRITER::start_tag('div',array('id'=>'container_professor'));
				$teachers = get_teachers($course->id);
				echo HTML_WRITER::start_tag('h4', array('class'=>'teachername'));
						 echo get_string("teacher", "moodecdescription");
				echo HTML_WRITER::end_tag('h4');
			  foreach($teachers as $teacher){
            $userteacher = $DB->get_record("user", array('id'=>$teacher->id));
            echo $OUTPUT->user_picture($userteacher, array('size'=>1));


            echo HTML_WRITER::start_tag('h4', array('class'=>'teachername'));
            echo ' ' .$teacher->firstname . ' ' . $teacher->lastname ;
            echo HTML_WRITER::end_tag('h4');
        }
			echo HTML_WRITER::end_tag('div');
			
		echo HTML_WRITER::end_tag('div');




    //send_stored_file($file, null, 0, true);
	$hasInfoPlus = false;
	$tmpInfoPlus = '';
    if(!empty($moodecdescription->prerequisite)) {
        $tmpInfoPlus .= HTML_WRITER::start_tag('p', array('class'=>'texteMoocBleu'));
        $tmpInfoPlus .= get_string("prerequisiteeditor", "moodecdescription");
        $tmpInfoPlus .= HTML_WRITER::end_tag('p');
				$tmpInfoPlus .= HTML_WRITER::start_tag('div',array('id'=>'prerequisite', 'class'=>'InfoComm'));
        $tmpInfoPlus .= $image;
				$tmpInfoPlus .= HTML_WRITER::end_tag('div');
				$hasInfoPlus = true;
				//echo $tmpInfoPlus;
    }

    if(!empty($moodecdescription->syllabus)) {
        $tmpInfoPlus .= HTML_WRITER::start_tag('p', array('class'=>'texteMoocBleu'));
        $tmpInfoPlus .= get_string("syllabuseditor", "moodecdescription");
        $tmpInfoPlus .= HTML_WRITER::end_tag('p');
				$tmpInfoPlus .= HTML_WRITER::start_tag('div',array('id'=>'prerequisite', 'class'=>'InfoComm'));
        $tmpInfoPlus .= file_rewrite_pluginfile_urls($moodecdescription->syllabus, 'pluginfile.php', $context->id, 'mod_moodecdescription', 'syllabus', 0);
				$tmpInfoPlus .= HTML_WRITER::end_tag('div');
				$hasInfoPlus = true;

    }
    if(!empty($moodecdescription->reading)) {
       $tmpInfoPlus .= HTML_WRITER::start_tag('p', array('class'=>'texteMoocBleu'));
        $tmpInfoPlus .=  get_string("readingeditor", "moodecdescription");
        $tmpInfoPlus .=  HTML_WRITER::end_tag('p');
				$tmpInfoPlus .= HTML_WRITER::start_tag('div',array('id'=>'prerequisite', 'class'=>'InfoComm'));
        $tmpInfoPlus .=  file_rewrite_pluginfile_urls($moodecdescription->reading, 'pluginfile.php', $context->id, 'mod_moodecdescription', 'reading', 0);
				$tmpInfoPlus .= HTML_WRITER::end_tag('div');
				$hasInfoPlus = true;
    }

    if(!empty($moodecdescription->faq)) {
       $tmpInfoPlus .= HTML_WRITER::start_tag('p', array('class'=>'texteMoocBleu'));
        $tmpInfoPlus .=  get_string("faqeditor", "moodecdescription");
        $tmpInfoPlus .=  HTML_WRITER::end_tag('p');
				$tmpInfoPlus .= HTML_WRITER::start_tag('div',array('id'=>'prerequisite', 'class'=>'InfoComm'));
        $tmpInfoPlus .=  file_rewrite_pluginfile_urls($moodecdescription->faq, 'pluginfile.php', $context->id, 'mod_moodecdescription', 'faq', 0);
				$tmpInfoPlus .= HTML_WRITER::end_tag('div');
				$hasInfoPlus = true;
    }

    if(!empty($moodecdescription->tags)) {
        $tmpInfoPlus .= HTML_WRITER::start_tag('p', array('class'=>'texteMoocBleu'));
        $tmpInfoPlus .=  get_string("coursetags", "moodecdescription");
        $tmpInfoPlus .=  HTML_WRITER::end_tag('p');
				$tmpInfoPlus .= HTML_WRITER::start_tag('div',array('id'=>'prerequisite', 'class'=>'InfoComm'));
        $tmpInfoPlus .=  $moodecdescription->tags;
				$tmpInfoPlus .= HTML_WRITER::end_tag('div');
				$hasInfoPlus = true;
    }
		
		if($hasInfoPlus)
		{
			echo HTML_WRITER::start_tag('div',array('id'=>'infoCourse'));
			echo '<h4>'.get_string('moreinfo', 'theme_mdcl').'</h4>';
			echo $tmpInfoPlus;
			echo HTML_WRITER::end_tag('div');
		}
	
	echo HTML_WRITER::start_tag('div',array('id'=>'clear'));
	echo HTML_WRITER::end_tag('div');

echo HTML_WRITER::end_tag('div');

// Second column, MOOC Informations







// Finish the page.
echo $OUTPUT->footer();
