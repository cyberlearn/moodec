<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * moodecvideo module version information
 *
 * @package mod_moodecvideo
 * @copyright  2009 Petr Skoda (http://skodak.org)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require('../../config.php');
require_once($CFG->dirroot.'/mod/moodecvideo/locallib.php');
require_once($CFG->libdir.'/completionlib.php');

$id      = optional_param('id', 0, PARAM_INT); // Course Module ID
$p       = optional_param('p', 0, PARAM_INT);  // moodecvideo instance ID
$inpopup = optional_param('inpopup', 0, PARAM_BOOL);

if ($p) {
    if (!$moodecvideo = $DB->get_record('moodecvideo', array('id'=>$p))) {
        print_error('invalidaccessparameter');
    }
    $cm = get_coursemodule_from_instance('moodecvideo', $moodecvideo->id, $moodecvideo->course, false, MUST_EXIST);

} else {
    if (!$cm = get_coursemodule_from_id('moodecvideo', $id)) {
        print_error('invalidcoursemodule');
    }
    $moodecvideo = $DB->get_record('moodecvideo', array('id'=>$cm->instance), '*', MUST_EXIST);
}

$course = $DB->get_record('course', array('id'=>$cm->course), '*', MUST_EXIST);

require_course_login($course, true, $cm);
$context = context_module::instance($cm->id);
require_capability('mod/moodecvideo:view', $context);

// Trigger module viewed event.
$event = \mod_moodecvideo\event\course_module_viewed::create(array(
   'objectid' => $moodecvideo->id,
   'context' => $context
));
$event->add_record_snapshot('course_modules', $cm);
$event->add_record_snapshot('course', $course);
$event->add_record_snapshot('moodecvideo', $moodecvideo);
$event->trigger();

// Update 'viewed' state if required by completion system
require_once($CFG->libdir . '/completionlib.php');
$completion = new completion_info($course);
$completion->set_module_viewed($cm);

$PAGE->set_url('/mod/moodecvideo/view.php', array('id' => $cm->id));

$PAGE->set_title($course->shortname.': '.$moodecvideo->name);
$PAGE->set_heading($course->fullname);
$PAGE->set_activity_record($moodecvideo);

echo $OUTPUT->header();



echo HTML_WRITER::start_tag('div', array('class'=>'videotitle'));

echo HTML_WRITER::start_tag('h1');
    echo HTML_WRITER::start_tag('img', array('src'=>'./pix/icon.gif'));
    echo HTML_WRITER::end_tag('img');
    echo ' '.$moodecvideo->name ;
echo HTML_WRITER::end_tag('h1');
echo HTML_WRITER::end_tag('div');

echo HTML_WRITER::start_tag('div', array('class'=>'iframevideomoodec'));
    echo $moodecvideo->content ;
echo HTML_WRITER::end_tag('div');

if ($moodecvideo->intro) {
    echo HTML_WRITER::start_tag('div', array('class'=>'videodescription'));
    echo $moodecvideo->intro ;
    echo HTML_WRITER::end_tag('div');
}

echo $OUTPUT->footer();
