<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'moodecvideo', language 'en', branch 'MOODLE_20_STABLE'
 *
 * @package   mod_moodecvideo
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['configdisplayoptions'] = 'Select all options that should be available, existing settings are not modified. Hold CTRL key to select multiple fields.';
$string['content'] = 'Video Iframe';
$string['content_help'] = 'Paste here your iframe Video from any broadcaster like youtube, only Iframe will be interpreted in your page';
$string['contentheader'] = 'Video';
$string['createmoodecvideo'] = 'Create a new video resource';
$string['displayoptions'] = 'Available display options';
$string['displayselect'] = 'Display';
$string['displayselectexplain'] = 'Select display type.';
$string['legacyfiles'] = 'Migration of old course file';
$string['legacyfilesactive'] = 'Active';
$string['legacyfilesdone'] = 'Finished';
$string['modulename'] = 'Moodec Video';
$string['modulename_help'] = 'The video module enables a teacher to create a web video resource using embedded  iframe. This video can come from any broadcaster like Youtube or Vimeo.';
$string['modulenameplural'] = 'moodecvideos';
$string['optionsheader'] = 'Display options';
$string['moodecvideo-mod-moodecvideo-x'] = 'Any video module';
$string['moodecvideo:addinstance'] = 'Add a new video resource';
$string['moodecvideo:view'] = 'View video content';
$string['pluginadministration'] = 'Video module administration';
$string['pluginname'] = 'moodecvideo';
$string['popupheight'] = 'Pop-up height (in pixels)';
$string['popupheightexplain'] = 'Specifies default height of popup windows.';
$string['popupwidth'] = 'Pop-up width (in pixels)';
$string['popupwidthexplain'] = 'Specifies default width of popup windows.';
$string['printheading'] = 'Display video name';
$string['printheadingexplain'] = 'Display video name above content?';
$string['printintro'] = 'Display video description';
$string['printintroexplain'] = 'Display video description above content?';
