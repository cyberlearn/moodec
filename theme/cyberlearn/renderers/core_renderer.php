<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * This is built using the bootstrapbase template to allow for new theme's using
 * Moodle's new Bootstrap theme engine
 *
 * @package     theme_essential
 * @copyright   2013 Julian Ridden
 * @copyright   2014 Gareth J Barnard, David Bezemer
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class theme_cyberlearn_core_renderer extends theme_bootstrapbase_core_renderer {
    public $language = null;
    protected $theme = null;

    /**
     * This renders the breadcrumbs
     * @return string $breadcrumbs
     */
		
    public function navbar() {

        $breadcrumbs = html_writer::start_tag('ul', array('class' => "breadcrumb"));
        $index = 1;
        foreach ($this->page->navbar->get_items() as $item) {
          $item->hideicon = true;
          $breadcrumbs .= html_writer::tag('li', $this->render($item), array('style' => 'z-index:' . (100 - $index) . ';'));
          $index += 1;
        }
        $breadcrumbs .= html_writer::end_tag('ul');
        return $breadcrumbs;
				
    }
		

    /**
     * Outputs the language menu
     * @return custom_menu object
     */
		 
    public function custom_menu_language() {
        global $CFG;
        $langmenu = new custom_menu();

        return $this->render_custom_menu($langmenu);
				//return ;
    }
		
		protected function render_custom_menu(custom_menu $menu) {
        global $CFG;

        // TODO: eliminate this duplicated logic, it belongs in core, not
        // here. See MDL-39565.
        $addlangmenu = true;
        $langs = get_string_manager()->get_list_of_translations();
        if (count($langs) < 2
            or empty($CFG->langmenu)
            or ($this->page->course != SITEID and !empty($this->page->course->lang))) {
            $addlangmenu = false;
        }

        if (!$menu->has_children() && $addlangmenu === false) {
            return '';
        }

        if ($addlangmenu) {
            $strlang =  get_string('language');
            $currentlang = current_language();
						$aaa=current_language();
            if (!isset($langs[$currentlang])) 
            {
                $currentlang = $strlang;
            }
            $this->language = $menu->add($currentlang, new moodle_url('#'), $strlang, 10000);
            foreach ($langs as $langtype => $langname) {
                $this->language->add($langtype, new moodle_url($this->page->url, array('lang' => $langtype)), $langname);
            }
        }

        $content = '<ul class="nav">';
        foreach ($menu->get_children() as $item) {
            $content .= $this->render_custom_menu_item($item, 1);
        }

        return $content.'</ul>';
    }
		

    /**
     * Outputs the user menu.
     * @return custom_menu object
		 
		 <div class="logininfo">Connecté sous le nom «&nbsp;<a href="http://www.cyberlearn-mobile-dev.ch/moodle/2-9/user/profile.php?id=2" title="Consulter le profil">Admin Utilisateur</a>&nbsp;» (<a href="http://www.cyberlearn-mobile-dev.ch/moodle/2-9/login/logout.php?sesskey=DU97O0WuoY">Déconnexion</a>)</div>
		 
		 
     */
    public function custom_menu_user() {
        // die if executed during install
				if (during_initial_install()) {
            return false;
        }
				global $USER, $CFG, $DB, $SESSION;
        $loginurl = get_login_url();
				$userclass = array('class' => 'loginName');
				
				$usermenu = html_writer::start_tag('div', array('class' => 'logininfo'));
				 if (!isloggedin()) {
            if ($this->page->pagelayout != 'login') {
                $usermenu .= html_writer::link($loginurl, get_string('login'));
            }
        } else if (isguestuser()) {
					$userurl = new moodle_url('#');
          $userpic = parent::user_picture($USER, array('link' => false));
					$usermenu .= html_writer::link($userurl, $userpic . get_string('guest'), $userclass);
					
					$branchlabel = '(' . get_string('logout') . ')';
          $branchurl = new moodle_url('/login/logout.php?sesskey=' . sesskey());
          $usermenu .= html_writer::link($branchurl, $branchlabel);
				} else {
          $branchurl = new moodle_url('/user/profile.php', array('id' => $USER->id));
          $usermenu .= html_writer::link($branchurl, fullname($USER, true), $userclass);
					
					$branchlabel = '(' . get_string('logout') . ')';
          $branchurl = new moodle_url('/login/logout.php?sesskey=' . sesskey());
          $usermenu .= html_writer::link($branchurl, $branchlabel);
					
				}
					
				$usermenu .= html_writer::end_tag('div');
				
        //$usermenu = "test";

        return $usermenu;
    }




}
