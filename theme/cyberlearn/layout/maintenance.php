<?php
	echo $OUTPUT->doctype();
?>
<html <?php echo $OUTPUT->htmlattributes(); ?>>

<?php
	include 'includes/themehead.php';
?>


<body <?php echo $OUTPUT->body_attributes(); ?>>

<?php echo $OUTPUT->standard_top_of_body_html() ?>




<div id="page" class="container-fluid">

	<div id="page-content-header"></div>

    <div id="page-content" class="row-fluid maintenance">
        <section id="region-main" class="span12">
            <?php echo $OUTPUT->main_content(); ?>
        </section>
    </div>
		<?php
    //<footer id="page-footer">
        
        //echo $OUTPUT->standard_footer_html();
        
    //</footer>
		?>
    <?php 
		
			echo $OUTPUT->standard_end_of_body_html(); 
		
		?>

</div>
<?php 
	include 'includes/footer.php';
?>
</body>
</html>
