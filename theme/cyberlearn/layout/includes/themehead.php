<head>
    <title><?php echo $OUTPUT->page_title(); ?></title>
    <link rel="shortcut icon" href="<?php echo $OUTPUT->favicon(); ?>" />
		<link href='https://fonts.googleapis.com/css?family=Oswald' rel='stylesheet' type='text/css'>
    <?php echo $OUTPUT->standard_head_html() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
		<script>

			//$( "#shower" ).click(function() {
			$( document ).ready(function() {
				var headerSearchLabel = $("#headersearchboxlabel");
				var headerSearchInput = $("#headershortsearchbox");
				var searchLabel = $("#coursesearch label");
				var searchInput = $("#coursesearch #shortsearchbox");
				var coursesearchInput = $("#coursesearch #coursesearchbox");
				//alert('coursesearchInput.val()' + coursesearchInput.val());
				
				
				if(coursesearchInput.val()=='' || coursesearchInput.val() == null )
				{
					searchLabel.show();
				}
				else
				{
					searchLabel.hide();
					//alert('hide');
				}
				
				searchLabel.insertAfter(searchInput);
				searchLabel.insertAfter(coursesearchInput);
				
				searchInput.focus(function() {
					focusFunction(searchLabel);
				});	
				coursesearchInput.focus(function() {
					focusFunction(searchLabel);
				});	
				searchInput.blur(function() {
					blurFunction(searchInput, searchLabel);
				});
				coursesearchInput.blur(function() {
					blurFunction(coursesearchInput, searchLabel);
				});
				
				
				headerSearchInput.focus(function() {
					focusFunction(headerSearchLabel);
				});	
				
				headerSearchInput.blur(function() {
					blurFunction(headerSearchInput, headerSearchLabel);
				});
				
				
				function focusFunction(t) {
					t.hide();
				};
				
				function blurFunction(x,t) {
					if(x.val()=='' || x.val() == null )
					{
						t.show();
					}
				};
			});
			

			
			
			function focusFunction1(x,t) {
				
				//document.getElementById(t).style.display = 'none';
				$("#"+t+"").hide();
				
				//x.style.display = 'none';
				
			}
			function blurFunction1(x,t) {
				if(x.value=='' || x.value == null )
				{
					$("#"+t+"").show();
					//document.getElementById(t).style.display = 'initial';
					
				}
				//x.style.display = 'initial';
			}
		</script>
</head>