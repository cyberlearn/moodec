<div id="pageheader">
	<div id="pageheaderContenu">
        <?php
        if(current_language()=='de'){
            $showheader = 'Header einblenden';
        }else{
            $showheader = 'Show Header';
        }
        ?>
        <?php /*
				<div id="show_header"><a href="javascript:showHeader();"><?php echo $showheader; ?></a></div>
				*/ ?>
				

        <div id="pageheader-top-left">
					<div id="headerlangmenu">
						<?php 
            $languages = get_string_manager()->get_list_of_translations();
            $current_language = current_language();
            $home = array('DE'=>'Startseite','EN'=>'Home','ES'=>'P&aacute;gina Principal','FR'=>'Accueil','IT'=>'Home');
            $s = '<ul id="TOOLREF">';
            foreach ($languages as $key => $v) {
                if($key==$current_language){
                    $s .= '<li>'.$key.'</li>';
                }else{
                    $s .= '<li><a href="'.$CFG->wwwroot.'/?lang='.$key.'" title="'.$v.'">'.$key.'</a></li>';
                }
            }
            $s .= '</ul>';
						
            //echo $s; 
						echo $OUTPUT->custom_menu_language();
						?>
					</div>
					<div id="headersearchbox">
						<form id="headercoursesearch" action="<?php echo $CFG->wwwroot; ?>/course/search.php" method="get">
						<input type="text" id="headershortsearchbox" size="12" name="search" />
						<label id="headersearchboxlabel" for="headershortsearchbox"><?php echo get_string('searchcourse', 'theme_cyberlearn'); ?></label><input type="submit" value="Valider" /></form>
					</div>
					 <div class="cleaner">&nbsp;</div>
				</div>
						
        <div id="pageheader-top-right"><?php //echo $OUTPUT->login_info(); ?>
				<?php echo $OUTPUT->user_menu();  //echo $OUTPUT->custom_menu_user(); ?></div>
        <div class="cleaner">&nbsp;</div>
				

        <div class="headermenu"><?php
            echo $PAGE->headingmenu;
        ?></div>
			</div>
		</div>
		<div id="pageHeaderMenu">
			<div id="pageHeaderMenuContenu">
				<div id="pageheader-menu-left">
					<a href="#" id="pull">Menu</a>
					<ul id="menu">
						<li id="menu1"><a href="http://blog.cyberlearn.ch/" target="_blank"><?php echo get_string('blog', 'theme_cyberlearn') ?></a></li>
						<li id="menu2"><a href="http://elearning.hes-so.ch/fr/team-cyberlearn-5268.html" target="_blank"><?php echo get_string('team', 'theme_cyberlearn') ?></a></li>
						<li id="menu3"><a href="http://elearning.hes-so.ch/fr/contact-5403.html" target="_blank"><?php echo get_string('contact', 'theme_cyberlearn') ?></a></li>
					</ul>
					
					<?php /*
					<div id="menu1"><a href="#" target="_blank"><?php echo get_string('blog', 'theme_cyberlearn') ?></a></div>
					<div id="menu2"><a href="#" target="_blank"><?php echo get_string('team', 'theme_cyberlearn') ?></a></div>
					<div id="menu3"><a href="#" target="_blank"><?php echo get_string('contact', 'theme_cyberlearn') ?></a></div>
					*/ ?>
				</div>
        <div id="pageheader-menu-right"><a id="logo" href="<?php echo $CFG->wwwroot; ?>/?redirect=0"></a></div>
				<div style="clear:both"></div>
			</div>
		</div>


<?php if (empty($PAGE->layout_options['nonavbar']) && $PAGE->has_navbar()) { ?>
          <div id="navbarMenu"> 
						 <div id="page-navbar" class="clearfix">
                <nav class="breadcrumb-nav"><?php echo $OUTPUT->navbar(); ?></nav>
                <div class="breadcrumb-button"> <?php echo $OUTPUT->page_heading_button(); ?></div>
								
            </div>
					</div>
<?php } ?>

