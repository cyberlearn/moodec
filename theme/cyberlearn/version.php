<?php

/**
 * Responsive Decaf theme for Moodle 2.6 and above.
 *
 * For full information about creating Moodle themes, see:
 * http://docs.moodle.org/dev/Themes_2.0
 *
 * @package   theme_cyberlearn
 * @copyright 2014 Cyberlearn
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die;

$plugin->version   = 2015011900;
$plugin->requires  = 2014050800;
$plugin->component = 'theme_cyberlearn';
$plugin->maturity  = MATURITY_STABLE;
$plugin->release   = '1.9';
