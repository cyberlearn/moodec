<?php

/**
 * Responsive Decaf theme for Moodle 2.6 and above.
 *
 * For full information about creating Moodle themes, see:
 * http://docs.moodle.org/dev/Themes_2.0
 *
 * @package   theme_cyberlearn
 * @copyright 2014 Cyberlearn
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['pluginname'] = 'Cyberlearn';
$string['region-side-post'] = 'Right';
$string['region-side-pre'] = 'Left';
$string['choosereadme'] = '<div class="clearfix">
	<h2>Cyberlearn</h2>
        <p>Cyberlearn template to extend the bootstrapbase theme</p>
    <h2>Key features</h2>
    <ul>
    <li>Cyberlearn design</li>
    <li>Easy to change the template color (possible to use different colors for different departments)</li>
    <li>The front-page design uses blocks in the content area and displaying them as boxes</li>
    <li>Multi device optimized: "Cyberlearn" follows the best practice of using responsive web design, namely serving the same HTML for devices and using only CSS media queries to decide the rendering on each device</li>
    </ul>    
</div>
';
$string['assistance'] = 'Assistance';
$string['links'] = 'Links';
$string['joinin'] = 'Join in!';
$string['blog'] = 'Blog';
$string['team'] = 'Team';
$string['contact'] = 'Contact';
$string['searchcourse'] = 'Search courses';