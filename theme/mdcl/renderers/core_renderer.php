<?php

/**
 * Responsive mdcl theme for Moodle 2.9 and above.
 *
 * For full information about creating Moodle themes, see:
 * http://docs.moodle.org/dev/Themes_2.0
 *
 * @package   theme_mdcl
 * @copyright 2015 Cyberlearn
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

class theme_mdcl_core_renderer extends theme_bootstrapbase_core_renderer {
	public $language = null;
	protected $theme = null;

	/**
	 * This renders the breadcrumbs
	 * @return string $breadcrumbs
	 */

	public function navbar() {

		$breadcrumbs = html_writer::start_tag('ul', array('class' => "breadcrumb"));
		$index = 1;
		foreach ($this->page->navbar->get_items() as $item) {
			$item->hideicon = true;
			$breadcrumbs .= html_writer::tag('li', $this->render($item), array('style' => 'z-index:' . (100 - $index) . ';'));
			$index += 1;
		}
		$breadcrumbs .= html_writer::end_tag('ul');
		return $breadcrumbs;

	}



	public function custom_menu_header() {
		$cMenuHeader = '<ul id="menu">';


		$menuUrl = new moodle_url('/?redirect=0');
		$menuLabel = get_string('home', 'theme_mdcl');
		$cMenuHeader .= html_writer::tag('li', html_writer::link($menuUrl, $menuLabel), array('id'=>'menu1'));

		$menuUrl = new moodle_url('/?redirect=0&categoryid=1');
		$menuLabel = get_string('catalog', 'theme_mdcl');
		$cMenuHeader .= html_writer::tag('li', html_writer::link($menuUrl, $menuLabel), array('id'=>'menu2'));

		$menuUrl = new moodle_url('/?redirect=0&contact=1');
		$menuLabel = get_string('contact', 'theme_mdcl');
		$cMenuHeader .= html_writer::tag('li', html_writer::link($menuUrl, $menuLabel), array('id'=>'menu3'));

		return $cMenuHeader.'</ul>';


		/*
    <ul id="menu">
                    <li id="menu1"><a href="<?php echo new moodle_url('/?redirect=0'); ?>" target="_blank"><?php echo get_string('home', 'theme_mdcl') ?></a></li><li id="menu2"><a href="http://elearning.hes-so.ch/fr/team-cyberlearn-5268.html" target="_blank"><?php echo get_string('catalog', 'theme_mdcl') ?></a></li><li id="menu3"><a href="http://elearning.hes-so.ch/fr/contact-5403.html" target="_blank"><?php echo get_string('contact', 'theme_mdcl') ?></a></li>
                </ul>
            */
	}

	/**
	 * Outputs the language menu
	 * @return custom_menu object
	 */

	public function custom_menu_language() {
		global $CFG;
		$langmenu = new custom_menu();

		return $this->render_custom_menu($langmenu);
		//return ;
	}

	protected function render_custom_menu(custom_menu $menu) {
		global $CFG;

		// TODO: eliminate this duplicated logic, it belongs in core, not
		// here. See MDL-39565.
		$addlangmenu = true;
		$langs = get_string_manager()->get_list_of_translations();
		//echo 'count($langs): '.count($langs).'    empty($CFG->langmenu: '.$CFG->langmenu;
		//echo $this->page->course->id;
		//echo $addlangmenu; 1
		//echo 'count($langs): '.count($langs); 3
		//echo $CFG->langmenu; 1
		//echo SITEID; 3
		//echo $this->page->course->lang; en
		if (count($langs) < 2
			or empty($CFG->langmenu)
			or ($this->page->course->id != SITEID and !empty($this->page->course->lang))) {
			$addlangmenu = false;
			//echo '$addlangmenu: false';
		}

		if (!$menu->has_children() && $addlangmenu === false) {
			return '';
		}

		if ($addlangmenu) {
			$strlang =  get_string('language');
			$currentlang = current_language();
			$aaa=current_language();
			if (!isset($langs[$currentlang]))
			{
				$currentlang = $strlang;
			}
			$this->language = $menu->add($currentlang, new moodle_url('#'), $strlang, 10000);
			foreach ($langs as $langtype => $langname) {
				$this->language->add($langtype, new moodle_url($this->page->url, array('lang' => $langtype)), $langname);
			}
		}

		$content = '<ul class="nav">';
		foreach ($menu->get_children() as $item) {
			$content .= $this->render_custom_menu_item($item, 1);
		}

		return $content.'</ul>';
	}













	/**
	 * Construct a user menu, returning HTML that can be echoed out by a
	 * layout file.
	 *
	 * @param stdClass $user A user object, usually $USER.
	 * @param bool $withlinks true if a dropdown should be built.
	 * @return string HTML fragment.
	 */
	public function user_menu_orig($user = null, $withlinks = null) {
		$returnstr = "";

		// If during initial install, return the empty return string.
		if (during_initial_install()) {
			return $returnstr;
		}


		global $USER, $CFG, $DB, $SESSION;

		if (is_null($user)) {
			$user = $USER;
		}

		// Note: this behaviour is intended to match that of core_renderer::login_info,
		// but should not be considered to be good practice; layout options are
		// intended to be theme-specific. Please don't copy this snippet anywhere else.
		if (is_null($withlinks)) {
			$withlinks = empty($this->page->layout_options['nologinlinks']);
		}

		// Add a class for when $withlinks is false.
		$usermenuclasses = 'usermenu';
		if (!$withlinks) {
			$usermenuclasses .= ' withoutlinks';
		}

		$loginpage = $this->is_login_page();
		$loginurl = get_login_url();


		// If not logged in, show the typical not-logged-in string.
		if (!isloggedin()) {
			$returnstr = get_string('loggedinnot', 'moodle');
			if (!$loginpage) {
				$returnstr .= " (<a href=\"$loginurl\">" . get_string('login') . '</a>)';
			}
			return html_writer::div(
				html_writer::div(
					$returnstr,
					'login'
				),
				$usermenuclasses
			);

		}

		// If logged in as a guest user, show a string to that effect.
		if (isguestuser()) {
			$returnstr = get_string('loggedinasguest');
			if (!$loginpage && $withlinks) {
				$returnstr .= " (<a href=\"$loginurl\">".get_string('login').'</a>)';
			}

			return html_writer::div(
				html_writer::div(
					$returnstr,
					'login'
				),
				$usermenuclasses
			);
		}

		$course = $this->page->course;
		$context = context_course::instance($course->id);

		$userpic = parent::user_picture($USER, array('link' => false));
		$caret = '<b class="caret"></b>';
		$usertxt = '<span id="usertext">';
		if (!empty($USER->alternatename)) {
			$usertxt .= $USER->alternatename.'</span>';
		} else {
			$usertxt .= $USER->firstname.'</span>';
		}
		$returnstr .= $usertxt.$userpic;

		$usermenulist = "";
		$sepLine = false;
		// Other user.
		if (\core\session\manager::is_loggedinas()) {
			$realuser = \core\session\manager::get_realuser();
			$branchlabel = fullname($realuser, true) . get_string('loggedinas', 'theme_mdcl') . fullname($USER, true);
			$usermenulist = html_writer::tag('li', $branchlabel, array('role'=>'presentation', 'class'=>'uml no_um_ico'));
			$sepLine = true;
		}

		// Role.
		if (is_role_switched($course->id)) {
			$branchlabel = get_string('switchrolereturn') ;
			$branchurl = new moodle_url('/course/switchrole.php', array('id' => $course->id, 'sesskey' => sesskey(), 'switchrole' => 0, 'returnurl' => $this->page->url->out_as_local_url(false)));
			$usermenulist .= html_writer::tag('li', html_writer::link($branchurl, $branchlabel), array('role'=>'presentation', 'class'=>'uml no_um_ico'));
			$sepLine = true;
		}

		// MNet.
		if (is_mnet_remote_user($USER) && $idprovider = $DB->get_record('mnet_host', array('id' => $USER->mnethostid))) {
			$branchlabel = get_string('loggedinfrom', 'theme_mdcl') . $idprovider->name;
			$branchurl = new moodle_url($idprovider->wwwroot);
			$usermenulist .= html_writer::tag('li', html_writer::link($branchurl, $branchlabel), array('role'=>'presentation', 'class'=>'uml no_um_ico'));
			$sepLine = true;

		}

		if($sepLine)
		{
			$usermenulist .= html_writer::empty_tag('hr', array('class' => 'sep'));
		}

		$branchlabel = get_string('myhomemdcl', 'theme_mdcl');
		$branchurl = new moodle_url('/my');
		$usermenulist .= html_writer::tag('li', html_writer::link($branchurl, $branchlabel), array('role'=>'presentation', 'class'=>'uml um_ico_dashboard'));

		$usermenulist .= html_writer::empty_tag('hr', array('class' => 'sep'));

		$branchlabel = get_string('profile');
		$branchurl = new moodle_url('/user/profile.php', array('id' => $USER->id));
		$usermenulist .= html_writer::tag('li', html_writer::link($branchurl, $branchlabel), array('role'=>'presentation', 'class'=>'uml um_ico_user'));


		$branchlabel = get_string('preferences');
		$branchurl = new moodle_url('/user/preferences.php');
		$usermenulist .= html_writer::tag('li', html_writer::link($branchurl, $branchlabel), array('role'=>'presentation', 'class'=>'uml um_ico_cog'));

		$usermenulist .= html_writer::empty_tag('hr', array('class' => 'sep'));


		$branchlabel = get_string('grades');
		$branchurl = new moodle_url('/grade/report/overview/index.php');
		$usermenulist .= html_writer::tag('li', html_writer::link($branchurl, $branchlabel), array('role'=>'presentation', 'class'=>'uml um_ico_garde'));
		
		
		

		// Check if messaging is enabled.
		if (!empty($CFG->messaging)) {
			$branchlabel = get_string('pluginname', 'block_messages');
			$branchurl = new moodle_url('/message/index.php');
			$usermenulist .= html_writer::tag('li', html_writer::link($branchurl, $branchlabel), array('role'=>'presentation', 'class'=>'uml um_ico_msg'));
		}

		if (has_capability('moodle/calendar:manageownentries', $context)) {
			$branchlabel = get_string('pluginname', 'block_calendar_month');
			$branchurl = new moodle_url('/calendar/view.php');
			$usermenulist .= html_writer::tag('li', html_writer::link($branchurl, $branchlabel), array('role'=>'presentation', 'class'=>'uml um_ico_cal'));
		}

		// Check if user is allowed to manage files
		if (has_capability('moodle/user:manageownfiles', $context)) {
			$branchlabel = get_string('privatefiles', 'block_private_files');
			$branchurl = new moodle_url('/user/files.php');
			$usermenulist .= html_writer::tag('li', html_writer::link($branchurl, $branchlabel), array('role'=>'presentation', 'class'=>'uml um_ico_file'));
		}


		$usermenulist .= html_writer::empty_tag('hr', array('class' => 'sep'));

		$branchlabel = get_string('logout');
		if (\core\session\manager::is_loggedinas()) {
			$branchurl = new moodle_url('/course/loginas.php', array('id' => $course->id, 'sesskey' => sesskey()));
		} else {
			$branchurl = new moodle_url('/login/logout.php', array('sesskey' => sesskey()));
		}
		$usermenulist .= html_writer::tag('li', html_writer::link($branchurl, $branchlabel), array('role'=>'presentation', 'class'=>'uml um_ico_logout'));

		// Create a divider (well, a filler).
		$divider = new action_menu_filler();
		$divider->primary = false;

		$am = new action_menu();
		$am->initialise_js($this->page);
		$am->set_menu_trigger(
			$returnstr
		);
		//$withlinks = true;

		$am->set_alignment(action_menu::TR, action_menu::BR);
		$am->set_nowrap_on_items();

		$am->add($usermenulist);

		$withlinks = false;

		return html_writer::div(
			$this->render($am),
			$usermenuclasses
		);
	}


	public function costom_user_menu($user = null, $withlinks = null) {
		$returnstr = "";
		$usermenulist = "";
		// If during initial install, return the empty return string.
		if (during_initial_install()) {return $returnstr;}

		global $USER, $CFG, $DB, $SESSION;

		if (is_null($user)) {$user = $USER;}

		// Note: this behaviour is intended to match that of core_renderer::login_info,
		// but should not be considered to be good practice; layout options are
		// intended to be theme-specific. Please don't copy this snippet anywhere else.
		if (is_null($withlinks)) {
			$withlinks = empty($this->page->layout_options['nologinlinks']);
		}

		// Add a class for when $withlinks is false.
		$usermenuclasses = 'usermenu';
		if (!$withlinks) {
			$usermenuclasses .= ' withoutlinks';
		}



		$loginpage = $this->is_login_page();
		$loginurl = get_login_url();

		//$OUTPUT->pix_url('default_slide', 'theme');
		$usertxtlogin = '<span id="usertext">';
		$userpiclogin = '';
		$usertxtlogintmp = '';



		if (!isloggedin() || isguestuser()) {

			if ($loginpage) {
				//return html_writer::div( html_writer::div($userlogin,'loginpage') ,$usermenuclasses);
			}

			$usertxtlogintmp = get_string('loggedinnot');

			if (isguestuser()) {
				$usertxtlogintmp = get_string('loggedinasguest');
			}

			$userpiclogin = html_writer::tag('img', '',
				array(
					'src'=>$this->pix_url('i/user'),
					'class'=>'userpicture',
					'alt'=>get_string('login'),
					'title'=>$usertxtlogintmp
				)
			);

			$returnstr = '<ul class="nav"><li class="dropdown usermenuonlogged"><a href="#" class="dropdown-toggle" data-toggle="dropdown" title="usermenuonlogged">';
			$returnstr .= $usertxtlogin.$usertxtlogintmp.'</span>'.$userpiclogin;
			$returnstr .= '</a><ul class="dropdown-menu" id="loginpaneldropdown"><li><h2>'.get_string('login').'</h2><hr></hr></li>';

			$returnstr .= '<li class="loginpanel">
										<form action="'.new moodle_url('/login/index.php').'" method="post" id="login">
										<div class="loginform">
											<input type="text" placeholder="'.get_string('username').'" name="username" id="username" size="15" value="">
											<input type="password" placeholder="'.get_string('password').'" name="password" id="password" size="15" value="">
										</div>
										<div class="rememberpass">
														<input type="checkbox" name="rememberusername" id="rememberusername" value="1">
														<label for="rememberusername">'.get_string('rememberusername', 'admin').'</label>
										</div>
										<div class="clearer"><!-- --></div>
										<input type="submit" id="loginbtn" value="'.get_string('login').'">
										<div class="forgetpass"><a href="'.new moodle_url('/login/forgot_password.php').'">'.get_string('forgotten').'</a></div>
									</form></li>';
			if (!isguestuser()) {
				$returnstr .= '<li class="guestsub">
											<div class="desc">'.get_string('someallowguest').'</div>
											<form action="'.new moodle_url('/login/index.php').'" method="post" id="guestlogin">
												<div class="guestform">
													<input type="hidden" name="username" value="guest">
													<input type="hidden" name="password" value="guest">
													<input type="submit" id="loginGuest" value="'.get_string('loginguest').'">
												</div>
											</form>
										</li>';
			}
			$returnstr .= '<li class="signup">
										
										<form action="'.new moodle_url('/login/signup.php').'" method="get" id="signup">
											 <div><input type="submit" value="'.get_string('startsignup').'"></div>
										</form>
									</li>';
			$returnstr .= '</li></ul></li></ul>';

			//	$returnstr = html_writer::div($aaa.$txt, 'usermenuContent');
			//	$returnstr .= '</li></ul></li></ul>';

			return $returnstr;


			/*
            $returnstr .= $usertxtlogin.$usertxtlogintmp.'</span>'.$userpiclogin;
            $returnstr = html_writer::div($returnstr, 'textmenu');


            $txt = '
                        <div id="loginpaneldropdown">
                            <h2>'.get_string('login').'</h2>
                            <div class="loginpanel">

                                <form action="'.new moodle_url('/login/index.php').'" method="post" id="login">
                                <div class="loginform">
                                    <input type="text" placeholder="'.get_string('username').'" name="username" id="username" size="15" value="">
                                    <input type="password" placeholder="'.get_string('password').'" name="password" id="password" size="15" value="">
                                </div>
                                <div class="clearer"><!-- --></div>
                                <div class="rememberpass">
                                                <input type="checkbox" name="rememberusername" id="rememberusername" value="1">
                                                <label for="rememberusername">Se souvenir du nom </label>
                                </div>
                                <div class="clearer"><!-- --></div>
                                <input type="submit" id="loginbtn" value="'.get_string('login').'">
                                <div class="forgetpass"><a href="'.new moodle_url('/login/forgot_password.php').'">'.get_string('forgotten').'</a></div>
                            </form>

                        </div>';
    //$txt .= html_writer::empty_tag('hr', array('class' => 'sep'));
    $txt .='
                    <div class="subcontent guestsub">
                        <div class="desc">'.get_string('someallowguest').'</div>
                        <form action="'.new moodle_url('/login/index.php').'" method="post" id="guestlogin">
                            <div class="guestform">
                                <input type="hidden" name="username" value="guest">
                                <input type="hidden" name="password" value="guest">
                                <input type="submit" id="loginGuest" value="'.get_string('loginguest').'">
                            </div>
                        </form>
                    </div>';
    //$txt .= html_writer::empty_tag('hr', array('class' => 'sep'));
    $txt .='
                    <div class="signup">
                        <h5>'.get_string('firsttime').'</h5>
                        <form action="'.new moodle_url('/login/signup.php').'" method="get" id="signup">
           <div><input type="submit" value="'.get_string('startsignup').'"></div>
                        </form>
                    </div>



                 </div>


            ';

            $returnstr = html_writer::div($returnstr.$txt, 'usermenuContent');
            return html_writer::div($returnstr, $usermenuclasses.' usermenunologged');
            */




		}
		else
		{
			$course = $this->page->course;
			$context = context_course::instance($course->id);
			
			$totalmessages = $this->get_nbr_user_newmessages();

			$userpic = parent::user_picture($USER, array('link' => false));
			$caret = '<b class="caret"></b>';
			$usertxt = '<span id="usertext">';
			if (!empty($USER->alternatename)) {
				$usertxt .= $USER->alternatename.'</span>';
			} else {
				$usertxt .= $USER->firstname.'</span>';
			}
			$returnstr .= $usertxt.$userpic;
			
			if($totalmessages > 0)
			{
				$returnstr .= '<div class="nbrMsg">'.$totalmessages.'</div>';
			}

			$sepLine = false;
			// Other user.
			if (\core\session\manager::is_loggedinas()) {
				$realuser = \core\session\manager::get_realuser();
				$branchlabel = fullname($realuser, true) . get_string('loggedinas', 'theme_mdcl') . fullname($USER, true);
				$usermenulist = html_writer::tag('li', $branchlabel, array('role'=>'presentation', 'class'=>'uml no_um_ico'));
				$sepLine = true;
			}

			// Role.
			if (is_role_switched($course->id)) {
				$branchlabel = get_string('switchrolereturn') ;
				$branchurl = new moodle_url('/course/switchrole.php', array('id' => $course->id, 'sesskey' => sesskey(), 'switchrole' => 0, 'returnurl' => $this->page->url->out_as_local_url(false)));
				$usermenulist .= html_writer::tag('li', html_writer::link($branchurl, $branchlabel), array('role'=>'presentation', 'class'=>'uml no_um_ico'));
				$sepLine = true;
			}

			// MNet.
			if (is_mnet_remote_user($USER) && $idprovider = $DB->get_record('mnet_host', array('id' => $USER->mnethostid))) {
				$branchlabel = get_string('loggedinfrom', 'theme_mdcl') . $idprovider->name;
				$branchurl = new moodle_url($idprovider->wwwroot);
				$usermenulist .= html_writer::tag('li', html_writer::link($branchurl, $branchlabel), array('role'=>'presentation', 'class'=>'uml no_um_ico'));
				$sepLine = true;

			}

			if($sepLine)
			{
				$usermenulist .= html_writer::empty_tag('hr', array('class' => 'sep'));
			}

			$branchlabel = get_string('myhomemdcl', 'theme_mdcl');
			$branchurl = new moodle_url('/my');
			$usermenulist .= html_writer::tag('li', html_writer::link($branchurl, $branchlabel), array('role'=>'presentation', 'class'=>'uml um_ico_dashboard'));

			$usermenulist .= html_writer::empty_tag('hr', array('class' => 'sep'));

			$branchlabel = get_string('profile');
			$branchurl = new moodle_url('/user/profile.php', array('id' => $USER->id));
			$usermenulist .= html_writer::tag('li', html_writer::link($branchurl, $branchlabel), array('role'=>'presentation', 'class'=>'uml um_ico_user'));


			$branchlabel = get_string('preferences');
			$branchurl = new moodle_url('/user/preferences.php');
			$usermenulist .= html_writer::tag('li', html_writer::link($branchurl, $branchlabel), array('role'=>'presentation', 'class'=>'uml um_ico_cog'));

			$usermenulist .= html_writer::empty_tag('hr', array('class' => 'sep'));


			$branchlabel = get_string('grades');
			$branchurl = new moodle_url('/grade/report/overview/index.php');
			$usermenulist .= html_writer::tag('li', html_writer::link($branchurl, $branchlabel), array('role'=>'presentation', 'class'=>'uml um_ico_garde'));
			
			$certicatelabel = get_string("mymoodeccertificates", "moodeccertificate");
			$certicateurl = new moodle_url('/mod/moodeccertificate/list_certificates.php');
			$usermenulist .= html_writer::tag('li', html_writer::link($certicateurl, $certicatelabel), array('role'=>'presentation', 'class'=>'uml um_ico_certificate'));
		
			// Check if messaging is enabled.
			if (!empty($CFG->messaging)) {
				$branchlabel = get_string('pluginname', 'block_messages');
				if($totalmessages > 0)
				{
					$branchlabel .= ' ('.$totalmessages.')';
				}
				$branchurl = new moodle_url('/message/index.php');
				$usermenulist .= html_writer::tag('li', html_writer::link($branchurl, $branchlabel), array('role'=>'presentation', 'class'=>'uml um_ico_msg'));
			}
			
			
			/*
			if (has_capability('moodle/calendar:manageownentries', $context)) {
				$branchlabel = get_string('pluginname', 'block_calendar_month');
				$branchurl = new moodle_url('/calendar/view.php');
				$usermenulist .= html_writer::tag('li', html_writer::link($branchurl, $branchlabel), array('role'=>'presentation', 'class'=>'uml um_ico_cal'));
			}

			// Check if user is allowed to manage files
			if (has_capability('moodle/user:manageownfiles', $context)) {
				$branchlabel = get_string('privatefiles', 'block_private_files');
				$branchurl = new moodle_url('/user/files.php');
				$usermenulist .= html_writer::tag('li', html_writer::link($branchurl, $branchlabel), array('role'=>'presentation', 'class'=>'uml um_ico_file'));
			}
			*/
			
			
			$usermenulist .= html_writer::empty_tag('hr', array('class' => 'sep'));

			$branchlabel = get_string('logout');
			if (\core\session\manager::is_loggedinas()) {
				$branchurl = new moodle_url('/course/loginas.php', array('id' => $course->id, 'sesskey' => sesskey()));
			} else {
				$branchurl = new moodle_url('/login/logout.php', array('sesskey' => sesskey()));
			}
			$usermenulist .= html_writer::tag('li', html_writer::link($branchurl, $branchlabel), array('role'=>'presentation', 'class'=>'uml um_ico_logout'));



		}


		// Create a divider (well, a filler).
		$divider = new action_menu_filler();
		$divider->primary = false;

		$am = new action_menu();
		$am->initialise_js($this->page);
		$am->set_menu_trigger(
			$returnstr
		);
		//$withlinks = true;

		$am->set_alignment(action_menu::TR, action_menu::BR);
		$am->set_nowrap_on_items();

		$am->add($usermenulist);






		$withlinks = false;


		/*
if ($withlinks) {
    $navitemcount = count($opts->navitems);
    $idx = 0;
    foreach ($opts->navitems as $key => $value) {

        switch ($value->itemtype) {
            case 'divider':
                // If the nav item is a divider, add one and skip link processing.
                $am->add($divider);
                break;

            case 'invalid':
                // Silently skip invalid entries (should we post a notification?).
                break;

            case 'link':
                // Process this as a link item.
                $pix = null;
                if (isset($value->pix) && !empty($value->pix)) {
                    $pix = new pix_icon($value->pix, $value->title, null, array('class' => 'iconsmall'));
                } else if (isset($value->imgsrc) && !empty($value->imgsrc)) {
                    $value->title = html_writer::img(
                        $value->imgsrc,
                        $value->title,
                        array('class' => 'iconsmall')
                    ) . $value->title;
                }
                $al = new action_menu_link_secondary(
                    $value->url,
                    $pix,
                    $value->title,
                    array('class' => 'icon')
                );
                $am->add($al);
                break;
        }

        $idx++;

        // Add dividers after the first item and before the last item.
        if ($idx == 1 || $idx == $navitemcount - 1) {
            $am->add($divider);
        }
    }
}
        */
		return html_writer::div(
			$this->render($am),
			$usermenuclasses
		);
	}





	private function get_nbr_user_newmessages() {
			global $USER, $DB;
			
			
			$nbrMsg = 0;
			$maxmessages = 99;

			$newmessagesql = "SELECT id, smallmessage, useridfrom, useridto, timecreated, fullmessageformat, notification, contexturl
												FROM {message}
												WHERE useridto = :userid
												ORDER BY timecreated DESC";

			$messages = $DB->get_records_sql($newmessagesql, array('userid' => $USER->id), 0, $maxmessages);
			$nbrMsg = count($messages);

			return $nbrMsg;

	}


}
