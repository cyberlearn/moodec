<?php

/**
 * Responsive Decaf theme for Moodle 2.6 and above.
 *
 * For full information about creating Moodle themes, see:
 * http://docs.moodle.org/dev/Themes_2.0
 *
 * @package   theme_mdcl
 * @copyright 2014 Cyberlearn
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */


function theme_mdcl_process_css($css, $theme) {
	global $CFG;
	$tag = '[[font:fontname]]';
	$replacement = $CFG->wwwroot.'/theme/mdcl/font/';
	//echo ($replacement);
	$css = str_replace($tag, $replacement, $css);
	
	$fontwww = preg_replace("(https?:)", "", $CFG->wwwroot . '/theme/mdcl/font/');

  $tag = '[[setting:fontwww]]';
	$css = str_replace($tag, $fontwww, $css);
	return $css;
}

function theme_mdcl_page_init(moodle_page $page) {
    $page->requires->jquery();
}