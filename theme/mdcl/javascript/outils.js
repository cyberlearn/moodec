$( document ).ready(function() {
	var widthNavSmall = 0;
	if($(window).width() > 800)
	{
		widthNavSmall = Math.round($(window).width() * .05);
	}
	else
	{
		widthNavSmall = Math.round($(window).width() * .1);
	}
	
	
	
	
	redimensionnement();
    
	var pull 		= $('#pull');
	var menu 		= $('ul#menu');
	var pageHeaderMenu = ('pageHeaderMenu');
	var menuHeight	= menu.height();
	
	
	pull.click(function(e) {
		e.preventDefault();
		menu.slideToggle();
	});	
		
  $(window).resize(function(){ 
    redimensionnement(); 
		if ($('iframe#moodecforum').length > 0) {
			resizeIframe();
		}
		var w = $(window).width();
		
    if(w > 320 && menu.is(':hidden')) {
       menu.removeAttr('style');
    }
		widthNavSmall = Math.round(w * .03);
		
		$('.breadcrumb-nav .breadcrumbAnim span').width(widthNavSmall);
		$('.breadcrumb-nav .breadcrumbAnim').each(function(i, listElement) {
			var wno = 0;
			wno = parseInt($(listElement).find('span').attr("data-width"));
			$(listElement).hover(
				function()
				{
					$(this).find('span').width(wno);
				},
				function(){
					 $(this).find('span').width(widthNavSmall);
				}
			);
		});
  }); 
 

	$('#moodecforum').load( function() {
		//alert('load iframe');
		console.log(this);
		var frame_window = frames['coursforum'];
		console.log(frame_window);
		var height = frame_window.document.documentElement.scrollHeight+80;
		var height2 = frame_window.frameElement.contentWindow.outerHeight+80; 
		//$('#moodecforum').height(frame_window.frameElement.contentWindow.outerHeight+80);
		
		console.log(height);
			console.log(height2);
		resizeIframe();
		
	});
	

	
 setupBreadCrumb();
	
	
	$('.usermenunologged').click(function(e) {
		$('#loginpaneldropdown').show();
		$('#loginpaneldropdown').focus();
		e.stopPropagation();
	});	
	
	
	$('.loginpaneldropdown').blur(function() {
			$('#loginpaneldropdown').hide();
			console.log('click outside');
	});	
	
	 function setupBreadCrumb() {
	//Check if easing plugin exists. If it doesn't, use "swing"
	if(typeof($.easing) == 'object') {
			easingEquation = 'easeOutQuad';
	} else {
		 easingEquation = 'swing';
	}
	var breadCrumbElements = $('nav.breadcrumb-nav').find('li');
	 //alert('breadCrumbElements.length: ' +breadCrumbElements.length);
	if (breadCrumbElements.length > 0) {
		//If the breadcrumb object length is long enough, compress.
		
		if (breadCrumbElements.length > optiondefaults.minimumCompressionElements) {
			
			breadCrumbElements.each(function(i, listElement) {
				if(i==0)
				{
					$(breadCrumbElements[0]).addClass('first');
				}
				else if(i== breadCrumbElements.length - 1) 
				{
					$(breadCrumbElements[breadCrumbElements.length - 1]).addClass('last');
				}
				else
				{
					var bcoptions = {
						id: i,
						width: $(listElement).width(),
						listElement: $(listElement).find('span'),
						isAnimating: false,
						element: $(listElement).find('span')
						
					};
					var widthNavOrig = 0;
					widthNavOrig =  $(listElement).width();
					var widthFinal = widthNavOrig;
					if(widthFinal>300)
					{
						widthFinal = 300;
					}
					var widthSm = widthNavSmall;
					if(widthSm>widthNavOrig)
					{
						widthSm = widthNavOrig;
					}

					$(breadCrumbElements[i]).addClass('breadcrumbAnim');
					$(listElement).find('a').wrap('<span></span>');
					$(listElement).find('span').attr("data-width",widthFinal);
					$(listElement).find('span').width(widthSm);
					$(listElement).hover(function(){
						 $(this).find('span').width(widthFinal);
						 if(widthFinal<widthNavOrig)
						 {
							  $(this).find('span').find('a').css({'display': 'block', '-o-text-overflow': 'ellipsis',	'-ms-text-overflow': 'ellipsis',	'text-overflow': 'ellipsis' });
						 }
					},function(){
						 $(this).find('span').width(widthSm);
					}
					);

				}
				
			});
		};
		
		
	};
 };

});



/*
function focusFunction1(x,t) {

	$("#"+t+"").hide();
}
function blurFunction1(x,t) {
	if(x.value=='' || x.value == null )
	{
		$("#"+t+"").show();	
	}
}
*/
function resizeIframe(){ 
	//var frame_element = document.getElementById('moodecforum');
	//alert('aaaa');
	
	var frame_window = frames['coursforum'];
	
	$('#moodecforum').height(100);
	$('#moodecforum').height(frame_window.document.documentElement.scrollHeight+80);
	//$('#moodecforum').height(frame_window.frameElement.contentWindow.outerHeight+80);
	var height = frame_window.document.documentElement.scrollHeight+80;
		//$('#moodecforum').height(frame_window.document.documentElement.scrollHeight+80);
	console.log(height);
	//frame_window.src = frame_window.src ;
	//alert(frame_window.document.documentElement.scrollHeight+80);
	//frame_element.style.height = "1px";
	//frame_element.style.height = (frame_window.document.documentElement.scrollHeight+80) +"px";
}
function redimensionnement(){ 
 
    var $bd = $('html');
		var $image = $('img.superbg');
    var image_width = $image.width(); 
    var image_height = $image.height();     
     
    var over = image_width / image_height; 
    var under = image_height / image_width; 
     
    var body_width = $(window).width(); 
    var body_height = $(window).height(); 
		//alert(image_width + ' - ' + image_height);
		//alert(over + ' - ' + under);
    //alert(body_width + ' - ' + body_height); 
    if (body_width / body_height >= over) { 
			$bd.css({'background-size': body_width + 'px ' + Math.ceil(under * body_width) + 'px'});
    }  
    else { 
			$bd.css({'background-size': Math.ceil(over * body_height) + 'px ' + body_height + 'px'});
    } 
		
		
}



optiondefaults = {
		maxFinalElementLength: Math.round($(window).width() * .05),
		minFinalElementLength: Math.round($(window).width() *.02),
		minimumCompressionElements: 1,
		endElementsToLeaveOpen: 0,
		beginningElementsToLeaveOpen: 0,
		timeExpansionAnimation: 500,
		timeCompressionAnimation: 400,
		timeInitialCollapse: 500,
		easing: 'swing',
		previewWidth: Math.round($(window).width() * .03)
};


var _container 		= $('nav.breadcrumb-nav');
var _options = {};
var _container = $('.breadcrumb-nav');
var _breadCrumbElements = {};
var _autoIntervalArray = [];
var easingEquation = 'swing';

//setupBreadCrumb1();



		

		
		function expandBreadCrumb(e) {
        var originalWidth = e.data.width;
				var widthfinal = originalWidth + (optiondefaults.previewWidth / 2);
				if(widthfinal>300)
				{
					widthfinal = 300;
				}
				alert('widthfinal: ' + widthfinal);
        $(e.data.element).stop();
        $(e.data.element).animate({
            width: widthfinal
        }, {
            duration: optiondefaults.timeExpansionAnimation,
            easing: optiondefaults.easing,
            queue: false
        });
        return false;
    };
    
    function shrinkBreadCrumb(e) {
        $(e.data.element).stop();
        $(e.data.element).animate({
            width: optiondefaults.previewWidth
        }, {
            duration: optiondefaults.timeCompressionAnimation,
            easing: optiondefaults.easing,
            queue: false
        });
        return false;
    };