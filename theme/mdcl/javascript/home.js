/**
 * Created by christop.hadorn on 06.10.2015.
 */

$(function () {
	// Get course list most popular
	getCourseListTop(8);

	var idCourse = gup('courseid', location.href);
	var idUser = $("#idUser").val();

	if(idCourse){
		getCourse(idCourse,idUser);
	}

	$(document).bind('PgwModal::Close', function() {
		window.location.hash="";
	});

	// Countdown
	var currentDate = new Date();
	var futurDate  = new Date(currentDate.getFullYear(), 1, 22);
	var diff = futurDate.getTime() / 1000 - currentDate.getTime() / 1000;

	var clock = $('.clock').FlipClock(0, {
		clockFace: 'DailyCounter',
		countdown: true
	});
	var time  = clock.getTime();

	time = 0;

	if(time == 0){
		$(".background_clock").css("display","none");
		$("#recherche").css("display","block");
	}
});
