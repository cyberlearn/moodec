/**
 * Created by christop.hadorn on 06.10.2015.
 */

/**
 * Action when user do a research advanced
 */
function manageActionSearchAdvanced(){

    var institutionFilterSearch = [];
    var langFilterSearch = [$(".langmenu>a").text()];
    var keyword = $("#inputTextSearchAdv").val();
    var langCheck = "";

    //Keyword
    $('#inputTextSearchAdv').on('input',function(e){
        keyword = $("#inputTextSearchAdv").val();
        ajaxCallAdvancedSearch(keyword, institutionFilterSearch, langFilterSearch);
    });

    // Institution
    $("#institutionFilter").on('click','input:checkbox', function(event) {
        var val = $(this).prop('value');
        keyword = $("#inputTextSearchAdv").val();
        if($(this).prop("checked")){
            institutionFilterSearch.push(val)
        } else {
            institutionFilterSearch = $.grep(institutionFilterSearch, function(value){
                return value != val;
            })
        }
        ajaxCallAdvancedSearch(keyword, institutionFilterSearch, langFilterSearch);
    });

    // Lang
    $("#langFilter").on('click','input:checkbox', function(event) {
        langFilterSearch = [];
        $("#langFilter").find('li>input:checkbox').each(function () {
            var val = $(this).prop('value');
            if($(this).prop("checked")){
                langFilterSearch.push(val)
            } else {
                langFilterSearch = $.grep(langFilterSearch, function(value){
                    return value != val;
                })
            }
        });
        keyword = $("#inputTextSearchAdv").val();
        ajaxCallAdvancedSearch(keyword, institutionFilterSearch, langFilterSearch);
    });
}

/**
 * Do ajax call to research advanced
 * @param keyword
 * @param institutionFilterSearch
 * @param langFilterSearch
 */
function ajaxCallAdvancedSearch(keyword, institutionFilterSearch, langFilterSearch){
    var data = {
        keyword: keyword,
        institution : institutionFilterSearch,
        lang : langFilterSearch,
        action: 'searchCourseAdvanced',
        token : $("#token").val()
    }

    var datacourse = "";

    $.ajax({
        url: $("#rootMoodec").val() + "/theme/mdcl/srv/ajax.php",
        type: "POST",
        data: data,
        datatype: "json",
        success: function (data) {
            var courselist = jQuery.parseJSON(data);
            $("#catalogue #coursListe").html("");
            showBlockCourse(courselist, "#catalogue #coursListe", datacourse);
        },
        error: function (xhr, ajaxOptions, thrownError) {
            datacourse = '<div class="blocCours"><p class="error">'+ $("#errorString").val() + '</p></div>';
            $("#catalogue #coursListe").append(datacourse);
        }
    });
}
/**
 * Get param for the advanced research
 */
function getParamResearchAdvanced(currentLang) {
    var filter ="";
    var data = {
        action: 'getParamResearchAdvanced',
        token : $("#token").val()
    }

    $.ajax({
        url:  $("#rootMoodec").val() + "/theme/mdcl/srv/ajax.php",
        type: "POST",
        data: data,
        datatype: "json",
        success: function (data) {
            var paramsList = jQuery.parseJSON(data);
            var langName = "";

            //Institution
            if (paramsList.institutions.length > 0) {
                for (i = 0; i < paramsList.institutions.length; i++) {
                    var institution = '<li class="filtreline"><input class="checkboxFilter" type="checkbox" name="' + paramsList.institutions[i].institution + '" value="' + paramsList.institutions[i].institution + '"><div class="textFilter">' + paramsList.institutions[i].institution + '</div><div id="clear"></div></li>';
                    $("#institutionFilter").append(institution);
                }
            }

            //Langs
            var langs = ["fr","en","de"];
            for (i = 0; i < langs.length; i++) {
                if(langs[i] == "fr"){
                    langName = $("#frFilter").val();
                } else if (langs[i] == "en"){
                    langName = $("#enFilter").val();
                } else if (langs[i] == "de"){
                    langName = $("#deFilter").val();
                }
                var lang = '<li class="filtreline"><input class="checkboxFilter" type="checkbox" name="' + langs[i] + '" value="' + langs[i] + '"><div class="textFilter">' + langName + '</div><div id="clear"></div></li>';
                $("#langFilter").append(lang);
            }
            //Check current lang
            $("input:checkbox[name='" + currentLang + "']").prop("checked",true);


        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#coursListe").html('<div class="blocCours"><p class="error">'+ $("#errorString").val() + '</p></div>');
        }
    });
}

/**
 * Research a course by keyword
 * @param keyword
 */
function searchCourse(keyword, currentLang) {
    var datacourse = "";
    var data = {
        keyword: keyword,
        lang : currentLang,
        action: 'search',
        token : $("#token").val()
    }
    $.ajax({
        url: $("#rootMoodec").val() + "/theme/mdcl/srv/ajax.php",
        type: "POST",
        data: data,
        datatype: "json",
        success: function (data) {
            var courselist = jQuery.parseJSON(data);
            showBlockCourse(courselist, "#catalogue #coursListe", datacourse);
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#catalogue #coursListe").append('<div class="blocCoursError"><p class="error">'+ $("#errorString").val() + '</p></div>');
        }
    });
}

function enrol(){

    if($("#idEnrol").val() != ""){
        var data = {
            courseid : $("#idCourse").val(),
            action: 'enrolUser',
            token : $("#token").val()
        }

        $.ajax({
            url: $("#rootMoodec").val() + "/theme/mdcl/srv/ajax.php",
            type: "POST",
            data: data,
            datatype: "json",
            success: function (data) {
                if (data.indexOf("login") >= 0){
                    location.href = data;
                } else {
                    location.href = $("#rootMoodec").val() + "/course/view.php?id=" + $("#idCourse").val();
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
            }
        });
    } else {
        location.href = $("#rootMoodec").val() + "/course/view.php?id=" + $("#idCourse").val();
    }
}

function gup( name, url ) {
    if (!url) url = location.href;
    name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
    var regexS = "[\\#?&]"+name+"=([^&#]*)";
    var regex = new RegExp( regexS );
    var results = regex.exec( url );
    return results == null ? null : results[1];
}

/**
 * Get Course for Popup
 * @param courseid
 */
function getCourse(courseid, iduser) {

    window.location.hash = "courseid=" + courseid;

    var data = {
        courseid: courseid,
        iduser: iduser,
        action: 'getCourse',
        token : $("#token").val()
    }
    $.ajax({
        url:  $("#rootMoodec").val() + "/theme/mdcl/srv/ajax.php",
        type: "POST",
        data: data,
        datatype: "json",
        success: function (data) {
            var course = jQuery.parseJSON(data);

            // Fill popup Dialog
            $("#courseFullname").html(course.fullname);
            $("#iframeYoutube iframe").remove();
            $("#iframeYoutube").append(course.video);
            $("#iframeYoutube iframe").prop("width", 400);
            $("#iframeYoutube iframe").prop("height", 225);
            $("#iframeYoutube iframe").prop("id", "youtube");
            $("#descriptionSummary").html(course.summary);

            if(course.prerequisite != ""){
                $("#prerequisite").html(course.prerequisite);
                $(".prerequisite").css("display", "block");
            } else {
                $("#prerequisite").html("");
                $(".prerequisite").css("display", "none");
            }

            if(course.syllabus != ""){
                $("#syllabus").html(course.syllabus);
                $(".syllabus").css("display", "block");
            } else {
                $("#syllabus").html("");
                $(".syllabus").css("display", "none");
            }

            if(course.reading != ""){
                $("#reading").html(course.reading);
                $(".reading").css("display", "block");
            } else {
                $("#reading").html("");
                $(".reading").css("display", "none");
            }

            if(course.faq != ""){
                $(".faq").css("display", "block");
                $("#faq").html(course.faq);
            } else {
                $("#faq").html("");
                $(".faq").css("display", "none");
            }

            if(course.prerequisite != "" || course.syllabus != ""  || course.reading != ""  || course.faq != ""){
                $("#infoCourse").css("display", "block");
            } else {
                // $("#infoCourse").css("display", "none");
            }

            //var startDateRegistration = Date.parse(dateFormat(course.enrolstartdate)).getTime();
            //var dateToday = Date.parse("today").getTime();

            var startDateRegistration = course.enrolstartdate;
            var dateToday = parseInt(new Date().getTime()/1000);

            if(startDateRegistration <= dateToday){
                if(course.enrol){
                    $("#aInscription").text($("#accessCourse").val());
                    $("#aInscription").css("cursor", "pointer");
                    $("#aInscription").css("pointer-events", "auto");
                    $("#idEnrol").val("");
                    $("#idCourse").val(course.id);
                } else {
                    $("#aInscription").text($("#enrolCourse").val());
                    $("#aInscription").css("pointer-events", "auto");
                    $("#aInscription").css("cursor", "pointer")
                    $("#idEnrol").val(course.enrolid);
                    $("#idCourse").val(course.id);
                }
            } else {
                $("#aInscription").text($("#enrolNotYetCourse").val() + dateFormat(course.enrolstartdate));
                $("#aInscription").css("pointer-events", "none");
                $("#aInscription").css("cursor", "default")
                $("#idEnrol").val("");
                $("#idCourse").val(course.id);
            }

            $("#school .iconDesc").html(course.institution);
            if(course.effort > 1){
                $("#effort .iconDesc").html(course.effort + " " + $("#hours").val() + "/" + $("#week").val());
            } else {
                $("#effort .iconDesc").html(course.effort + " " + $("#hour").val() + "/" + $("#week").val());
            }
            if(course.duration > 1){
                $("#duration .iconDesc").html(course.duration + " " + $("#weeks").val());
            } else {
                $("#duration .iconDesc").html(course.duration + " " + $("#week").val());
            }
            $("#startdate .iconDesc").html(dateFormat(course.startdate));
            $("#enddate .iconDesc").html(dateFormat(course.endcoursedate));

            // Teacher
            $("#container_professor").html("");
            var imgEmail = $("#img").prop("src");

            for(var i = 0 ; i < course.teachers.length ; i++){
                var teacher = course.teachers[i];
                var idTeacher = teacher.uid;

                var pictureTeacher = isPicture(teacher.pictureTeacher);

                $("#container_professor").append('<a target="_blank " href="' + $("#rootMoodec").val() + '/user/profile.php?id='+idTeacher+'"><img class="imgProf" id="imgProf' + i + '"></img></a>');
                $("#container_professor").append('<a target="_blank " href="' + $("#rootMoodec").val() + '/user/profile.php?id='+idTeacher+'"><h4 id="professor' + i + '"></h4></a>');
                $("#container_professor").append('<p id="roleProfessor' + i + '"></p>');
                $("#container_professor").append('<div id="socialNetwork'+i+'"><a id="emailProfessor'+i+'"><img src="' + imgEmail + '" width="25px"></a></div>');


                if (idTeacher != null) {
                    $("#imgProf" + i).css("display", "block");
                    $("#imgProf" + i).prop("src", $("#rootMoodec").val() + pictureTeacher);
                } else {
                    $("#imgProf" + i).html("");
                    $("#imgProf" + i).css("display", "none");
                }

                if (teacher.role != null) {
                    $("#roleProfessor" + i).css("display", "block");
                    $("#roleProfessor" + i).html($("#role").val());
                } else {
                    $("#roleProfessor" + i).html("");
                    $("#roleProfessor" + i).css("display", "none");
                }

                if (teacher.firstname != null || teacher.lastname != null) {
                    $("#professor" + i).css("display", "block");
                    $("#professor" + i).html(teacher.firstname + " " + teacher.lastname);
                } else {
                    $("#professor" + i).html("");
                    $("#professor" + i).css("display", "none");
                }

                if (teacher.email != null) {
                    $("#emailProfessor" + i).css("display", "block");
                    $("#emailProfessor" + i).prop("href", "mailto:" + teacher.email);
                } else {
                    $("#emailProfessor" + i).html("");
                    $("#emailProfessor" + i).css("display", "none");

                }
            }

            //Open Dialog
            $.pgwModal({
                target: '.popupBloc',
                maxWidth: '90%',
                titleBar: false,
                closeOnBackgroundClick : false,
                closeContent: '<a id="closeDialog" class="btnClose"></a>'
            });
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert($("#errorString").val());
        }
    });
}
/**
 * Get course list Top
 */
function getCourseListTop(nbr) {
    var datacourse = "";
    var data = {
        nbr: nbr,
        action: 'getCourseListTop',
        token : $("#token").val()
    }

    $.ajax({
        url: $("#rootMoodec").val() + "/theme/mdcl/srv/ajax.php",
        type: "POST",
        data: data,
        datatype: "json",
        success: function (data) {
            var courselist = jQuery.parseJSON(data);
            showBlockCourse(courselist, "#coursListe", datacourse);
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#coursListe").html('<div class="blocCours"><p class="error">'+ $("#errorString").val() + '</p></div>');
        }
    });
}

/**
 * Show correctly a list of course
 * @param courselist
 */
function showBlockCourse(courselist, idBlock, datacourse){
    var pictureCourse = "";
    var date = "";
    var idUser = $("#idUser").val();
    if (courselist.length > 0) {
        var classHide = ' ';
        for (i = 0; i < courselist.length; i++) {
            if (i >= 6) {
                classHide = ' tohide';
            }
            pictureCourse = isPicture(courselist[i].pictureCourse);
            date = dateFormat(courselist[i].startdate);
            datacourse = '<a class="blocCours'+classHide+'" onclick="getCourse(' + courselist[i].courseid + ',' + idUser + ')">';
            datacourse +='<div class="imgCours" style="background: #f5f5f5 url(' + $("#rootMoodec").val() + '/theme/mdcl/layout/home/image.php?file=' + pictureCourse + ') no-repeat center; background-size: 100% auto">';
            datacourse +='</div>';
            datacourse +='<div class="fdTop"><div class="borderWhite">'+ $("#accessCourse").val() + '</div></div>';
            datacourse +='<div class="fdBottom"><div><span class="detailsMoocs">' + courselist[i].institution + '</span><span class="detailsMoocs">' + date + '</span><span class="titreMooc">' + courselist[i].fullname + '</span></div></div></a>';
            $(idBlock).append(datacourse);
        }
    } else {
        datacourse = '<div class="blocCoursEmpty"><p class="error">'+ $("#noCourseString").val() + '</p></div>';
        $(idBlock).html(datacourse);
    }
}

/**
 * Test is a picture exist
 * @param picture
 * @returns {*}
 */
function isPicture(picture) {
    if (picture.length > 3) {
        return picture;
    } else {
        return $("#rootMoodec").val() + "/theme/mdcl/pix/home/default_course_image.png";
    }
}
/**
 * Format the date
 * @param date
 */
function dateFormat(date) {
    var dateFormat = new Date(date * 1000);
    dateFormat = dateFormat.toString('dd/MM/yyyy');
    return dateFormat;
}


/**
 * resize div coursListe
 * @param width windows
 */
function resizeCoursList(w) {
    var nbrBlocCourRow = (w*0.66)/240;
    alert("nbrBlocCourRow: "+nbrBlocCourRow);
    return nbrBlocCourRow;
}


/**
 Contact page
 */

function fillContact(){
    var idUser = $("#idUser").val();
    if(idUser){
        var data = {
            iduser: idUser,
            action: 'getUser',
            token : $("#token").val()
        }
        $.ajax({
            url: $("#rootMoodec").val() + "/theme/mdcl/srv/ajax.php",
            type: "POST",
            data: data,
            datatype: "json",
            success: function (data) {
                var user = jQuery.parseJSON(data);
                $("#nameContact").val(user.lastname);
                $("#fornameContact").val(user.firstname);
                $("#emailContact").val(user.email);
            },
            error: function (xhr, ajaxOptions, thrownError) {
            }
        });
    }
}
function sendContact() {
    var valid;
    valid = validateContact();
    if(valid) {
        jQuery.ajax({
            url: $("#rootMoodec").val() + "/theme/mdcl/layout/home/contact_mail.php",
            data:'name='+$("#nameContact").val()+'&email='+
            $("#emailContact").val()+'&forname='+
            $("#fornameContact").val()+'&message='+
            $("#messageContact").val()+'&captcha='+
            $("#captcha").val()+'&token='+$("#token").val(),
            type: "POST",
            success:function(data){
                if(data == "captcha"){
                    $("#info").html($("#captchainvalid").val());
                    $("#captcha").css('background-color','#FFFFDF');
                } else if(data == "ok"){
                    $("#info").html("<p class='success'>" + $("#mailsuccess").val() + "</p>");
                    $("#frmContact input").val("");
                    $("#frmContact textarea").val("");
                    refreshCaptcha();
                    fillContact();
                } else {
                    $("#info").html("<p class='error'>"+ $("#mailinvalid").val() + "</p>");
                }
            },
            error:function (){
                $("#info").html("<p class='error'>"+ $("#mailinvalid").val() + "</p>");
            }
        });
    }
}

function validateContact() {
    var valid = true;
    var colorTmp = 'rgba(0, 124, 183, 0.1)';
    $(".formInput").css('background-color','');
    $("#info").html('');

    if(!$("#nameContact").val()) {
        $("#info").html($("#mandatoryfield").val());
        $("#nameContact").css('background-color',colorTmp);
        $("#nameContact").css('border','1px solid #007cb7');
        valid = false;
    }
    if(!$("#emailContact").val().match(/^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/)) {
        $("#info").html($("#emailinvalid").val());
        $("#emailContact").css('background-color',colorTmp);
        $("#emailContact").css('border','1px solid #007cb7');
        valid = false;
    }
    if(!$("#emailContact").val()) {
        $("#info").html($("#mandatoryfield").val());
        $("#emailContact").css('background-color',colorTmp);
        $("#emailContact").css('border','1px solid #007cb7');
        valid = false;
    }
    if(!$("#fornameContact").val()) {
        $("#info").html($("#mandatoryfield").val());
        $("#fornameContact").css('background-color',colorTmp);
        $("#fornameContact").css('border','1px solid #007cb7');
        valid = false;
    }
    if(!$("#messageContact").val()) {
        $("#info").html($("#mandatoryfield").val());
        $("#messageContact").css('background-color',colorTmp);
        $("#messageContact").css('border','1px solid #007cb7');
        valid = false;
    }
    if(!$("#captcha").val()) {
        $("#info").html($("#mandatoryfield").val());
        $("#captcha").css('background-color',colorTmp);
        $("#captcha").css('border','1px solid #007cb7');
        valid = false;
    }
    return valid;
}

function refreshCaptcha() {
    $("#captcha_code").prop('src',$("#rootMoodec").val() + '/theme/mdcl/layout/home/captcha_code_file.php');
}