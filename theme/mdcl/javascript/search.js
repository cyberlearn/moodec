/**
 * Created by christop.hadorn on 06.10.2015.
 */

$(function () {
    // Init popup course
    var currentLang;
    var keyword = $("#inputTextSearchAdv").val();

    // Fill the params advanced research
    if($.trim(keyword).length > 0){
        currentLang = "%%"
        getParamResearchAdvanced(currentLang);
    } else {
        currentLang = $("#headerlangmenu a:first").text();
        getParamResearchAdvanced(currentLang);
    }

    // Get course list research
    searchCourse(keyword, currentLang);

    //Menu accordion research
    $(".accordion").accordion({
        firstChildExpand : false,
        multiExpand: true,
        slideSpeed: 500,
        dropDownIcon: "&#9660",
    });
    $('.accordion .accordion-content').show();

    //Manage the research advanced
    manageActionSearchAdvanced();
		
		

});
