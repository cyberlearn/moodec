<div id="pageheader">
	<div id="pageheaderContenu">
        <?php
        if(current_language()=='de'){
            $showheader = 'Header einblenden';
        }else{
            $showheader = 'Show Header';
        }
        ?>
        <?php /*
				<div id="show_header"><a href="javascript:showHeader();"><?php echo $showheader; ?></a></div>
				*/ ?>
				

        <div id="pageheader-top-left">
					<div id="headerlangmenu">
						<?php 
							echo $OUTPUT->custom_menu_language();
						?>
					</div>
					<div id="headersearchbox">
						<form id="headercoursesearch" action="<?php echo $CFG->wwwroot; ?>/course/search.php" method="get">
						<input type="text" id="headershortsearchbox" size="12" name="search" />
						<label id="headersearchboxlabel" for="headershortsearchbox"><?php echo get_string('searchcourse', 'theme_mdcl'); ?></label><input type="submit" value="Valider" /></form>
					</div>
					 <div id="clear"></div>
				</div>
						
        <div id="pageheader-top-right"><?php //echo $OUTPUT->login_info(); ?>
				<?php 
					//echo $OUTPUT->user_menu();  
					echo $OUTPUT->costom_user_menu();
				
					?>
					 <div id="clear"></div>
				</div>
        <div id="clear"></div>
			</div>
		</div>
		<div id="clear"></div>
		<div id="pageHeaderMenu">


					<a href="#" id="pull">Menu</a>
					<?php 
							echo $OUTPUT->custom_menu_header();
						?>
        <div id="pageheader-menu-right">
					<a id="logo" href="<?php echo $CFG->wwwroot; ?>/?redirect=0"></a>
				</div>
		</div>


<?php if (empty($PAGE->layout_options['nonavbar']) && !$PAGE->has_navbar()) { ?>
          <div id="navbarMenu"> 
						 <div id="page-navbar" class="clearfix">
                <nav class="breadcrumb-nav"><?php echo $OUTPUT->navbar(); ?></nav>
                <div class="breadcrumb-button"> <?php echo $OUTPUT->page_heading_button(); ?></div>
								
            </div>
					</div>
<?php } ?>

