<div>
    <input type="hidden" id="errorString" value="<?php echo get_string("error", "theme_mdcl") ?>">
    <input type="hidden" id="courseFilterString" value="<?php echo get_string("cours", "theme_mdcl") ?>">
    <input type="hidden" id="courseFilterStringPluriel" value="<?php echo get_string("courspluriel", "theme_mdcl") ?>">
    <input type="hidden" id="noCourseString" value="<?php echo get_string("nocourse", "theme_mdcl") ?>">
    <input type="hidden" id="frFilter" value="<?php echo get_string("fr", "theme_mdcl") ?>">
    <input type="hidden" id="enFilter" value="<?php echo get_string("en", "theme_mdcl") ?>">
    <input type="hidden" id="deFilter" value="<?php echo get_string("de", "theme_mdcl") ?>">
    <input type="hidden" id="hour" value="<?php echo get_string("hour", "theme_mdcl") ?>">
    <input type="hidden" id="week" value="<?php echo get_string("week", "theme_mdcl") ?>">
    <input type="hidden" id="hours" value="<?php echo get_string("hours", "theme_mdcl") ?>">
    <input type="hidden" id="weeks" value="<?php echo get_string("weeks", "theme_mdcl") ?>">
    <input type="hidden" id="enrolCourse" value="<?php echo get_string("enrolme", "theme_mdcl") ?>">
    <input type="hidden" id="accessCourse" value="<?php echo get_string("accesscours", "theme_mdcl") ?>">
    <input type="hidden" id="role" value="<?php echo get_string("role", "theme_mdcl") ?>">
    <input type="hidden" id="mandatoryfield" value="<?php echo get_string("mandatoryfield", "theme_mdcl") ?>">
    <input type="hidden" id="emailinvalid" value="<?php echo get_string("emailinvalid", "theme_mdcl") ?>">
    <input type="hidden" id="mailsuccess" value="<?php echo get_string("mailsuccess", "theme_mdcl") ?>">
    <input type="hidden" id="mailinvalid" value="<?php echo get_string("mailinvalid", "theme_mdcl") ?>">
    <input type="hidden" id="captchainvalid" value="<?php echo get_string("captchainvalid", "theme_mdcl") ?>">
    <input type="hidden" id="enrolNotYetCourse" value="<?php echo get_string("enrolnotyet", "theme_mdcl") ?>">
	
</div>

