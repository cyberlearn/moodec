<?php
session_start();
$captcha = $_POST["captcha"];
// Correct captcha
if ($_SESSION['digit'] == $captcha) {

    session_destroy();
    require_once ("../../../../config.php");

    //Get email Admin
    global $DB;
    $userAdmin = $DB->get_record("user", array("id" => 2));

    // POST Variables
    //$to = $userAdmin->email;
    $to = "cyberlearn@hes-so.ch";
    $name = $_POST["name"];
    $from = $_POST["email"];
    $forname = $_POST["forname"];
    $message = $_POST["message"];
    $subject = "[MOOCs HES-SO] - Contact";

    $fromEmail = new stdClass();
    $fromEmail->email = $from;
    $fromEmail->firstname = $forname;
    $fromEmail->lastname = $name;
    $fromEmail->maildisplay = true;
    $fromEmail->mailformat = 0;
    $fromEmail->id = -99;
    $fromEmail->firstnamephonetic = $forname;
    $fromEmail->lastnamephonetic = $name;
    $fromEmail->middlename = $forname . " " . $name;
    $fromEmail->alternatename = $forname . " " . $name;

    $toEmail = new stdClass();
    $toEmail->email = $to;
    $toEmail->firstname = "Admin";
    $toEmail->lastname = "Moodec";
    $toEmail->maildisplay = true;
    $toEmail->mailformat = 0;
    $toEmail->id = -100;
    $toEmail->firstnamephonetic = "Admin";
    $toEmail->lastnamephonetic = "Moodec";
    $toEmail->middlename = "Admin Moodec";
    $toEmail->alternatename = "Admin Moodec";

    $email = email_to_user($toEmail, $fromEmail, $subject, $message);

    if ($email) {
        echo 'ok';
    } else {
        echo 'no';
    }
} else {
    echo 'captcha';
}
?>