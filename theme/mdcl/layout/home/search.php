<link rel="stylesheet" type="text/css" href="<?php echo new moodle_url('/theme/mdcl/style/woco-accordion.css'); ?>">
<script src="<?php echo new moodle_url('/theme/mdcl/javascript/woco.accordion.js'); ?>"></script>
<script src="<?php echo new moodle_url('/theme/mdcl/javascript/search.js'); ?>"></script>
<div id="page">
    <div class="hide"><?php echo $OUTPUT->main_content() ?></div>
    <div id="bgWindow">
        <div id="blocCatalogue">
            <div id="filtreCatalogue">
                <h3 id="researchAdv"><?php echo get_string('searchadvanced', 'theme_mdcl') ?></h3>

                <div class="accordion">
                    <div class="filtreBloc">
                    <h1 class="titleFilter"><?php echo get_string('keywords', 'message') ?></h1>
                    <ul class="subTitleFilter"><p id="keywordsFilter"><input type="text" id="inputTextSearchAdv"
                                                                             placeholder="Chercher un cours..."
                                                                             name="keyword"
                                                                             value="<?php echo $keyword ?>"></p></ul>
                    </div>
                    <div class="filtreBloc">
                        <h1 class="titleFilter"><?php echo get_string('institution') ?></h1>
                        <ul class="subTitleFilter" id="institutionFilter"></ul>
                    </div>
                    <div class="filtreBloc">
                        <h1 class="titleFilter"><?php echo get_string('language', 'hub') ?></h1>
                        <ul class="subTitleFilter" id="langFilter"></ul>
                    </div>
                    <div id="clear"></div>
                </div>
            </div>
            <div id="catalogue">
                <div id="coursListe">

                </div>
            </div>
            <div id="clear"></div>
        </div>
    </div>
