<script src="<?php echo new moodle_url('/theme/mdcl/javascript/flipclock.js');?>"></script>
<script src="<?php echo new moodle_url('/theme/mdcl/javascript/home.js');?>"></script>
<link rel="stylesheet" type="text/css" href="<?php echo new moodle_url('/theme/mdcl/style/flipclock.css');?>">
<div id="page" class="homepage">
    <div class="hide"><?php echo $OUTPUT->main_content() ?></div>

        <div id="banner">
            <div id="logoMooc">
							<img src="<?php echo new moodle_url('/theme/mdcl/pix/logoMooc.svg');?>">
						</div>
            <div id="bannerSoustitre"><?php echo  get_string('bannersoustitre', 'theme_mdcl')?></div>
            <div id="recherche">
                <form id="searchFormHome" name="keywords" type="GET">
                   <input type="hidden" name="redirect" value="0">
									<input type="hidden" name="categoryid" value="1">
                  <input type="text" id="inputText" placeholder="<?php echo  get_string('searchcourses')?>" name="keyword" value="">
                  <input type="submit" value="Valider" id="searchWhite" />
                    
                </form>
            </div>
            <div class="background_clock">
               <div class="title_clock"><h2><?php echo get_string("titleclock","theme_mdcl")?></h2></div><div class="label_clock"><div class="dividerclock firstdivider"></div><div class="langclock"><?php echo get_string("dayclock","theme_mdcl")?></div><div class="dividerclock"></div><div class="langclock"><?php echo get_string("hourclock","theme_mdcl")?></div><div class="dividerclock"></div><div class="langclock"><?php echo get_string("minclock","theme_mdcl")?></div><div class="dividerclock"></div><div class="langclock"><?php echo get_string("secclock","theme_mdcl")?></div></div><div class="clock"></div>
           </div>
					 <div id="tagInscris">
						<div id="tagInscrisTxt"><?php echo get_string("subscribehome","theme_mdcl")?>
							<p><?php echo get_string("nowhome","theme_mdcl")?></p>
						</div>
						<div id="tagInscrisTriangle"></div>
					</div>
        </div>
        <div id="topCours">
            <h2><?php echo get_string('topcourse', 'theme_mdcl')?></h2>
						<div id="coursListe"></div>
        </div>

        <img src="<?php echo new moodle_url('/theme/mdcl/ressources/carre2.png');?>" id="carreBas">
        <div id="logos">
            <h2><?php echo  get_string('partenaires', 'theme_mdcl')?></h2>
			<a href="http://www.hes-so.ch/fr/recherche-hes-so-31.html" target="_blank"><img src="<?php echo new moodle_url('/theme/mdcl/ressources/logo_isnet.png');?>" width="150"></a>
            <a href="http://www.hes-so.ch/" target="_blank"><img src="<?php echo new moodle_url('/theme/mdcl/ressources/logo_hes.png');?>" width="150"></a>
            <a href="http://www.hesge.ch/heds/" target="_blank"><img src="<?php echo new moodle_url('/theme/mdcl/ressources/heds.png');?>" width="170"></a>
            <a href="http://www.hevs.ch/fr/" target="_blank"><img src="<?php echo new moodle_url('/theme/mdcl/ressources/hevs.png');?>" width="150"></a>
        </div>
				
				<?php if ($hassidepost) { ?>
				<div id="region-post" class="block-region">
					<div id="region-post-wrap-1">
						<div id="region-post-wrap-2">
								<div class="region-content">
										<?php echo $OUTPUT->blocks_for_region('side-post') ?>
								</div>
						</div>
					</div>
				</div>
				<?php } ?>

</div>