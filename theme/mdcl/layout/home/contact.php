
<script language="JavaScript" src="<?php echo new moodle_url('/theme/mdcl/javascript/contact.js');?>" type="text/javascript"></script>
<div id="page" class="contact">
    <div class="hide"><?php echo $OUTPUT->main_content() ?></div>
		
    <div id="frmContact">
		<h1 id="titleContact"><?php echo get_string('contacttitle', 'theme_mdcl')?></h1>
     <div id="frm">   
        <div class="formGroup">
                <label><?php echo  get_string('contactname', 'theme_mdcl')?></label>
                <input type="text" name="name" id="nameContact" class="formInput">
        </div>
        <div class="formGroup">
            <label><?php echo  get_string('contactforname', 'theme_mdcl')?></label>
            <input type="text" name="forname" id="fornameContact"  class="formInput">
        </div>
        <div class="formGroup">
            <label><?php echo  get_string('contactemail', 'theme_mdcl')?></label>
            <input type="text" name="email" id="emailContact"  class="formInput">
        </div>
        <div class="formGroup">
            <label><?php echo  get_string('message', 'theme_mdcl')?></label>
            <textarea name="message" id="messageContact"   class="formInput" rows="4" cols="50"></textarea>
        </div>
        <div class="formGroup">
            <label>Captcha : </label>
            <img id="captcha_code" src="<?php echo new moodle_url('/theme/mdcl/layout/home/captcha_code_file.php');?>" />
            <button name="submit" class="btnRefresh" onClick="refreshCaptcha();"><?php //echo  get_string('refresh', 'theme_mdcl')?></button>
        </div>
        <div class="formGroup">
            <label></label>
            <input type="text" name="captcha" id="captcha"  class="formInput" />
        </div>
        <div class="formGroup">
            <label></label>
            <span id="info"></span>
        </div>
			</div>
			<button name="submit" class="btnAction" onClick="sendContact();"><?php echo  get_string('send', 'theme_mdcl')?></button>
    </div>
</div>
</br>

