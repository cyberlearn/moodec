<div class="popupBloc">
    <div id="mooc">
        <h1 id="courseFullname"><span class="texteMooc"><span></h1>
    </div>
    <div id="descriptionCours">
        <div id="texteDescription">
            <div id="iframeYoutube"> </div>
            <div id="descriptionSummary"></div>
			<div id="clear"></div>
        </div>
    </div>

    <input id="idUser" type="hidden" value="<?php echo $USER->id;?>">
    <input id="idEnrol" type="hidden" value="">
    <input id="idCourse" type="hidden" value="">
    <input id="idContext" type="hidden" value="">
	
    <div id="inscription">
        <a id="aInscription" onclick="enrol();" target="_blank"><?php echo  get_string('enrolme', 'theme_mdcl')?></a>

        <div id="moodecDescIcons">
            <div id="school"><span class="iconTitle"><?php echo  get_string('school', 'theme_mdcl')?></span><span class="iconDesc"></span></div>
            <div id="effort"><span class="iconTitle"><?php echo  get_string('effort', 'theme_mdcl')?></span><span class="iconDesc"></span></div>
            <div id="startdate"><span class="iconTitle"><?php echo  get_string('startdate', 'theme_mdcl')?></span><span class="iconDesc"></span></div>
            <div id="enddate"><span class="iconTitle"><?php echo  get_string('enddate', 'theme_mdcl')?></span><span class="iconDesc"></span></div>
            <div id="duration"><span class="iconTitle"><?php echo  get_string('duration', 'theme_mdcl')?></span><span class="iconDesc"></span></div>
        </div>

        <div id="container_professor"></div>

    <img id="img" src="<?php echo new moodle_url('/theme/mdcl/ressources/Hypertension/ressources/iconssocial/icon_mail.png');?>"">

    </div>
	<div id="infoCourse">
		<h4><?php echo  get_string('moreinfo', 'theme_mdcl')?></h4>
		<p class="texteMoocBleu prerequisite"><?php echo  get_string('prerequisite', 'theme_mdcl')?><div class="InfoComm" id="prerequisite"></div> </p>
		<p class="texteMoocBleu syllabus"><?php echo  get_string('syllabus', 'theme_mdcl')?><div class="InfoComm" id="syllabus"></div></p>
		<p class="texteMoocBleu reading"><?php echo  get_string('reading', 'theme_mdcl')?><div class="InfoComm" id="reading"></div> </p>
		<p class="texteMoocBleu faq"><?php echo  get_string('faq', 'theme_mdcl')?><div class="InfoComm" id="faq"></div> </p>
	</div>
    <div id="clear"></div>
</div>
