﻿<?php
echo $OUTPUT->doctype(); 
echo '<html'.$OUTPUT->htmlattributes().'>';
echo '<head>';
include 'head.php';
echo '</head>';


$hasheading = ($PAGE->heading);
$hasnavbar = (empty($PAGE->layout_options['nonavbar']) && $PAGE->has_navbar());
$hasfooter = (empty($PAGE->layout_options['nofooter']));
$hassidepost = (empty($PAGE->layout_options['noblocks']) && $PAGE->blocks->region_has_content('side-post', $OUTPUT));
$hastitle = (empty($PAGE->layout_options['notitle']));
$showsidepost = ($hassidepost && !$PAGE->blocks->region_completely_docked('side-post', $OUTPUT));

$custommenu = $OUTPUT->custom_menu();
$hascustommenu = (empty($PAGE->layout_options['nocustommenu']) && !empty($custommenu));

$courseheader = $coursecontentheader = $coursecontentfooter = $coursefooter = '';
if (empty($PAGE->layout_options['nocourseheaderfooter'])) {
    $courseheader = $OUTPUT->course_header();
    $coursecontentheader = $OUTPUT->course_content_header();
    if (empty($PAGE->layout_options['nocoursefooter'])) {
        $coursecontentfooter = $OUTPUT->course_content_footer();
        $coursefooter = $OUTPUT->course_footer();
    }
}
$bodyclasses = array();
  if ($showsidepost) {
    $bodyclasses[] = 'side-post-only';
} else if (!$showsidepost) {
    $bodyclasses[] = 'content-only';
}
if ($hascustommenu) {
    $bodyclasses[] = 'has_custom_menu';
}
if (!empty($PAGE->theme->settings->footertext)) {
    $footnote = $PAGE->theme->settings->footertext;
} else {
    $footnote = '<!-- There was no custom footnote set -->';
}

?>

<?php
echo '<body'.$OUTPUT->body_attributes().'><img src="'.$OUTPUT->pix_url('bgMoodle', 'theme').'" class="superbg" />';
echo $OUTPUT->standard_top_of_body_html();
?>

<header role="banner" class="navbar notfrontpage">
	<?php include 'header.php';?>
</header>
<div id="page">
    <div id="course-header"><?php echo $OUTPUT->course_header(); ?></div>
		<div id="page-navbar" class="clearfix">
      <nav class="breadcrumb-nav"><?php echo $OUTPUT->navbar(); ?></nav>
      <div class="breadcrumb-button"> <?php echo $OUTPUT->page_heading_button(); ?></div>
			<div id="clear"></div>
    </div>

<!-- START OF CONTENT -->

		<div id="page-content" class="wrapper clearfix">
				<div id="region-main">
						<div class="region-content">

						<?php 
							if ($hastitle)
							{
								//echo $OUTPUT->heading(format_string($COURSE->fullname), 1, 'coursetitle');
							}
							echo $OUTPUT->course_content_header();
							echo $OUTPUT->main_content();
							if (empty($PAGE->layout_options['nocoursefooter'])) {
								echo $OUTPUT->course_content_footer();
							}
						?>
						</div>
				</div>

				<?php if ($hassidepost) { ?>
				<div id="region-post" class="block-region">
					<div id="region-post-wrap-1">
						<div id="region-post-wrap-2">
								<div class="region-content">
										<?php echo $OUTPUT->blocks_for_region('side-post') ?>
								</div>
						</div>
					</div>
				</div>
				<?php } ?>
    	</div>

<!-- END OF CONTENT -->
</div>


<!-- START OF FOOTER -->
<?php 
	if ($hasfooter) { 
		include 'footer.php';
	} 
?>

<?php echo $OUTPUT->standard_end_of_body_html() ?>
</body>
</html>