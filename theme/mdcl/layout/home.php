<?php
$rootMoodec = $CFG->wwwroot;
echo  html_writer::empty_tag('input', array('type'=>'hidden', 'id'=>'rootMoodec', 'value'=> $rootMoodec));

$hasheading = ($PAGE->heading);
$hasnavbar = (empty($PAGE->layout_options['nonavbar']) && $PAGE->has_navbar());
$hasfooter = (empty($PAGE->layout_options['nofooter']));
$hassidepost = (empty($PAGE->layout_options['noblocks']) && $PAGE->blocks->region_has_content('side-post', $OUTPUT));

$showsidepost = ($hassidepost && !$PAGE->blocks->region_completely_docked('side-post', $OUTPUT));

$custommenu = $OUTPUT->custom_menu();
$hascustommenu = (empty($PAGE->layout_options['nocustommenu']) && !empty($custommenu));

$courseheader = $coursecontentheader = $coursecontentfooter = $coursefooter = '';
if (empty($PAGE->layout_options['nocourseheaderfooter'])) {
    $courseheader = $OUTPUT->course_header();
    $coursecontentheader = $OUTPUT->course_content_header();
    if (empty($PAGE->layout_options['nocoursefooter'])) {
        $coursecontentfooter = $OUTPUT->course_content_footer();
        $coursefooter = $OUTPUT->course_footer();
    }
}

$bodyclasses = array();
if ($showsidepost) {
    $bodyclasses[] = 'side-post-only';
} else if (!$showsidepost) {
    $bodyclasses[] = 'content-only';
}
if ($hascustommenu) {
    $bodyclasses[] = 'has_custom_menu';
}

if (!empty($PAGE->theme->settings->footertext)) {
    $footnote = $PAGE->theme->settings->footertext;
} else {
    $footnote = '<!-- There was no custom footnote set -->';
}

echo $OUTPUT->doctype();
echo '<html'.$OUTPUT->htmlattributes().'><head>';
include 'head.php';

echo '<link rel="stylesheet" type="text/css" href="'.new moodle_url('/theme/mdcl/style/home_style.css').'"';
echo '<link rel="stylesheet" type="text/css" href="'.new moodle_url('/theme/mdcl/style/pgwmodal.min.css').'"';
echo '<script src="'.new moodle_url('/theme/mdcl/javascript/pgwmodal.min.js').'"></script>';
echo '<script src="'.new moodle_url('/theme/mdcl/javascript/date.js').'"></script>';
echo '<script src="'.new moodle_url('/theme/mdcl/javascript/function_moodec.js').'"></script>';

echo '</head>';

//echo $OUTPUT->standard_top_of_body_html();
echo '<body' . $OUTPUT->body_attributes() . '>';
echo '<img src="' . $OUTPUT->pix_url('bgMoodle', 'theme') . '" class="superbg" />';


?>

<header role="banner" class="navbar notfrontpage">
    <?php include 'header.php'; ?>
</header>


<?php
$keyword = (!empty($_GET['keyword']) ? $_GET['keyword'] : '');
include_once('home/popup.php');
include_once('home/strings_js.php');

if (isset($_GET['catalogue'])) {
    include_once('home/search.php');
} else {
    include_once('home/index.php');
}

if ($hasfooter) {
    include 'footer.php';
}
?>
<?php echo $OUTPUT->standard_end_of_body_html() ?>
</body>
</html>