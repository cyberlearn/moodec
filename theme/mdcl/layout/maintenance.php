<?php
echo $OUTPUT->doctype();
echo '<html'.$OUTPUT->htmlattributes().'>';
echo '<head>';
include 'head.php';
echo '<head>';


echo '<body '.$OUTPUT->body_attributes().'>';
echo $OUTPUT->standard_top_of_body_html() ?>




<div id="page" class="container-fluid">

	<div id="page-content-header"></div>

    <div id="page-content" class="row-fluid maintenance">
        <section id="region-main" class="span12">
            <?php echo $OUTPUT->main_content(); ?>
        </section>
    </div>

    <?php echo $OUTPUT->standard_end_of_body_html(); ?>

</div>
</body>
</html>
