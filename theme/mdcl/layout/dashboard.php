<?php

$hasheading = ($PAGE->heading);
$hasnavbar = (empty($PAGE->layout_options['nonavbar']) && $PAGE->has_navbar());
$hasfooter = (empty($PAGE->layout_options['nofooter']));

$bodyclasses = array();
    $bodyclasses[] = 'content-only';




echo $OUTPUT->doctype(); 
echo '<html'.$OUTPUT->htmlattributes().'><head>';
include 'head.php';
echo '</head>';

echo '<body'.$OUTPUT->body_attributes().'>';
echo '<img src="'.$OUTPUT->pix_url('bgMoodle', 'theme').'" class="superbg" />';
echo $OUTPUT->standard_top_of_body_html();

/*
<body id="<?php p($PAGE->bodyid) ?>" class="<?php p($PAGE->bodyclasses.' '.join(' ', $bodyclasses)) ?>">
<?php echo $OUTPUT->standard_top_of_body_html() ?>
*/
?>

<header role="banner" class="navbar notfrontpage">
	<?php include 'header.php';?>   
</header>

<div id="page">



    <?php if (!empty($courseheader)) { ?>
    <div id="course-header"><?php echo $courseheader; ?></div>
    <?php } ?>
		
		<div id="page-navbar" class="clearfix">
      <nav class="breadcrumb-nav"><?php echo $OUTPUT->navbar(); ?></nav>
      <div class="breadcrumb-button"> <?php echo $OUTPUT->page_heading_button(); ?></div>
     </div>

<!-- START OF CONTENT -->

		<div id="page-content" class="wrapper clearfix">
    	            	    <div id="region-main">
        	            	    <div class="region-content">
                              <?php 
																echo $OUTPUT->heading(format_string($COURSE->fullname), 1, 'coursetitle');
																echo $OUTPUT->main_content(); 
															?>	
	                	        </div>
    	                	</div>
    	</div>
		
<!-- END OF CONTENT -->
</div>


<!-- START OF FOOTER -->
<?php 
	if ($hasfooter) { 
		include 'footer.php';
	} 

?>

<?php echo $OUTPUT->standard_end_of_body_html() ?>
</body>
</html>