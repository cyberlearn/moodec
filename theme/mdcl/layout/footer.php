<div style="clear:both"></div>
<footer>
	<div id="footerContenu">
		<div id="footerLeft">
			<div class="col">
				<h4>
					<?php echo get_string('assistance', 'theme_cyberlearn'); ?>
				</h4>
				<ul>
					<li>+41 (0)58 900 01 17 </li>
					<li class="des">9:00-12:00 13:30-17:00</li>
					<li><a href="mailto:cyberlearn@hes-so.ch?subject=Moodle Homepage Message" >cyberlearn@hes-so.ch</a></li>
				</ul>
				
				
				
				
				
			</div>
			<div class="col colLast">
				<h4><?php echo get_string('links', 'theme_cyberlearn'); ?></h4>
				<ul>
					<li><a href="<?php echo new moodle_url('/mod/moodeccertificate/verify_certificate.php');?>" target="_blank"><?php echo get_string("verifymoodeccertificate", "theme_mdcl") ?></a></li>
				</ul>
				
			</div>
			<div style="clear:both"></div>
			
		</div>
		<div id="footerRight">
			<div>
				<h4><?php echo get_string('joinin', 'theme_cyberlearn') ?></h4>
				
				<a id="btnFacebook" class="btnRS" title="Facebook" href="http://www.facebook.com/cyberlearn" target="_blank"></a>
				<a id="btnTwitter" class="btnRS" title="Twitter" href="http://www.twitter.com/thecyberlearn" target="_blank"></a>
				<a id="btnYoutube" class="btnRS" title="Youtube" href="http://www.youtube.com/user/TheCyberlearn" target="_blank"></a>
				<a id="btnSwitch" class="btnRS" title="SWITCH Tube" href="https://tube.switch.ch/profiles/2100" target="_blank"></a>
				<div style="clear:both"></div>
			</div>
		</div>
		<div style="clear:both"></div>
	</div>
</footer>