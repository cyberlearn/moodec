<?php
$hasheading = ($PAGE->heading);
$hasnavbar = (empty($PAGE->layout_options['nonavbar']) && $PAGE->has_navbar());
$hasfooter = (empty($PAGE->layout_options['nofooter']));
$hassidepost = $PAGE->blocks->region_has_content('side-post', $OUTPUT);
$showsidepost = false;
$token = md5(rand(1000,9999));
$_SESSION['token'] = $token;

$custommenu = $OUTPUT->custom_menu();
$hascustommenu = (empty($PAGE->layout_options['nocustommenu']) && !empty($custommenu));

$bodyclasses = array();
if ($showsidepost) {
	$bodyclasses[] = 'side-post-only';
} else if (!$showsidepost) {
	$bodyclasses[] = 'content-only';
}
if ($hascustommenu) {
	$bodyclasses[] = 'has_custom_menu';
}


if (!empty($PAGE->theme->settings->tagline)) {
	$tagline = $PAGE->theme->settings->tagline;
} else {
	$tagline = '<!-- There was no custom tagline set -->';
}

if (!empty($PAGE->theme->settings->footertext)) {
	$footnote = $PAGE->theme->settings->footertext;
} else {
	$footnote = '<!-- There was no custom footnote set -->';
}

$bodyclass = ' ';
$fileInclude = 'home/index.php';
if (isset($_GET['categoryid']))
{
	$fileInclude = 'home/search.php';
} else if (isset($_GET['contact']))
{
	$fileInclude = 'home/contact.php';
}
else if (isset($_GET['certificate']))
{
	$fileInclude = 'home/certificate.php';
}
else
{
	$fileInclude = 'home/index.php';
	$bodyclass = 'site_frontpage ';
}


echo $OUTPUT->doctype() ;
echo '<html'.$OUTPUT->htmlattributes().'><head>';
include 'head.php';


echo '<link rel="stylesheet" type="text/css" href="'.new moodle_url('/theme/mdcl/style/pgwmodal.min.css').'">';
echo '<link rel="stylesheet" type="text/css" href="'.new moodle_url('/theme/mdcl/style/home_style.css').'">';

echo '<script src="'.new moodle_url('/theme/mdcl/javascript/pgwmodal.min.js').'"></script>';
echo '<script src="'.new moodle_url('/theme/mdcl/javascript/date.js').'"></script>';
echo '<script src="'.new moodle_url('/theme/mdcl/javascript/function_moodec.js').'"></script>';

echo '</head>';

?>


<body id="<?php p($PAGE->bodyid) ?>" class="<?php echo $bodyclass; p($PAGE->bodyclasses.' '.join(' ', $bodyclasses)) ?>">
<?php
echo '<input type="hidden" id="token" value="' . $token . '"/>';
echo $OUTPUT->standard_top_of_body_html();
echo '<img src="'.$OUTPUT->pix_url('bgMoodle', 'theme').'" class="superbg" />';
?>

<header role="banner" class="navbar frontpage">
	<?php include 'header.php';?>
</header>


<?php
$rootMoodec = $CFG->wwwroot;
echo  html_writer::empty_tag('input', array('type'=>'hidden', 'id'=>'rootMoodec', 'value'=> $rootMoodec));
$keyword = (!empty($_GET['keyword']) ? $_GET['keyword'] : '');
include_once('home/popup.php');
include_once('home/strings_js.php');


include_once($fileInclude);
/*
if (isset($_GET['categoryid'])) {
        include_once('home/search.php');
} else if (isset($_GET['contact'])){
    include_once('home/contact.php');
}  else if (isset($_GET['certificate'])){
    include_once('home/certificate.php');
}else{
    include_once('home/index.php');
}
*/
include 'footer.php';
echo $OUTPUT->standard_end_of_body_html();
?>
</body>
</html>
