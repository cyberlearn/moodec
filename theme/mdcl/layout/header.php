<div id="pageheader">
	<div id="pageheaderContenu">
        <?php
        if(current_language()=='de'){
            $showheader = 'Header einblenden';
        }else{
            $showheader = 'Show Header';
        }
        ?>
        <?php /*
				<div id="show_header"><a href="javascript:showHeader();"><?php echo $showheader; ?></a></div>
				*/ ?>
				

        <div id="pageheader-top-left">
					<div id="headerlangmenu">
						<?php 
							echo $OUTPUT->custom_menu_language();
						?>
					</div>
					<div id="headersearchbox">
						<form id="headercoursesearch"  method="get" action="<?php echo $CFG->wwwroot; ?>/?redirect=0">
							<input type="hidden" name="redirect" value="0">
							<input type="hidden" name="categoryid" value="1">
						<input type="text" id="headershortsearchbox" size="12" name="keyword" placeholder="<?php echo get_string('searchcourses'); ?>" />
						<input type="submit" value="Valider" /></form>
					</div>
					 <div id="clear"></div>
				</div>
						
        <div id="pageheader-top-right"><?php //echo $OUTPUT->login_info(); ?>
				<?php 
					//echo $OUTPUT->user_menu();  
					echo $OUTPUT->costom_user_menu(); ?>
					 <div id="clear"></div>
				</div>
        <div id="clear"></div>
			</div>
		</div>
		<div id="clear"></div>
		<div id="pageHeaderMenu">


					<a href="#" id="pull">Menu</a>
					<?php 
							echo $OUTPUT->custom_menu_header();
						?>
        <div id="pageheader-menu-right">
				<a id="logoMoocHeader" href="<?php echo $CFG->wwwroot; ?>/?redirect=0"></a>
					<a id="logo" href="http://www.hes-so.ch/" target="_blank"></a>
				</div>
		</div>


<?php if (empty($PAGE->layout_options['nonavbar']) && !$PAGE->has_navbar()) { ?>
          <div id="navbarMenu"> 
						 <div id="page-navbar" class="clearfix">
                <nav class="breadcrumb-nav"><?php echo $OUTPUT->navbar(); ?></nav>
                <div class="breadcrumb-button"> <?php echo $OUTPUT->page_heading_button(); ?></div>
								
            </div>
					</div>
<?php } ?>

