<?php
// Ouverture d'une connexion � la base de donn�es
require_once('../db/config.php');
/**
 * Verify a certificate code
 * @param $code
 * @return mixed
 */
function verifyCertificate($code){
    global $DB, $CFG, $PAGE;
    require_once($CFG->dirroot . '/mod/moodeccertificate/lib.php');
    require_once($CFG->dirroot . '/mod/moodeccertificate/locallib.php');

    $context = context_system::instance();
    $PAGE->set_context($context);

    $sql = "SELECT ci.timecreated AS citimecreated,
     ci.code, ci.moodeccertificateid, ci.userid, c.*, u.firstname, u.lastname
     FROM {moodeccertificate_issues} ci
                           INNER JOIN {user} u
                           ON u.id = ci.userid
                           INNER JOIN {moodeccertificate} c
                           ON c.id = ci.moodeccertificateid
                           WHERE ci.code = ?";
    $certificates = $DB->get_records_sql($sql, array($code));
    // Print Section.
    foreach ($certificates as $certdata) {
        $course = $DB->get_record('course', array('id' => $certdata->course));
        if ($course) {
            $course->fullname ;
        }

        // Modify printdate so that date is always printed.
        $certdata->printdate = 1;
        $certrecord = new stdClass();
        $certrecord->timecreated = $certdata->citimecreated;
        $certrecord->code = $certdata->code;
        $certrecord->userid = $certdata->userid;
        $certrecord->user = $certdata->firstname . " " . $certdata->lastname;

        $course = $DB->get_record('course', array('id' => $certdata->course));
        $date = moodeccertificate_get_date($certdata, $certrecord, $course, $certdata->userid);

        if ($course) {
            $certrecord->course = $course->fullname;
        }

        if ($date) {
            $certrecord->date = $date;
        }

        if ($course && $certdata->printgrade > 0) {
            $certrecord->grade =  moodeccertificate_get_grade($certdata, $course, $certdata->userid);
        }

        return json_encode($certrecord);
    }
}

/**
 * Get top course
 * @param int $nbr
 * @return string
 */
function getCourseListTop($nbr = 8){

    $connection = $GLOBALS['connection'];

    // Pr�paration des requ�tes
    $query = $connection->prepare("
SELECT mc.id, course as courseid, fullname, summary, startdate, picture as pictureCourse, institution
FROM mdl_moodecdescription md, mdl_course mc, mdl_enrol en
WHERE mc.id = md.course
AND mc.id = en.courseid
AND en.enrol = 'self'
AND en.enrolstartdate < :todaystate
AND endcoursedate > :today
AND mc.visible = 1 AND mc.id != 30
ORDER BY id,startdate ASC LIMIT :limit");

    $today = time();

    $query->bindParam(':limit', $nbr, PDO::PARAM_INT);
    $query->bindParam(':todaystate', $today, PDO::PARAM_INT);
    $query->bindParam(':today', $today, PDO::PARAM_INT);

    // Execution des requ�tes
    try {
        $query -> execute();
        $record = $query -> fetchAll(PDO::FETCH_CLASS, "stdClass");
    } catch( Exception $e ) {
        return $e->getMessage();
    }
    return json_encode($record);
}

/**
 * Enrol a new user
 * */
function enrolUser($courseid,$prelogin = false){
    global $CFG , $SESSION, $USER;

    $connection = $GLOBALS['connection'];

    require_once($CFG->libdir . "/formslib.php");
    require_once($CFG->dirroot. "/enrol/self/locallib.php");
    require_once ($CFG->dirroot . "/enrol/self/lib.php");

    if (!isloggedin()) {
        $SESSION->wantsurl = $CFG->wwwroot.'/theme/mdcl/srv/ajax.php?action=enrolUser&courseid='.$courseid . '&prelogin=true&token=' . $_SESSION['token'];
        return get_login_url();

    } else {
        $queryEnrol = $connection->prepare("
SELECT COUNT( * ) as count
FROM  mdl_user_enrolments ue, mdl_enrol e, mdl_user u
WHERE ue.userid = u.id
AND ue.enrolid = e.id
AND e.courseid = :courseid
AND u.id =:iduser");

        $queryEnrol->bindParam(':courseid', $courseid);
        $queryEnrol->bindParam(':iduser', $USER->id);

        $queryEnrol -> execute();
        $count = $queryEnrol->fetchColumn();

        if($count < 1) {
            $enrols = enrol_get_plugins(true);
            $enrolinstances = enrol_get_instances($courseid, true);
            foreach ($enrolinstances as $instance) {
                if ($instance->enrol == "self") {
                    $enrols[$instance->enrol]->enrol_self($instance);
                }
            }
        }

        if($prelogin == true){
            header("location : " . $CFG->wwwroot . "/course/view.php?id=" . $courseid);
        }
    }
}

/**
 * Get a course by ID
 * @param $idC
 * @return string
 */
function getCourse($idC, $idU){

    $connection = $GLOBALS['connection'];

    $contextLevel = 50;
    $roleId = 3;
    $enrol = "%self%";
    // Pr�paration des requ�tes
    $query = $connection->prepare('
SELECT c.id as contextid, mc.id, course as courseid, fullname, summary, startdate, e.id as enrolid, endcoursedate, enrolstartdate, enrolenddate, effort, duration, md.picture as pictureCourse, md.institution, video, syllabus, reading, faq, prerequisite
FROM mdl_moodecdescription md, mdl_course mc, mdl_enrol e,  mdl_context c
WHERE mc.id=md.course AND e.courseid = mc.id AND md.course= :id AND e.enrol LIKE :enrol AND c.instanceid = mc.id AND contextlevel = :contextlevel');

    $query->bindParam(':id', $idC);
    $query->bindParam(':enrol', $enrol);
    $query->bindParam(':contextlevel', $contextLevel);

    $queryTeacher = $connection->prepare("
SELECT u.id as uid, firstname, lastname, email, r.shortname as role
FROM mdl_moodecdescription md, mdl_course mc, mdl_user u, mdl_context c, mdl_role r, mdl_role_assignments ra
WHERE mc.id=md.course AND md.course= :id
AND c.id = ra.contextid
AND ra.roleid = r.id
AND ra.userid = u.id
AND r.id = :roleid
AND c.contextlevel = :contextlevel
AND c.instanceid = mc.id");

    $queryTeacher->bindParam(':id', $idC);
    $queryTeacher->bindParam(':roleid', $roleId);
    $queryTeacher->bindParam(':contextlevel', $contextLevel);

    $queryEnrol = $connection->prepare("
SELECT COUNT( * ) as count
FROM  mdl_user_enrolments ue, mdl_enrol e, mdl_user u
WHERE ue.userid = u.id
AND ue.enrolid = e.id
AND e.courseid = :courseid
AND u.id =:iduser");

    $queryEnrol->bindParam(':courseid', $idC);
    $queryEnrol->bindParam(':iduser', $idU);

    // Execution des requ�tes
    try {
        $query -> execute();
        $query->setFetchMode(PDO::FETCH_CLASS, "stdClass");
        $record = $query -> fetch();

        $queryEnrol -> execute();
        $record->enrol = $queryEnrol->fetchColumn();

        $queryTeacher -> execute();
        $recordTeacher = $queryTeacher -> fetchAll(PDO::FETCH_CLASS, "stdClass");

        for($i = 0 ; $i < count($recordTeacher) ; $i++){
            $recordTeacher[$i]->pictureTeacher = "/user/pix.php/". $recordTeacher[$i]->uid ."/f1.jpg";
        }

        $record->teachers = $recordTeacher;

    } catch( Exception $e ) {
        return $e->getMessage();
    }
    return json_encode($record);
}

function getKeywords($keywords){
    $temp = preg_split('/(\s+)/', $keywords, -1, PREG_SPLIT_DELIM_CAPTURE | PREG_SPLIT_NO_EMPTY);

    $words = array_reduce( $temp, function( &$result, $item) {
        if(strlen( trim( $item)) !== 0) {
            $result[] = "%" . $item . "%";
        }
        return $result;
    }, array());
    return $words;
}
/**
 * Search a course by keyword
 * @param $keywords
 * @return string
 */
function searchCourse($keywords, $lang)
{
    $connection = $GLOBALS['connection'];
    $arrayKeywords = getKeywords($keywords);
    $queryS = "";
    $queryL = "";
    $countKeywords = count($arrayKeywords);

    // Pr�paration des requ�tes
    // Keywords
    for($i = 0 ; $i < $countKeywords ; $i++) {
        $queryS .= "(tags LIKE :tag" . $i . " OR fullname LIKE :fullname" . $i . " OR summary LIKE :summary" . $i . "  OR institution LIKE :institution" . $i . " ) AND ";
    }

    if($lang == "fr"){
        $queryL .= '(mc.lang LIKE :lang OR mc.lang = "")';
    } else {
        $queryL .= "(mc.lang LIKE :lang)";
    }


    $query = $connection->prepare("
SELECT  mc.id, course as courseid, picture as pictureCourse, fullname, startdate, summary, institution
FROM mdl_moodecdescription md, mdl_course mc, mdl_enrol en
WHERE mc.id=md.course
AND mc.id = en.courseid  
AND md.endcoursedate > :today 
AND en.enrol = 'self'
AND en.enrolstartdate < :todaystate 
AND ". $queryS . $queryL . " 
AND mc.visible = 1 AND mc.id != 30");

    for ($i = 0; $i < $countKeywords; $i++) {
        $summaryKey = ":summary" . $i;
        $fullnameKey = ":fullname" . $i;
        $institutionKey = ":institution" . $i;
        $tagKey = ":tag" . $i;

        if(!isset($arrayKeywords[$i])){
            $summaryString = "%%";
            $fullnameString = "%%";
            $institutionString = "%%";
            $tagString = "%%";
        } else {
            $summaryString = $arrayKeywords[$i];
            $fullnameString = $arrayKeywords[$i];
            $institutionString = $arrayKeywords[$i];
            $tagString = $arrayKeywords[$i];
        }
        $query->bindValue($summaryKey,$summaryString);
        $query->bindValue($fullnameKey,$fullnameString);
        $query->bindValue($institutionKey,$institutionString);
        $query->bindValue($tagKey,$tagString);
    }

    $query->bindValue(":lang", $lang);

    $today = time();
    $query->bindValue(":todaystate", $today);
    $query->bindValue(":today", $today);

    // Execution des requ�tes
    try {
        $query -> execute();
        $record = $query -> fetchAll(PDO::FETCH_CLASS, "stdClass");
    } catch( Exception $e ) {
        return $e->getMessage();
    }
    return json_encode($record);
}

function searchCourseAdvanced($keywords, $institution = array(), $lang = array())
{
    $connection = $GLOBALS['connection'];
    $queryInstitution = "";
    $queryLang = "";
    $arrayKeywords = getKeywords($keywords);
    $queryKeywords = "";

    // Keywords
    $countKeywords = count($arrayKeywords);

    // Institution
    $countInstitution = count($institution);
    for ($i = 0; $i < $countInstitution; $i++) {
        if ($i == 0 && $i == $countInstitution - 1) {
            $queryInstitution = "(institution LIKE :institution" . $i . ") AND ";
        } else if ($i == 0) {
            $queryInstitution = "(institution LIKE :institution" . $i;
        } else if ($i > 0 && $i != $countInstitution - 1) {
            $queryInstitution .= " OR institution LIKE :institution" . $i;
        } else if ($i == $countInstitution - 1) {
            $queryInstitution .= " OR institution LIKE :institution" . $i . ") AND ";
        }
    }

    // Lang
    $countLang = count($lang);
    for ($i = 0; $i < $countLang; $i++) {
        if ($i == 0 && $i == $countLang - 1) {
            if(!empty($lang[$i])){
                if($lang[$i] == "fr"){
                    $queryLang = '(lang LIKE :lang' . $i . ' OR lang = "")';
                } else {
                    $queryLang = "(lang LIKE :lang" . $i . ")  ";
                }
            }else {
                $queryLang = "(lang LIKE :lang" . $i . ")  ";
            }
        } else if ($i == 0) {
            if(!empty($lang[$i])) {
                if ($lang[$i] == "fr") {
                    $queryLang = '((lang LIKE :lang' . $i . ' OR lang = "")';
                } else {
                    $queryLang = "(lang LIKE :lang" . $i;
                }
            } else {
                $queryLang = "(lang LIKE :lang" . $i;
            }
        } else if ($i > 0 && $i != $countLang - 1) {
            $queryLang .= " OR lang LIKE :lang" . $i;
        } else if ($i == $countLang - 1) {
            $queryLang .= " OR lang LIKE :lang" . $i . ")  ";
        }
    }

    // Keywords
    if($countKeywords == 0){
        $query =  $queryInstitution . $queryLang;
    } else {
        for($i = 0 ; $i < $countKeywords ; $i++) {
            $queryKeywords .= " AND (tags LIKE :tag" . $i . " OR fullname LIKE :fullname" . $i . " OR summary LIKE :summary" . $i . "  OR institution LIKE :institutionkw" . $i . " ) ";
        }
        $query =  $queryInstitution . $queryLang . $queryKeywords;
    }

    // Pr�paration des requ�tes
    $queryAdv = $connection->prepare("
SELECT mc.id, course as courseid, picture as pictureCourse, fullname, startdate, summary, institution
FROM mdl_moodecdescription md, mdl_course mc, mdl_course_categories cc, mdl_enrol en
WHERE mc.id=md.course
AND mc.id = en.courseid  
AND md.endcoursedate > :today 
AND en.enrol = 'self'
AND en.enrolstartdate < :todaystate 
AND mc.category = cc.id
AND mc.visible = 1
AND mc.id != 30
AND " . $query);

    $today = time();
    $queryAdv->bindValue(":today", $today);
    $queryAdv->bindValue(":todaystate", $today);
    for ($i = 0; $i < $countKeywords; $i++) {
        $summaryKey = ":summary" . $i;
        $fullnameKey = ":fullname" . $i;
        $institutionKey = ":institutionkw" . $i;
        $tagKey = ":tag" . $i;

        $summaryString = $arrayKeywords[$i];
        $fullnameString = $arrayKeywords[$i];
        $institutionString = $arrayKeywords[$i];
        $tagString = $arrayKeywords[$i];

        $queryAdv->bindValue($summaryKey,$summaryString);
        $queryAdv->bindValue($fullnameKey,$fullnameString);
        $queryAdv->bindValue($institutionKey,$institutionString);
        $queryAdv->bindValue($tagKey,$tagString);
    }

    for ($i = 0; $i < $countLang; $i++) {
        $langKey = ":lang" . $i;
        if(!isset($lang[$i])){
            $langString = "%%";
        }
        else {
            $langString = $lang[$i];
        }
        $queryAdv->bindValue($langKey,$langString);
    }

    for ($i = 0; $i < $countInstitution; $i++) {
        $institutionKey = ":institution".$i;
        if(!isset($institution[$i])){
            $institutionString = "%%";
        } else {
            $institutionString = $institution[$i];
        }
        $queryAdv->bindValue($institutionKey, $institutionString);
    }

    // Execution des requ�tes
    try {
        $queryAdv -> execute();
        $record = $queryAdv -> fetchAll(PDO::FETCH_CLASS, "stdClass");
    } catch( Exception $e ) {
        return $e->getMessage();
    }
    return json_encode($record);

}

/**
 * Get param for the adv search
 * @return string
 */
function getParamResearchAdvanced(){
    $connection = $GLOBALS['connection'];

    $queryInstitution = $connection->prepare("
SELECT DISTINCT md.institution
FROM mdl_moodecdescription md, mdl_course mc, mdl_course_categories cc
WHERE mc.id=md.course
AND mc.category = cc.id AND mc.visible = 1");

    // Execution des requ�tes
    try {

        $queryInstitution -> execute();
        $institution = $queryInstitution -> fetchAll(PDO::FETCH_CLASS, "stdClass");

        $record = new stdClass();
        $record->institutions = $institution;

    } catch( Exception $e ) {
        return $e->getMessage();
    }
    return json_encode($record);
}

/**
 * Get User
 * @return string
 */
function getUser($idUser){
    $connection = $GLOBALS['connection'];

    $query = $connection->prepare("
SELECT  lastname, firstname, email
FROM mdl_user
WHERE id=:id");
    $query->bindParam(':id', $idUser);
    // Execution des requ�tes
    try {
        $query -> execute();
        $record = $query -> fetch();

    } catch( Exception $e ) {
        return $e->getMessage();
    }
    return json_encode($record);
}
?>