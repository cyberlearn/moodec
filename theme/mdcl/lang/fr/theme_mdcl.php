<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'theme_fusion', language 'en', branch 'MOODLE_20_STABLE'
 *
 * @package   moodlecore
 * @copyright 2015 Cyberlearn
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
$string['subscribehome'] = 'Inscrivez-vous';
$string['nowhome'] = 'dès maintenant';
$string['dayclock'] = 'Jours';
$string['hourclock'] = 'Heures';
$string['minclock'] = 'Minutes';
$string['secclock'] = 'Secondes';
$string['titleclock'] = 'Les premiers Moocs de la HES-SO commenceront dans';
$string['pluginname'] = 'mdcl';
$string['region-side-post'] = 'Right';
$string['region-side-pre'] = 'Left';
$string['choosereadme'] = '
<div class="clearfix">
	<div class="theme_screenshot">

		<h2>MECL - Moodec Cyberlearn</h2>
		<img src="fusion/pix/screenshot.jpg" />

		<h3>Theme Discussion Forum:</h3>
		<p><a href="http://moodle.org/mod/forum/view.php?id=46">http://moodle.org/mod/forum/view.php?id=46</a></p>

		<h3>Theme Credits</h3>
		<p><a href="http://docs.moodle.org/en/Theme_credits">http://docs.moodle.org/en/Theme_credits</a></p>

		<h3>Theme Documentation:</h3>
		<p><a href="http://docs.moodle.org/en/Themes">http://docs.moodle.org/en/Themes</a></p><h3>Report a bug:</h3><p><a href="http://tracker.moodle.org">http://tracker.moodle.org</a></p>
	</div>

	<div class="theme_description">

		<h2>About</h2>
		<p>Fusion is a two-column, fluid-width theme coded for Moodle 2.0. It makes use of custom menus that appear above the site title on every page. It also includes a basic settings page allowing you to change link color, add a tagline above the site name on the front page, and add text to the footer.</p>

		<h2>Parents</h2>
		<p>This theme is built upon both Base and Canvas, two parent themes included in the Moodle core. If you wish to modify aspects of this theme beyond the settings offered, we advise creating a new  theme using this theme, Base and Canvas all as parents so updates to any of those themes in the core will find their way into your new theme.</p>

		<h2>Credits</h2>
		<p>This design was originally created by Digital Nature (hello@digitalnature.ro) before being ported to Moodle by Patrick Malley (patrick@newschoollearning.com).
		<h2>License</h2>
		<p>This, and all other themes included in the Moodle core, are licensed under the <a href="http://www.gnu.org/licenses/gpl.html">GNU General Public License</a>.
	</div>
</div>';

$string['home'] = 'Accueil';
$string['catalog'] = 'Catalogue';
$string['contact'] = 'Contact';
$string['assistance'] = 'Assistance';
$string['links'] = 'Liens';
$string['joinin'] = 'Rejoignez-nous!';
$string['searchcourse'] = 'Rechercher des cours';
$string['myhomemdcl'] = 'Mon tableau de bord';
$string['loggedinas'] = ' connecté en tant que ';
$string['loggedinfrom'] = 'Connectés à partir de ';


/**
 * HOME PAGE
 */
$string['bannersoustitre'] = 'La plateforme Mooc de la HES-SO';
$string['topcourse'] = 'Cours disponibles';
$string['partenaires'] = 'Partenaires';
$string['enrolme'] = 'M\'inscrire';
$string['enrolnotyet'] = 'Disponible à partir du ';
$string['searchadvanced'] = 'Recherche avancée';
$string['keywords'] = 'Mots-clés';
$string['category'] = 'Catégories';
$string['institution'] = 'Institutions';
$string['lang'] = 'Langues';
$string['moreinfo'] = 'Informations complémentaires';
$string['prerequisite'] = 'Pré-requis';
$string['syllabus'] = 'Syllabus';
$string['reading'] = 'Lecture';
$string['faq'] = 'FAQ';
$string['school'] = 'Institution';
$string['startdate'] = 'Début';
$string['enddate'] = 'Fin';
$string['duration'] = 'Durée';
$string['effort'] = 'Effort';
$string['error'] = 'Une erreur est survenue';
$string['nocourse'] = 'Aucun cours n\'est disponible';
$string['cours'] = 'cours';
$string['courspluriel'] = 'cours';
$string['fr'] = 'Français';
$string['de'] = 'Allemand';
$string['en'] = 'Anglais';
$string['accesscours'] = 'Accéder au cours';
$string['hour'] = 'heure';
$string['hours'] = 'heures';
$string['week'] = 'semaine';
$string['weeks'] = 'semaines';
$string['role'] = 'Professeur';
//$string['contact'] = 'Contact';
$string['contacttitle'] = 'Contacter l\'administrateur' ;
$string['contactforname'] = 'Prénom : ';
$string['contactname'] = 'Nom : ';
$string['contactemail'] = 'Email : ';
$string['message'] = 'Message : ';
$string['send'] = 'Envoyer';
$string['refresh'] = 'Rafraichir';
$string['mandatoryfield'] = 'Tous les champs sont obligatoires';
$string['emailinvalid'] = "L'email n'est pas correct";
$string['mailinvalid'] = "Une erreur est survenue";
$string['mailsuccess'] = "Votre demande sera traitée dans les plus brefs délais";
$string['captchainvalid'] = "Captcha invalide";
$string['enrolend'] = "Cours terminé";
$string['verifymoodeccertificate'] = 'Verifier un certificat';