<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'theme_fusion', language 'en', branch 'MOODLE_20_STABLE'
 *
 * @package   moodlecore
 * @copyright 2015 Cyberlearn
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
  $string['subscribehome'] = 'Einschreiben';
$string['nowhome'] = 'jetzt';
 $string['dayclock'] = 'Tage';
$string['hourclock'] = 'Stunden';
$string['minclock'] = 'Minuten';
$string['secclock'] = 'Sekunden';
$string['titleclock'] = 'Die ersten Moocs HES-SO beginnen in ';
$string['pluginname'] = 'mdcl';
$string['region-side-post'] = 'Right';
$string['region-side-pre'] = 'Left';
$string['choosereadme'] = '
<div class="clearfix">
	<div class="theme_screenshot">

		<h2>MECL - Moodec Cyberlearn</h2>
		<img src="fusion/pix/screenshot.jpg" />

		<h3>Theme Discussion Forum:</h3>
		<p><a href="http://moodle.org/mod/forum/view.php?id=46">http://moodle.org/mod/forum/view.php?id=46</a></p>

		<h3>Theme Credits</h3>
		<p><a href="http://docs.moodle.org/en/Theme_credits">http://docs.moodle.org/en/Theme_credits</a></p>

		<h3>Theme Documentation:</h3>
		<p><a href="http://docs.moodle.org/en/Themes">http://docs.moodle.org/en/Themes</a></p><h3>Report a bug:</h3><p><a href="http://tracker.moodle.org">http://tracker.moodle.org</a></p>
	</div>

	<div class="theme_description">

		<h2>About</h2>
		<p>Fusion is a two-column, fluid-width theme coded for Moodle 2.0. It makes use of custom menus that appear above the site title on every page. It also includes a basic settings page allowing you to change link color, add a tagline above the site name on the front page, and add text to the footer.</p>

		<h2>Parents</h2>
		<p>This theme is built upon both Base and Canvas, two parent themes included in the Moodle core. If you wish to modify aspects of this theme beyond the settings offered, we advise creating a new  theme using this theme, Base and Canvas all as parents so updates to any of those themes in the core will find their way into your new theme.</p>

		<h2>Credits</h2>
		<p>This design was originally created by Digital Nature (hello@digitalnature.ro) before being ported to Moodle by Patrick Malley (patrick@newschoollearning.com).
		<h2>License</h2>
		<p>This, and all other themes included in the Moodle core, are licensed under the <a href="http://www.gnu.org/licenses/gpl.html">GNU General Public License</a>.
	</div>
</div>';
$string['home'] = 'Zuhause';
$string['catalog'] = 'Katalog';
$string['contact'] = 'Kontakt';
$string['contacttitle'] = 'Kontakt administrator' ;
$string['assistance'] = 'Unterstützung';
$string['links'] = 'Links';
$string['joinin'] = 'Dazu kommen!';
$string['searchcourse'] = 'Kurse suchen';
$string['myhomemdcl'] = 'Mein Dashboard';
$string['loggedinas'] = ' Angemeldet als ';
$string['loggedinfrom'] = 'In vom angemeldet ';

$string['bannersoustitre'] = 'Die Mooc Plattform der HES-SO';
$string['topcourse'] = 'Kurse zur Verfügung';
$string['partenaires'] = 'Partners';
$string['enrolme'] = 'Registrieren';
$string['enrolnotyet'] = 'Ab  ';
$string['searchadvanced'] = 'Erweiterte Suche';
$string['keywords'] = 'Stichwörter';
$string['category'] = 'Kategorien';
$string['institution'] = 'Institutionen';
$string['lang'] = 'Sprachen';
$string['moreinfo'] = 'Zusätzliche Informationen';
$string['prerequisite'] = 'Voraussetzung';
$string['syllabus'] = 'Lehrplan';
$string['reading'] = 'Lesen';
$string['faq'] = 'FAQ';
$string['school'] = 'Institution';
$string['startdate'] = 'Beginn';
$string['enddate'] = 'Ende';
$string['duration'] = 'Dauer';
$string['effort'] = 'Bemühung';
$string['error'] = 'Ein Fehler trat auf';
$string['nocourse'] = 'Kein Kurs ist verfügbar';
$string['cours'] = 'Kurs';
$string['courspluriel'] = 'Kurse';
$string['fr'] = 'Französich';
$string['de'] = 'Deutsch';
$string['en'] = 'English';
$string['accesscours'] = 'Zugriff auf den Kurs';
$string['hour'] = 'Stunde';
$string['hours'] = 'Stunden';
$string['week'] = 'Woche';
$string['weeks'] = 'Wochen';
$string['role'] = 'Dozent';
$string['contact'] = 'Kontakt';
$string['contactforname'] = 'Vorname : ';
$string['contactname'] = 'Name : ';
$string['contactemail'] = 'Email : ';
$string['message'] = 'Mitteilung : ';
$string['send'] = 'Senden';
$string['refresh'] = 'Aktualisieren';
$string['mandatoryfield'] = 'Alle Felder obligatorisch';
$string['emailinvalid'] = "Email ist ungültig";
$string['mailinvalid'] = "Ein Fehler ist aufgetreten";
$string['mailsuccess'] = "Ihre Anfrage wurde verschickt";
$string['captchainvalid'] = "Captcha ist ungültig";
$string['enrolend'] = "Der Kurs  ist fertig";
$string['verifymoodeccertificate'] = 'Überprüfen eines Zertifikats';