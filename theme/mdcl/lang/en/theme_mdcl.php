<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'theme_fusion', language 'en', branch 'MOODLE_20_STABLE'
 *
 * @package   moodlecore
 * @copyright 2015 Cyberlearn
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
 $string['subscribehome'] = 'Register';
$string['nowhome'] = 'now';
 $string['dayclock'] = 'Days';
$string['hourclock'] = 'Hours';
$string['minclock'] = 'Minutes';
$string['secclock'] = 'Seconds';
$string['titleclock'] = 'First Moocs of HES-SO will begin in ';
$string['pluginname'] = 'mdcl';
$string['region-side-post'] = 'Right';
$string['region-side-pre'] = 'Left';
$string['choosereadme'] = '
<div class="clearfix">
	<div class="theme_screenshot">

		<h2>MECL - Moodec Cyberlearn</h2>
		<img src="fusion/pix/screenshot.jpg" />

		<h3>Theme Discussion Forum:</h3>
		<p><a href="http://moodle.org/mod/forum/view.php?id=46">http://moodle.org/mod/forum/view.php?id=46</a></p>

		<h3>Theme Credits</h3>
		<p><a href="http://docs.moodle.org/en/Theme_credits">http://docs.moodle.org/en/Theme_credits</a></p>

		<h3>Theme Documentation:</h3>
		<p><a href="http://docs.moodle.org/en/Themes">http://docs.moodle.org/en/Themes</a></p><h3>Report a bug:</h3><p><a href="http://tracker.moodle.org">http://tracker.moodle.org</a></p>
	</div>

	<div class="theme_description">

		<h2>About</h2>
		<p>Fusion is a two-column, fluid-width theme coded for Moodle 2.0. It makes use of custom menus that appear above the site title on every page. It also includes a basic settings page allowing you to change link color, add a tagline above the site name on the front page, and add text to the footer.</p>

		<h2>Parents</h2>
		<p>This theme is built upon both Base and Canvas, two parent themes included in the Moodle core. If you wish to modify aspects of this theme beyond the settings offered, we advise creating a new  theme using this theme, Base and Canvas all as parents so updates to any of those themes in the core will find their way into your new theme.</p>

		<h2>Credits</h2>
		<p>This design was originally created by Digital Nature (hello@digitalnature.ro) before being ported to Moodle by Patrick Malley (patrick@newschoollearning.com).
		<h2>License</h2>
		<p>This, and all other themes included in the Moodle core, are licensed under the <a href="http://www.gnu.org/licenses/gpl.html">GNU General Public License</a>.
	</div>
</div>';

$string['home'] = 'Home';
$string['catalog'] = 'Catalog';
$string['contact'] = 'Contact';

$string['assistance'] = 'Assistance';
$string['links'] = 'Links';
$string['joinin'] = 'Join in!';

$string['searchcourse'] = 'Search courses';
$string['myhomemdcl'] = 'My dashboard';


$string['loggedinas'] = ' logged in as ';
$string['loggedinfrom'] = 'Logged in from ';


$string['linkcolor'] = 'Link color';
$string['linkcolordesc'] = 'This sets the link color for the theme.';
$string['configtitle'] = 'Fusion settings';
$string['customcss'] = 'Custom CSS';
$string['customcssdesc'] = 'Any CSS you enter here will be added to every page allowing your to easily customise this theme.';
$string['tagline'] = 'Tagline';
$string['taglinedesc'] = 'A short tagline to be displayed under the site name on the front page.<br />';
$string['footertext'] = 'Footertext';
$string['footertextdesc'] = 'Set a footnote or footer text.';

/**
 * HOME PAGE
 */
$string['bannersoustitre'] = 'The Mooc plateform of the HES-SO';
$string['topcourse'] = 'Available courses';
$string['partenaires'] = 'Partners';
$string['enrolnotyet'] = 'Available from ';
$string['enrolme'] = 'Enrol me';
$string['searchadvanced'] = 'Research advanced';
$string['keywords'] = 'Keywords';
$string['institution'] = 'Institutions';
$string['lang'] = 'Languages';
$string['moreinfo'] = 'More informations';
$string['prerequisite'] = 'Pre-requisite';
$string['syllabus'] = 'Syllabus';
$string['reading'] = 'Reading';
$string['faq'] = 'FAQ';
$string['school'] = 'Institution';
$string['startdate'] = 'Start date';
$string['enddate'] = 'End date';
$string['duration'] = 'Duration';
$string['effort'] = 'Effort';
$string['error'] = 'An error has occurred';
$string['nocourse'] = 'No courses are availables';
$string['cours'] = 'course';
$string['courspluriel'] = 'courses';
$string['fr'] = 'French';
$string['de'] = 'German';
$string['en'] = 'English';
$string['accesscours'] = 'Go to course';
$string['hour'] = 'hour';
$string['hours'] = 'hours';
$string['week'] = 'week';
$string['weeks'] = 'weeks';
$string['role'] = 'Teacher';
$string['contacttitle'] = 'Contact administrator' ;
$string['contactforname'] = 'Firstname : ';
$string['contactname'] = 'Lastname : ';
$string['contactemail'] = 'Email : ';
$string['message'] = 'Content : ';
$string['send'] = 'Send';
$string['refresh'] = 'Refresh';
$string['mandatoryfield'] = 'All fields are required';
$string['emailinvalid'] = "Email is invalid";
$string['mailinvalid'] = "An error has occurred";
$string['mailsuccess'] = "Your request will be processed as soon as possible";
$string['captchainvalid'] = "Captcha is invalid";
$string['enrolend'] = "Course is over";
$string['verifymoodeccertificate'] = 'Verify certificate';
