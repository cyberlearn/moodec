QUICK INSTALL
=============

For the impatient, here is a basic outline of the
installation process, which normally takes me only
a few minutes:

Prerequiste : 

Server compatible with Moodle
Set Apache PHP execution time to 120 sec.

On your Apache server make sure you have a script execution time set to 60 seconds minimum

1) Move the MOODEC files into your web directory.

2) Create a single database for MOODEC to store all
   its tables in (or choose an existing database).

3) Visit your MOODEC site with a browser, you should
   be taken to the install.php script, which will lead
   you through creating a config.php file and then
   setting up MOODEC creating an admin account etc.

4) Set up a cron task to call the file admin/cron.php
   every five minutes or so.


For more information, see the INSTALL DOCUMENTATION:
Just follow the standard Moodle Installation Documentation

http://docs.moodle.org/en/Installing_Moodle

Good luck and have fun!